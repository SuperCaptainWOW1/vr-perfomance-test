import S123Core from "./s123_core.js";
window.S123Core = S123Core;
// import * as S123Core from "../../../s123_core/dist/index.js";

import { HubConnectionBuilder } from "@microsoft/signalr";
import Network from "./Network.js";

export const S123 = function (THREE, FBXLoader, GLTFLoader) {
  S123.THREE = THREE;
  S123.FBXLoader = FBXLoader;
  S123.GLTFLoader = GLTFLoader;
  S123.texture_cache = {};
  S123.model_cache = {};
  S123.load_sprites = [];
  S123.clock = new S123.THREE.Clock();
  S123.loadingManager = new S123.THREE.LoadingManager();
};

S123.HOST = window.location.origin;
if (
  S123.HOST.startsWith("https://localhost") ||
  S123.HOST.startsWith("https://192.168.0.123")
)
  S123.HOST = "https://lab.system123.ru";
S123.DOTNET_HOST = S123.HOST + "/rest";
S123.COMMON_PATH = S123.HOST + "/common";
S123.auth_key = `oidc.user:${S123.HOST}/identity:web-business-app`;

S123.materialCubeGUID = "3169af9a-1f9b-4284-a28d-123906b700af";

S123.iik_max_texture_size = 512;
S123.iik_has_detailed_materials = false;

S123.signal_connection = null;

S123.Log = (...args) => {
  console.log("S123 LOG:", ...args);
};

window.S123 = S123;

export const guidsCache = new Set();

/**
 * Generates random guid
 *
 * @return {guid}
 */
S123.uuidv4 = function () {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
    const r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
};

S123.formatParams = function (params, start = "?") {
  const startParams = {};

  for (const paramKey in params) {
    const param = params[paramKey];

    if (paramKey === "pathToFile") {
      const nestedParams = param.split("&");

      if (nestedParams.length === 1) {
        startParams[paramKey] = param;
      } else {
        nestedParams.forEach((p) => {
          if (p.split("=").length !== 1) {
            startParams[p.split("=")[0]] = p.split("=")[1];
          } else {
            startParams[paramKey] = param.split("&")[0];
          }
        });
      }
    } else startParams[paramKey] = param;
  }

  let result = "?";

  for (let paramKey in startParams) {
    startParams[paramKey] = encodeURIComponent(startParams[paramKey]);
    paramKey = encodeURIComponent(paramKey);

    result += `${paramKey}=${startParams[paramKey]}&`;
  }

  return result;
};

S123.updateOptions = function (options = {}) {
  const update = { ...options };

  if (localStorage.getItem(S123.auth_key)) {
    //localStorage.jwt
    const token = JSON.parse(localStorage.getItem(S123.auth_key)).access_token;
    update.headers = {
      ...update.headers,
      Authorization: `Bearer ${token}`,
    };
  }

  if (S123.abortController) {
    update.signal = S123.abortController.signal;
    S123.abortController = null;
  }

  return update;
};

/// NETWORK ///

/**
 * Returns calculation internal parameters, such as name, creation date, list of files, tags and so on.
 *
 * @param {string} calculation_guid Guid of calculation
 * @return {json} json data with calculation params.
 */
S123.NET_GetCalculationStat = function (calculation_guid) {
  //Получение информации о расчете (асинхронный метод)
  return fetch(
    S123.DOTNET_HOST +
      "/api/Calculation/GetCalculationStat" +
      S123.formatParams({ guid: calculation_guid }),
    S123.updateOptions()
  )
    .then((response) => {
      return response.json();
    })
    .catch((e) => console.error(e));
};

S123.NET_DownloadFile_URL = function (pathToFile, maxSize = null) {
  //Получение файла из дотнета
  const get_params = {
    pathToFile: pathToFile,
  };

  if (maxSize) get_params["maxSize"] = maxSize;

  return (
    S123.DOTNET_HOST + "/api/Data/DownloadFile" + S123.formatParams(get_params)
  );
};

S123.NET_GetAllPaginationCalculations = async function (
  page = 1,
  limit = 50,
  tags = ""
) {
  const get_params = {
    offset: (page - 1) * limit,
    limit: limit,
    tags: tags,
  };

  return fetch(
    S123.DOTNET_HOST +
      "/api/Calculation/GetAllPaginationCalculations" +
      S123.formatParams(get_params),
    S123.updateOptions()
  ).then((response) => response.json());
};

S123.NET_GetAllTags = async function (tags = "") {
  const get_params = {
    tags: tags,
  };
  return fetch(
    S123.DOTNET_HOST + "/api/Tags/GetAllTags" + S123.formatParams(get_params),
    S123.updateOptions()
  ).then((response) => response.json());
};

S123.NET_GetAllTagStructures = async function (tags = "") {
  const get_params = {
    tags: tags,
  };
  return fetch(
    S123.DOTNET_HOST +
      "/api/Tags/GetAllTagStructures" +
      S123.formatParams(get_params),
    S123.updateOptions()
  ).then((response) => response.json());
};

S123.NET_UpdateTags = async function (calculation_guid, tags) {
  const get_params = {
    calculationGuid: calculation_guid,
    tags: tags,
  };

  return fetch(
    S123.DOTNET_HOST + "/api/Tags/UpdateTags" + S123.formatParams(get_params),
    S123.updateOptions()
  ).then((response) => response.json());
};

S123.NET_AddTags = async function (calculation_guid, tags, idParentTag) {
  const get_params = {
    calculationGuid: calculation_guid,
    tags: tags,
    idParentTag: idParentTag,
  };

  return fetch(
    S123.DOTNET_HOST + "/api/Tags/AddTags" + S123.formatParams(get_params),
    S123.updateOptions()
  ).then((response) => response.json());
};

S123.NET_GetUserInfo = async function () {
  return fetch(
    S123.DOTNET_HOST + "/api/Users/GetUserInfo",
    S123.updateOptions()
  )
    .then((response) => response.json())
    .catch((error) => null);
};

S123.NET_GetUserBusinessFunctions = async function () {
  return fetch(
    S123.DOTNET_HOST + "/api/BusinessFunctions/GetForUser",
    S123.updateOptions()
  )
    .then((response) => response.json())
    .catch((error) => null);
};

S123.NET_CopyCalculationToLinkedServer = async function (guidsList = []) {
  return fetch(
    S123.DOTNET_HOST + "/api/Calculation/CopyCalculations2LinkedServer",
    S123.updateOptions({
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(guidsList),
    })
  )
    .then((response) => response.json())
    .catch((error) => {});
};

S123.NET_SendSMSViberEmail = async function (sendingData) {
  const headers = {
    "Content-Type": "application/json",
    accept: "application/octet-stream",
  };

  return fetch(
    S123.DOTNET_HOST + "/api/StructureUrls/SendSMSViberEmail",
    S123.updateOptions({
      method: "POST",
      headers,
      body: JSON.stringify(sendingData),
    })
  )
    .then((response) => response.json())
    .catch((error) => null);
};

S123.NET_AddTaskToCalculation = async function (tasks_data) {
  return fetch(
    S123.DOTNET_HOST + "/api/Calculation/AddTaskToCalculation",
    S123.updateOptions({
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(tasks_data),
    })
  ).then((response) => response.json());
};

S123.NET_CreateOrUpdateCalculation = async function (form_data) {
  const get_params = {};

  if (form_data.get("guid")) get_params["guid"] = form_data.get("guid");
  if (form_data.get("name")) get_params["name"] = form_data.get("name");
  if (form_data.get("isSaved"))
    get_params["isSaved"] = form_data.get("isSaved");
  if (form_data.get("projectGuid"))
    get_params["projectGuid"] = form_data.get("projectGuid");
  if (form_data.get("guidParentCalculation"))
    get_params["guidParentCalculation"] = form_data.get(
      "guidParentCalculation"
    );

  if (form_data.get("idCalculationType"))
    get_params["idCalculationType"] = form_data.get("idCalculationType");

  const company_data = localStorage.getItem("company");
  //console.log(company_data);
  if (company_data) {
    const company_id = JSON.parse(company_data).id;

    if (company_id) {
      get_params["idCompany"] = company_id;
    }
  }

  return fetch(
    S123.DOTNET_HOST +
      "/api/Calculation/CreateOrUpdateCalculation" +
      S123.formatParams(get_params),
    S123.updateOptions({
      method: "POST",
      credentials: "include",
      body: form_data,
    })
  )
    .then((response) => response.json())
    .catch((error) => null);
};

S123.NET_AddFolderWithFiles = async function (form_data) {
  const get_params = {};

  if (form_data.get("calculationGuid"))
    get_params["calculationGuid"] = form_data.get("calculationGuid");
  if (form_data.get("folderName"))
    get_params["folderName"] = form_data.get("folderName");

  return fetch(
    S123.DOTNET_HOST +
      "/api/Calculation/AddFolderWithFiles" +
      S123.formatParams(get_params),
    S123.updateOptions({
      method: "POST",
      credentials: "include",
      body: form_data,
    })
  )
    .then((response) => response.json())
    .catch((error) => null);
};

S123.NET_SearchCalculations = async function (searchString) {
  const get_params = {
    searchString: searchString,
  };

  return fetch(
    S123.DOTNET_HOST +
      "/api/Calculation/SearchCalculations" +
      S123.formatParams(get_params),
    S123.updateOptions()
  ).then((response) => response.json());
};

S123.NET_DeleteCalculation = async function (guid) {
  const get_params = {
    guid: guid,
  };

  return fetch(
    S123.DOTNET_HOST +
      "/api/Calculation/DeleteCalculation" +
      S123.formatParams(get_params),
    S123.updateOptions()
  ).then((response) => response.json());
};

S123.NET_DeleteCalculationFile = function (guid, file_name) {
  const get_params = {
    guid: guid,
    fileName: file_name,
  };

  return fetch(
    S123.DOTNET_HOST +
      "/api/Calculation/DeleteCalculationFile" +
      S123.formatParams(get_params),
    S123.updateOptions()
  ).then((response) => response.json());
};

S123.NET_DeleteFileOrFolder = async function (key) {
  const get_params = { key };

  return fetch(
    S123.DOTNET_HOST +
      "/api/Files/DeleteFileOrFolder" +
      S123.formatParams(get_params),
    S123.updateOptions()
  );
};

S123.NET_GetCalculationFileExcel = async function (calculation_guid) {
  const url =
    S123.DOTNET_HOST +
    "/api/Calculation/GetExcelFileFromCalculation" +
    S123.formatParams({ calculationGuid: calculation_guid });
  window.open(url, "_blank");
};

S123.NET_GetCalculationFilesArchive = async function (calculation_guid) {
  /*return fetch(S123.DOTNET_HOST + "/api/Calculation/GetCalculationFilesArchive" + S123.formatParams({"guid": calculation_guid}), S123.updateOptions() ) // FETCH BLOB FROM IT
    .then((response) => response.blob())
    .then((blob) => { // RETRIEVE THE BLOB AND CREATE LOCAL URL
      var _url = window.URL.createObjectURL(blob);
      window.open(_url, "_blank").focus(); // window.open + focus
    }).catch((err) => {
      console.log(err);
    });*/
  const url =
    S123.DOTNET_HOST +
    "/api/Calculation/GetCalculationFilesArchive" +
    S123.formatParams({ guid: calculation_guid });
  window.open(url, "_blank");
};

S123.NET_CopyCalculation = async function (calculation_guid) {
  const get_params = {
    guid: calculation_guid,
  };

  return fetch(
    S123.DOTNET_HOST +
      "/api/Calculation/CopyCalculation" +
      S123.formatParams(get_params),
    S123.updateOptions()
  ).then((response) => response.json());
};

S123.NET_AddForUser = async function (data) {
  const company_data = localStorage.getItem("company");

  if (company_data) {
    const company_id = JSON.parse(company_data).id;
    data["idCompany"] = company_id;
  }

  return fetch(
    S123.DOTNET_HOST + "/api/Projects/AddForUser",
    S123.updateOptions({
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
  ).then((response) => response.json());
};

S123.NET_Projects_GetChats = async function (projectGuid) {
  const get_params = {
    projectGuid: projectGuid,
  };

  return fetch(
    S123.DOTNET_HOST + "/api/Projects/GetChats" + S123.formatParams(get_params),
    S123.updateOptions()
  ).then((response) => response.json());
};

S123.NET_StructureUrls_CreateQRCodeLink = async function (
  chatGuid,
  guidCalculation,
  room,
  extraUrlParams = ""
) {
  let data = {
    chatGuid: chatGuid,
    guidCalculation: guidCalculation,
    room: room,
    lifeTimeHours: 24,
    type: "3D",
    extraUrlParams: extraUrlParams,
  };

  return fetch(
    S123.DOTNET_HOST + "/api/StructureUrls/CreateQRCodeLink",
    S123.updateOptions({
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
  ).then((response) => response.json());
};

/// METHODS ///

S123.helpers = [];
S123.animate = function () {
  const delta = S123.clock.getDelta();
  S123.load_sprites.forEach((sprite) => {
    sprite.material.rotation -= 4 * delta;
  });

  S123.helpers.forEach((helper) => {
    helper.update();
  });
};

S123.last_calculation_status = {};

//S123.texture_load = 0;
S123.loadTexture = async function (
  texture_path,
  resolution = S123.iik_max_texture_size,
  as_url = false,
  load_mip = true
) {
  if (!S123.enableTextures) return;

  if (texture_path in S123.texture_cache) {
    if (S123.texture_cache[texture_path]) {
      const text_clone = S123.texture_cache[texture_path].clone();
      text_clone.needsUpdate = true;
      return text_clone;
    } else {
      return null;
    }
  }

  let texture_url = texture_path;
  if (!as_url)
    texture_url = S123.NET_DownloadFile_URL(
      texture_path,
      //load_mip ? 4 :
      texture_path.includes("cropSize") ? "" : resolution
    );
  const texture = await new S123.THREE.TextureLoader(S123.loadingManager)
    .loadAsync(texture_url)
    .catch((error) => null);
  if (texture) {
    texture.wrapS = texture.wrapT = S123.THREE.RepeatWrapping;
  } else {
    console.log("Can't load:", texture_path, texture);
  }

  S123.texture_cache[texture_path] = texture;
  return S123.texture_cache[texture_path];
};

S123.loadTextureFromJson = async function (material_json, system_name) {
  const texture_json = material_json[system_name];

  const maxWebResolution = texture_json.maxWebResolution
    ? texture_json.maxWebResolution
    : S123.iik_max_texture_size;
  let texture = await S123.loadTexture(
    texture_json.fileName.replace("&maxSize=512", ""),
    maxWebResolution
  );

  if (texture) {
    let uv_scale_x = material_json?.uvs?.scale ? material_json.uvs.scale.x : 1;
    let uv_scale_y = material_json?.uvs?.scale ? material_json.uvs.scale.y : 1;

    if (texture_json?.realSize) {
      //S123.Log('real size', texture_json.realSize)
      uv_scale_x /= texture_json.realSize.x;
      uv_scale_y /= texture_json.realSize.y;
    }

    // if (system_name == "alpha") {
    //   repeatTexture(texture, uv_scale_x, uv_scale_y);
    // } else {
    texture.repeat.set(uv_scale_x, uv_scale_y);
    // }

    if (material_json.uvs?.offset?.x && material_json.uvs?.offset?.y) {
      texture.offset.set(
        material_json.uvs.offset.x,
        material_json.uvs.offset.y
      );
    }
    if (material_json.json?.uvs?.rotation) {
      texture.rotation = S123.THREE.Math.degToRad(material_json.uvs.rotation);
    }
    //console.log(threejs_name, material[threejs_name])
    texture.needsUpdate = true;
  }
  return texture;
};

S123.material_cache = {};

S123.getParentContainer = function (three_object) {
  let obj = three_object;
  while (obj.type != "container") {
    if (obj.parent == null) return null;

    obj = obj.parent;
  }
  return obj;
};

function logPersitent(...args) {
  let log = JSON.parse(localStorage.getItem("log"));
  if (!Array.isArray(log)) log = [];
  log.push(...args);
  localStorage.setItem("log", JSON.stringify(log));
}

S123.getToken = function () {
  const tokenJSON = localStorage.getItem(S123.auth_key);
  const token = tokenJSON && JSON.parse(tokenJSON);

  if (token) {
    return token;
  } else {
    throw new Error("Token doesn't exist in local storage");
  }
};

async function setupSignalConnection(projectGuid, roomGuid) {
  S123.signal_connection = new HubConnectionBuilder()
    .withUrl(
      `${S123.HOST}/rest/chatHub?userGuid=${S123.user.guid}&projectGuid=${projectGuid}&roomGuid=${roomGuid}`
    )
    .withAutomaticReconnect()
    .build();

  async function start() {
    try {
      await S123.signal_connection.start();
      S123.Log("SignalR Connected.");
    } catch (err) {
      S123.Log(err);
      setTimeout(start, 5000);
    }
  }

  S123.signal_connection.onclose(async () => {
    await start();
  });

  // Start the connection.
  await start();

  S123.Log("connection", S123.signal_connection);
}

S123.authorize = async function (projectGuid, roomGuid) {
  const network = new Network();

  S123.signal_connection = null;
  S123.user = await network.get("/api/Users/GetUserInfo", { silent: true });

  if (!S123.user) {
    console.error("Cannot fetch user info");
    return;
  }

  setupSignalConnection(projectGuid, roomGuid);

  // Try ro refresh the token every minute
  setInterval(() => {
    const token = S123.getToken();
    // Continue only if there's left less than 10 minutes until the token expiration
    if (Date.now() > token.expires_at * 1000 - 10 * 60 * 1000) {
      network.refreshToken();
    }
  }, 60 * 1000);
};

S123.loadMaterialFromJson = function (material_json) {
  const key = JSON.stringify(material_json);
  if (key in S123.material_cache) return S123.material_cache[key];

  if (
    S123.special_material &&
    material_json.material_guid == "7afcfd47-d8e9-4621-b8a1-e75b2c21ba0b"
  ) {
    S123.material_cache[key] = S123.special_material;
    return S123.special_material;
  }

  let color = new S123.THREE.Color(material_json.color);
  const material = new S123.THREE.MeshStandardMaterial({
    color: color,
    roughness: 0.6,
    metalness: 0.2,
    envMapIntensity: 0.8,
    aoMapIntensity: 0.8,
    lightMapIntensity: 0.8,
  });

  S123.UpdateMaterialAsyncFromJson(material, material_json);
  S123.material_cache[key] = material;
  return material;
};

S123.UpdateMaterialAsyncFromJson = async function (material, material_json) {
  const bake_max_size = material_json.bake_max_size ? bake_max_size : 512;

  // Добавляем карту запекания на материал
  let bake_texture;
  if (material_json.bake) {
    bake_texture = await S123.loadTexture(
      `calculationResults/${material_json.calculation_guid}/${material_json.bake}`,
      512,
      false,
      false
    );
  } else {
    bake_texture = await S123.loadTexture(
      S123.COMMON_PATH + "/web-data/def_light.jpg",
      16,
      true,
      false
    );
  }

  material.lightMap = bake_texture;
  material.aoMap = bake_texture;
  material.needsUpdate = true;

  //Добавляем карту окружения на материал
  let envMap_texture = await S123.loadTexture(
    `calculationResults/${material_json.calculation_guid}/ref_map.jpg`,
    bake_max_size,
    false,
    false
  );

  if (envMap_texture) {
    envMap_texture.mapping = S123.THREE.EquirectangularReflectionMapping;
    material.envMap = envMap_texture;
    material.needsUpdate = true;
  }

  //Задаем оффсеты теустур
  if (material_json.uvs) {
    material_json.uvs.offset = material_json.uvs.offset || { x: 0, y: 0 };
    material_json.uvs.scale = material_json.uvs.scale || { x: 1, y: 1 };
    material_json.uvs.rotation = material_json.uvs.rotation || 0;
  } else {
    material_json.uvs = {
      offset: { x: 0, y: 0 },
      scale: { x: 1, y: 1 },
      rotation: 0,
    };
  }

  //Задаем параметры матриала
  if (material_json.opacity) {
    material.opacity = parseFloat(material_json.opacity);
    material.needsUpdate = true;
  }

  if (material_json.web?.side) {
    if (material_json.web.side.toLowerCase() == "frontside")
      material.side = S123.THREE.FrontSide;
    if (material_json.web.side.toLowerCase() == "backside")
      material.side = S123.THREE.BackSide;
    if (material_json.web.side.toLowerCase() == "doubleside")
      material.side = S123.THREE.DoubleSide;
    material.needsUpdate = true;
  }

  let inherit_params = { roughnessValue: "roughness" };

  for (let [param_name_system, param_name_threejs] of Object.entries(
    inherit_params
  )) {
    if (param_name_system in material_json)
      material_json.web[param_name_threejs] = material_json[param_name_system];
  }

  let param_names = [
    "lightMapIntensity",
    "envMapIntensity",
    "transparent",
    "alphaTest",
    "depthTest",
    "depthWrite",
    "emissiveMapIntensity",
    "roughness",
  ];

  for (let param_name of param_names) {
    if (material_json.web && param_name in material_json.web) {
      material[param_name] = material_json.web[param_name];
      material.needsUpdate = true;
    }
  }

  //Задаем текстуры на материал
  let textures_names = [
    //['system_name', 'three.js_name']
    ["diffuse", "map"],
    ["normal", "normalMap"],
    ["emissive", "emissiveMap"],
    ["alpha", "alphaMap"],
    ["roughness", "roughnessMap"],
    ["metallic", "metalnessMap"],
  ];

  // см https://threejs.org/docs/#api/en/textures/Texture.offset
  // В обычных старых убер-материалах three.js (в отличие от NodeMaterial) все текстуры разделяют одну uv-transform matrix
  // Какую именно, определяется в соответствии с приоритетом, указанным в массиве threejsUVPriority. Для работы перфорации на изделиях самый простой способ --
  // "поднять приоритет настроек uv альфа-карты", чтоб они использовались вместо uv-настроек дифузки. Лучшие альтернативы - препроцессинг текстур (см функцию repeatTexture)
  // либо переход на NodeMaterial.
  (function raiseAlphaMapRealsize() {
    console.log(material_json);

    if (material_json.alpha?.realSize) {
      const threejsUVPriority = [
        "diffuse",
        "normal",
        "roughness",
        "metallic",
        "alpha",
        "emissive",
      ];

      const texture_json_to_alter =
        material_json[
          textures_names
            .filter(([system_name]) => system_name in material_json)
            .sort(
              ([a], [b]) =>
                threejsUVPriority.indexOf(a) - threejsUVPriority.indexOf(b)
            )[0][0]
        ];

      console.log("alter");
      console.log(texture_json_to_alter);

      if (texture_json_to_alter) {
        texture_json_to_alter.realSize = material_json.alpha.realSize;
      }
    }
  })();

  for (let textures_name of textures_names) {
    let system_name = textures_name[0];
    let threejs_name = textures_name[1];

    if (system_name in material_json) {
      if (typeof material_json[system_name] === "object") {
        if (
          !material_json[system_name]?.fileName.startsWith(
            "calculationResults/"
          )
        )
          material_json[
            system_name
          ].fileName = `calculationResults/${material_json.material_guid}/${material_json[system_name].fileName}`;
        material[threejs_name] = await S123.loadTextureFromJson(
          material_json,
          system_name
        );
        material.needsUpdate = true;
      } else {
        material[threejs_name] = await S123.loadTexture(
          `calculationResults/${material_json.material_guid}/${material_json[system_name]}`,
          512
        );

        if (material[threejs_name]) {
          let uv_scale_x = material_json?.uvs?.scale
            ? material_json.uvs.scale.x
            : 1;
          let uv_scale_y = material_json?.uvs?.scale
            ? material_json.uvs.scale.y
            : 1;

          //if ("real_size_x" in material_json) uv_scale_x /= material_json.real_size_x;
          //if ("real_size_y" in material_json) uv_scale_y /= material_json.real_size_y;
          if (material_json?.real_size) {
            if (material_json?.real_size[system_name]) {
              //console.log('real size', system_name, material_json.real_size[system_name])
              uv_scale_x /= material_json.real_size[system_name].x;
              uv_scale_y /= material_json.real_size[system_name].y;
            } else if (material_json?.real_size["default"]) {
              uv_scale_x /= material_json.real_size["default"].x;
              uv_scale_y /= material_json.real_size["default"].y;
            }
          }

          material[threejs_name].repeat.set(uv_scale_x, uv_scale_y);
          material[threejs_name].offset.set(
            material_json.uvs.offset.x,
            material_json.uvs.offset.y
          );
          material[threejs_name].rotation = S123.THREE.Math.degToRad(
            material_json.uvs.rotation
          );
          //console.log(threejs_name, material[threejs_name])
          material.needsUpdate = true;
        }
      }
    }
  }
};

export class S123Product {
  constructor(
    calculation_guid,
    debug_mode = false,
    scene = null,
    content = null,
    related_guid = null,
    cache_guid = true,
    parent_project = null,
    enableTextures = true
  ) {
    this.calculation_guid = calculation_guid;

    this.debug = debug_mode;
    S123.debug = debug_mode;

    S123.enableTextures = enableTextures;

    this.container = new S123.THREE.Group();
    this.container.type = "container";
    this.container.product = this;

    this.product_container = new S123.THREE.Group();
    this.product_container.type = "product_container";
    this.product_container.related_guid = related_guid;
    this.container.add(this.product_container);

    this.iik_inputs = [];
    this.block_update = false;
    this.loadCalculationAsyncPromise = this.loadCalculationAsync(content);
    this.checkCalculationStatePromise = this.checkCalculationState();

    this.stat_json = {};
    this.parent_project = parent_project;

    if (cache_guid) {
      guidsCache.add(calculation_guid);
    }
    // console.log("NEW PRODUCT");
  }

  async loadCalculationAsync(content = null) {
    let calculation_guid = this.calculation_guid;
    let container = this.container;

    //Загружаем необходимые файлы
    console.log("loading");

    try {
      this.stat_json = await S123.NET_GetCalculationStat(calculation_guid);
      console.log("stat_json", this.stat_json);
    } catch (e) {
      console.log(e);
    }
    var info_url = S123.NET_DownloadFile_URL(
      `calculationResults/${calculation_guid}/info.json`
    );
    var info_json = await fetch(info_url)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Something went wrong");
      })
      .catch((error) => null);

    this.info_json = info_json;
    //console.log("info_json", info_json);

    if (info_json) {
      if (info_json.project == "sphere_360.png") {
        //S123.special_material = await S123.loadMaterial(calculation_guid, {"material": `s123mat://${calculation_guid}`})
        //console.log("building shpere");
        const sphere_texture = await S123.loadTexture(
          `calculationResults/${calculation_guid}/${info_json.project}`,
          2048,
          false,
          false
        );
        const geometry = new S123.THREE.SphereGeometry(30, 32, 16);
        const material = new S123.THREE.MeshBasicMaterial({
          color: 0xffffff,
          side: S123.THREE.DoubleSide,
          map: sphere_texture,
          transparent: true,
        });
        const local_container = new S123.THREE.Mesh(geometry, material);
        container.add(local_container);
      }

      if (info_json.project == "material.s123mat") {
        let material_guid = this.calculation_guid;
        let material_url = S123.NET_DownloadFile_URL(
          `calculationResults/${material_guid}/material.s123mat`
        );
        let material_json = await fetch(material_url).then((response) =>
          response.json()
        );

        material_json.calculation_guid = this.calculation_guid;
        material_json.material_guid = material_guid;

        S123.special_material = S123.loadMaterialFromJson(material_json);

        /*
        S123.special_material = await S123.loadMaterial(calculation_guid, {
          material: `s123mat://${calculation_guid}`,
        });*/
        const local_container = new S123Product(S123.materialCubeGUID)
          .container;
        container.add(local_container);

        const project_url = S123.NET_DownloadFile_URL(
          `calculationResults/${calculation_guid}/${info_json.project}`
        );
        this.project_json = await fetch(project_url)
          .then((response) => {
            if (response.ok) {
              return response.json();
            }
            throw new Error("Something went wrong");
          })
          .catch((error) => null);
      } else {
        /*
        var front_url = S123.NET_DownloadFile_URL(`calculationResults/${calculation_guid}/product.s123front`);
        var front_json = await fetch(front_url).then(response => {
          if (response.ok) {
            return response.json();
          }
          throw new Error('Something went wrong');
        }).catch(error => null);

        this.front_json = front_json*/

        const project_url = S123.NET_DownloadFile_URL(
          `calculationResults/${calculation_guid}/${info_json.project}`
        );
        this.project_json = await fetch(project_url)
          .then((response) => {
            if (response.ok) {
              return response.json();
            }
            throw new Error("Something went wrong");
          })
          .catch((error) => null);

        searchForGuids(this.project_json);

        //Создаем зеленый куб загрузки
        const geometry = new S123.THREE.BoxGeometry(
          info_json.size.x,
          info_json.size.y,
          info_json.size.z
        );
        const load_cube_material = new S123.THREE.MeshStandardMaterial({
          color: 0x00ff00,
          opacity: 1,
          emissive: 0x00ff00,
          transparent: false,
          emissiveIntensity: 0.7,
        });
        const load_cube = new S123.THREE.Mesh(geometry, load_cube_material);
        load_cube.position.y = info_json.size.y * 0.5;
        this.container.add(load_cube);
        this.load_cube = load_cube;

        const top_y = info_json.size.y;

        //Создаем спрайт анимации загрузки
        const load_icon = new S123.THREE.TextureLoader(
          S123.loadingManager
        ).load(S123.COMMON_PATH + "/web-data/loading.png");
        /*
        var load_sprite = new S123.THREE.Sprite( material );
        load_sprite.position.set(0, parseInt(info_json.size.y) + 130, 0)
        load_sprite.scale.set(200, 200, 1)
        container.add( load_sprite );

        S123.load_sprites.push(load_sprite)
        */

        //Создаем спрайт для отображения статуса
        const material = new S123.THREE.SpriteMaterial({
          map: load_icon,
          color: 0xffff00,
        });
        const stat_sprite = new S123.THREE.Sprite(material);
        stat_sprite.position.set(0, top_y + 0.13, 0);
        stat_sprite.scale.set(0.2, 0.2, 1);
        stat_sprite.visible = false;
        this.container.add(stat_sprite);
        this.stat_sprite = stat_sprite;
        S123.load_sprites.push(this.stat_sprite);

        //Создаем спрайт для отображения ошибки
        const error_sprite = new S123.THREE.Sprite(material);
        error_sprite.position.set(0, top_y + 0.13, 0);
        error_sprite.scale.set(0.2, 0.2, 1);
        error_sprite.visible = false;
        this.container.add(error_sprite);
        this.error_sprite = error_sprite;
        S123.load_sprites.push(this.error_sprite);

        if (this.debug && !this.parent_project) {
          //if (show_load_cube) {
          // this.box_helper = new S123.THREE.BoxHelper( load_cube, 0x00ff00 );
          // Viewer.scene.add( this.box_helper );
          //}

          const grid_helper = new S123.THREE.GridHelper(10, 10);
          grid_helper.userData.isHelper = true;
          grid_helper.scale.set(1, 1, 1);
          this.container.add(grid_helper);

          const axesHelper = new S123.THREE.AxesHelper(6);
          axesHelper.userData.isHelper = true;
          this.container.add(axesHelper);
        }

        if (content == null) {
          try {
            S123.Log("Начало расчета!", this.calculation_guid);
            await this.calculate_IIK();
            S123.Log("Конец расчета!", this.calculation_guid);
          } catch (error) {
            console.error(error);
            this.block_calculation = false;
          }
        } else {
          this.front_json = content;
          for (var key in this.front_json) {
            await this.create_calculation_node(
              this.front_json[key],
              this.product_container,
              this.calculation_guid,
              key
            );
          }
        }

        load_cube.visible = false;
      }
    }

    const event = new CustomEvent("onloadproduct", {
      detail: { product: this },
    });
    document.dispatchEvent(event);

    if (S123.signal_connection)
      S123.signal_connection.on(
        `CalculationUpdated_${this.calculation_guid}`,
        (data) => {
          //console.log('new data', data)
          this.checkCalculationState();
        }
      );
  }

  checkCalculationState = async function () {
    const calculation_guid = this.calculation_guid;
    const container = this.product_container;
    const _this = this;
    let last_task = null;

    if (!this.block_update) {
      const stat_json = await S123.NET_GetCalculationStat(calculation_guid);
      //console.log(stat_json);
      if (stat_json) Object.assign(this.stat_json, stat_json);
      else throw new Error("NET_GetCalculationStat returns no data!");

      last_task =
        stat_json.calculationSessions[stat_json.calculationSessions.length - 1]
          ?.calculationTasks[0];
    }
    if (!this.block_update && this.stat_sprite && this.error_sprite) {
      if (
        last_task &&
        [1, 2].includes(S123.last_calculation_status[calculation_guid]) &&
        last_task.taskState == 3
      ) {
        //console.log("Calculation complete");
        container.remove(...container.children);
        S123.last_calculation_status[calculation_guid] = null;
        _this.stat_sprite.visible = false;
        _this.error_sprite.visible = false;
        _this.loadCalculationAsync();
      } else {
        if (last_task && [1, 2].includes(last_task?.taskState)) {
          this.stat_sprite.visible = true;
          this.error_sprite.visible = false;
        } else {
          this.stat_sprite.visible = false;
        }

        if (last_task && last_task.taskState == 4) {
          //this.error_sprite.visible = true;
          this.stat_sprite.visible = false;
        } else {
          this.error_sprite.visible = false;
        }

        if (last_task)
          S123.last_calculation_status[calculation_guid] = last_task.taskState;
      }
    }

    /*
    clearTimeout(this.check_timeout);
    this.check_timeout = setTimeout(() => {
      this.checkCalculationState();
    }, 1000);*/
  };

  async calculate_IIK(event, event_value, show_load_cube = true) {
    if (this.block_calculation) return;
    if (!this.project_json) {
      console.error("Product.project_json is not exist");
    }

    this.block_calculation = true;
    //const project_json = this.project_json;
    const container = this.container;
    const calculation_guid = this.calculation_guid;

    //if (project_json.type != "SCENE"){
    //Прописываем гуиды компонентов на случай, если они не заданы
    //for (var ind in project_json.components ){
    //  var component = project_json.components[ind]
    //  if (!component.guid) component.guid = S123.uuidv4()
    //}

    if (S123Core.Filesystem.GetFileContentsAsync == null) {
      S123Core.Filesystem.GetFileContentsAsync = async function (file) {
        let file_url = null;
        if (file.startsWith("http")) file_url = file;
        else if (file.startsWith("s123://"))
          file_url = S123.NET_DownloadFile_URL(file.replace("s123://", ""));
        else
          file_url = S123.NET_DownloadFile_URL(
            `calculationResults/${calculation_guid}/${file}`
          );
        let file_data = await fetch(file_url)
          .then((response) => {
            if (response.ok) {
              return response.text();
            }
            throw new Error("Something went wrong");
          })
          .catch((error) => null);
        if (file_data == null) file_data = "";
        return file_data;
      };
    }

    var iik_instance = new S123Core.IIK.IIKCore();
    //S123Core.IIK.instance.has_geometry = false;
    iik_instance.has_related = true;

    if (this.project_json.components) {
      for (const component of this.project_json.components) {
        if (component.modifier?.child?.project) {
          delete component.modifier.child.project;
        }
      }
    }

    iik_instance.project = JSON.parse(JSON.stringify(this.project_json));
    iik_instance.project.guid = this.calculation_guid;
    iik_instance.inputs = this.iik_inputs;
    iik_instance.related_inputs = this.iik_related_inputs;
    iik_instance.related_event = this.iik_related_event;

    iik_instance.max_texture_size = S123.iik_max_texture_size;

    iik_instance.is_detailed_materials = S123.iik_has_detailed_materials;
    if (this.parent_project) iik_instance.parent_project = this.parent_project;
    //window.IIK.frontend = front_json

    await iik_instance.Prepare();
    if (event && event_value) {
      S123.Log("EVENT", event, event_value);
      await iik_instance.Invoke(event, event_value);
    }
    await iik_instance.Calculate();

    this.front_json = iik_instance.frontend;

    this.iik_inputs = iik_instance.inputs;
    this.iik_related_inputs = iik_instance.related_inputs;
    this.iik_related_event = iik_instance.related_event;
    //console.log('iik_inputs', this.iik_inputs )
    this.iik_outputs = iik_instance.outputs;
    this.iik_project_json = iik_instance.project;

    //console.log("related_inputs", this.iik_related_inputs);
    //console.log('this.guid', calculation_guid)
    //console.log('inputs data', this.project_json.graph)
    //console.log('iik_instance', iik_instance)

    /*} else {
      console.log('NO IIK WAS CALCULATED')
    }*/

    this.product_container.remove(...this.product_container.children);
    //Строим изделие по файлу отображения

    for (let key in this.front_json) {
      await this.create_calculation_node(
        this.front_json[key],
        this.product_container,
        this.calculation_guid,
        key
      );
    }

    const product_update_event = new CustomEvent("productupdate", {
      detail: { product: this },
    });
    document.dispatchEvent(product_update_event);

    this.block_calculation = false;
  }

  async calculate(
    full_calculation = true,
    event = null,
    event_value = null,
    local = false,
    show_load_cube = true
  ) {
    if (this.block_calculation) {
      S123.Log("Расчет уже инициализирован!");
      return;
    }

    try {
      S123.Log("Начало расчета!", this.calculation_guid);
      await this.calculate_IIK(event, event_value, show_load_cube);
      S123.Log("Конец расчета!", this.calculation_guid);
    } catch (e) {
      console.error(e, e.stack);
      this.block_calculation = false;
    }

    if (full_calculation) {
      if (this.stat_sprite) this.stat_sprite.visible = true;
      this.block_update = true;

      if (!local) {
        const data = await S123.NET_CopyCalculation(this.calculation_guid);
        //console.log("Copy guid:", data.guid);
        this.calculation_guid = data.guid;
      }

      //console.log("Calculation new guid:", this.calculation_guid);
      if (S123.signal_connection)
        S123.signal_connection.on(
          `CalculationUpdated_${this.calculation_guid}`,
          (data) => {
            //console.log('new data', data)
            this.checkCalculationState();
          }
        );

      const new_form_data = new FormData();
      new_form_data.set("guid", this.calculation_guid);

      var generator_data = {
        projectFile: this.info_json.project,
        formats: ["fbx", "usdc"],
        inputs: this.iik_inputs,
        generateRenderData: true,
      };

      var gen_file = new Blob([JSON.stringify(generator_data)], {
        type: "application/json",
      });
      //console.log("new Gen data:", JSON.stringify(generator_data));
      new_form_data.append("files", gen_file, "Generator.json");

      var res = await S123.NET_CreateOrUpdateCalculation(new_form_data);
      //console.log(res);
      S123.model_cache = {};

      const tasks_data = {
        guid: this.calculation_guid,
        calculationTasks: [
          {
            handlerName: "Generator",
            order: 0,
          },
        ],
      };
      const result = await S123.NET_AddTaskToCalculation(tasks_data);
      //console.log(result);

      this.block_update = false;
    }
  }

  async create_calculation_node(
    node_json,
    container,
    calculation_guid,
    node_key
  ) {
    let local_container;
    let material;

    //console.log(node_json)

    if (node_json.type == "shadow_plane" && !node_json.material) {
      node_json.material = "s123mat://d2265cc6-da6c-4889-b0ae-716126c90bc0";
    }

    if (node_json.material_content) {
      node_json.material_content.calculation_guid = this.calculation_guid;
      if (node_json.material?.substring(0, 7) == "s123mat")
        node_json.material_content.material_guid =
          node_json.material.substring(10);
      else node_json.material_content.material_guid = this.calculation_guid;
      //node_json.material_content.color = '#ffffff'
      node_json.material_content.bake = node_json.bake;
      node_json.material_content.uvs = node_json.uvs;

      material = S123.loadMaterialFromJson(node_json.material_content);
    } else if (node_json.material) {
      if (node_json.material.substring(0, 7) == "s123mat") {
        let material_guid = node_json.material.substring(10);
        let material_url = S123.NET_DownloadFile_URL(
          `calculationResults/${material_guid}/material.s123mat`
        );
        let material_json = await fetch(material_url).then((response) =>
          response.json()
        );

        material_json.calculation_guid = this.calculation_guid;
        material_json.material_guid = material_guid;

        material = S123.loadMaterialFromJson(material_json);
      } else {
        const mat_ext = node_json.material.split(".").pop();

        if (["s123mat"].includes(mat_ext)) {
          //var material_guid = node_json.material.substring(10)
          const material_url = S123.NET_DownloadFile_URL(
            `calculationResults/${calculation_guid}/${node_json.material}`
          );
          const material_json = await fetch(material_url).then((response) =>
            response.json()
          );

          material_json.calculation_guid = this.calculation_guid;
          material_json.material_guid = material_guid;

          material = S123.loadMaterialFromJson(material_json);
        }
        //TODO: Добавить поддержку текстур как материалов, если это все еще нужно
      }
    } else {
      let material_json = {};
      material_json.calculation_guid = this.calculation_guid;
      material_json.material_guid = this.calculation_guid;
      material = S123.loadMaterialFromJson(material_json);
    }

    if (node_json.type == "model_group") {
      for (let key in node_json.children) {
        const object = node_json.children[key];
        object.type = "geometry";
      }
    }

    if (node_json.type == "model_node" || node_json.type == "model") {
      if (node_json.file) {
        const model_path = `calculationResults/${calculation_guid}/${node_json.file}`;

        if (model_path in S123.model_cache) {
          //console.log('!!!!', node_json.file)
          //model = S123.model_cache[object.file + ':' + object.node];
        } else {
          const model_url = S123.NET_DownloadFile_URL(model_path);
          let model;

          if (model_path.toLowerCase().endsWith(".fbx")) {
            model = await new S123.FBXLoader(S123.loadingManager).loadAsync(
              model_url
            );
          } else if (
            model_path.toLowerCase().endsWith(".gltf") ||
            model_path.toLowerCase().endsWith(".glb")
          ) {
            model = (
              await new S123.GLTFLoader(S123.loadingManager).loadAsync(
                model_url
              )
            ).scene;
          } else {
            throw new Error(
              `Can't read ${model_url}. Only .fbx, .gltf and .glb extensions are supported.`
            );
          }

          model.traverse(function (child) {
            if (child.type == "Mesh") {
              S123.model_cache[`${model_path}:${child.name}`] = child;
              if (!S123.model_cache[`${model_path}:def_node`])
                S123.model_cache[`${model_path}:def_node`] = child;
            }
          });
          S123.model_cache[model_path] = model;
          /*
          model.traverse( function ( child ) {
            if ( child.type == "Mesh" ) {
              //console.log(`${node_json.file}:${child.name}`)
              S123.model_cache[`${node_json.file}:${child.name}`] = child;
              if (!S123.model_cache[`${node_json.file}:def_node`])
                S123.model_cache[`${node_json.file}:def_node`] = child;
            }
          });

          S123.model_cache[node_json.file] = model;
          */
        }
      }
    }

    if (node_json.type == "group" || node_json.type == "model_group") {
      //создаем контейнер, в случае если нода являеется группой
      local_container = new S123.THREE.Group();
    }

    if (node_json.type == "plane") {
      //создаем плоскость, в случае если нода являеется плоскостью
      const geometry = new S123.THREE.PlaneGeometry(1000, 1000); //размер стандартной плоскости - 1000 мм
      local_container = new S123.THREE.Mesh(geometry, material);
    }

    if (node_json.type == "shadow_plane") {
      //создаем плоскость, в случае если нода являеется плоскостью
      const geometry = new S123.THREE.PlaneGeometry(1, 1); //размер стандартной плоскости - 1000 мм
      let inner_container = new S123.THREE.Mesh(geometry, material);
      inner_container.rotation.x = -Math.PI / 2;
      inner_container.userData.type = "shadow";
      inner_container.userData.guid = node_json.guid;

      if (node_json.data.shadow_type == 0) {
        inner_container.material.map = await S123.loadTexture(
          `calculationResults/${calculation_guid}/${node_json.data.path}`
        );
      }

      local_container = new S123.THREE.Group();
      local_container.add(inner_container);
    }

    if (node_json.type == "camera" && S123.debug) {
      local_container = new S123.THREE.Group();

      const obj = await new S123.FBXLoader(S123.loadingManager).loadAsync(
        S123.COMMON_PATH + "/models/Camera.fbx"
      );

      obj.traverse(function (child) {
        if (child instanceof S123.THREE.Mesh) {
          child.material = material;
          child.name = node_json.guid;
          child.userData.type = "camera";
          child.userData.guid = node_json.guid;
          child.userData.name = node_json.name;

          // child.rotation.z = Math.PI;
        }
      });

      local_container.add(obj);
    }

    if (node_json.type == "light") {
      local_container = new S123.THREE.Group();

      //console.log('color', node_json.color)
      let color = new S123.THREE.Color(node_json.color);

      let light, helper;
      if (node_json.data.light_type == 0) {
        //directional
        light = new S123.THREE.DirectionalLight(color, 1);
        helper = new S123.THREE.DirectionalLightHelper(light, 0.5);
      }

      if (node_json.data.light_type == 1) {
        //point
        light = new S123.THREE.PointLight(color, 1, 0, 2);
        light.power = node_json.data.intensity;
        helper = new S123.THREE.PointLightHelper(light, 0.1);
      }

      if (node_json.data.light_type == 2) {
        //spot
        light = new S123.THREE.SpotLight(color, 1, 0, Math.PI / 3, 0, 2);
        light.power = node_json.data.intensity;
        helper = new S123.THREE.SpotLightHelper(light, 0.1);
      }

      helper.userData.isHelper = true;
      helper.userData.type = "light";
      helper.userData.guid = node_json.guid;

      light.name = node_json.guid;

      //light.rotateX(-Math.PI/2)
      light.position.y = 0;
      //console.log('light',light)
      local_container.add(light);

      if (this.debug && S123.scene) {
        S123.scene.add(helper);
        S123.helpers.push(helper);
      }

      //S123.helpers.matrixAutoUpdate = false
    }

    if (node_json.type == "calculation") {
      //загружаем расчет, в случае если нода является расчетом
      const new_guid = node_json.file.substring(11);
      const new_product = new S123Product(
        new_guid,
        this.debug,
        null,
        node_json.content,
        node_key,
        true,
        this.iik_project_json
      );

      new_product.loadCalculationAsyncPromise?.then(() => {
        local_container.traverse((child) => {
          child.userData.type = "detail";
          child.userData.guid = node_key;
        });
      });

      local_container = new_product.container;

      local_container.name = "model_" + node_key;

      if (this?.iik_related_inputs) {
        //console.log('iik_related_inputs:', this?.iik_related_inputs)
        var inputs = this.iik_related_inputs[node_json.key];
        if (inputs) {
          //console.log('INPUTS:', inputs)
          new_product.iik_inputs = inputs;
        }
      }

      //console.log('%c this.calculation_guid', 'color: #ff0000; font-size:20px', this.calculation_guid)
      //console.log('this.project_json.graph', this.project_json.graph)
      //console.log('this.iik_related_inputs', this.iik_related_inputs )
      //console.log('node_json', node_json)
      //new_product.iik_inputs =
    }

    if (node_json.type == "geometry") {
      const node_geometry = node_json.geometry;
      let geometry;

      if (
        typeof (geometry = cacheGeometryMesh.get(node_geometry.guid)) ==
        "undefined"
      ) {
        geometry = new S123.THREE.BufferGeometry();
        const positions = [];
        const normals = [];
        const uvs = [];
        const uvs2 = [];

        for (let i = 0; i < node_geometry.vertices.length; i++) {
          positions.push(node_geometry.vertices[i].x);
          positions.push(node_geometry.vertices[i].y);
          positions.push(node_geometry.vertices[i].z);

          normals.push(-node_geometry.normals[i].x);
          normals.push(node_geometry.normals[i].y);
          normals.push(-node_geometry.normals[i].z);

          uvs.push(node_geometry.uvs[i].x);
          uvs.push(node_geometry.uvs[i].y);

          if (node_geometry.uvs2.length === node_geometry.uvs.length) {
            uvs2.push(node_geometry.uvs2[i].x);
            uvs2.push(node_geometry.uvs2[i].y);
          } else {
            uvs2.push(0);
            uvs2.push(0);
          }
        }

        geometry.setIndex([...node_geometry.indices].reverse());
        geometry.setAttribute(
          "position",
          new S123.THREE.BufferAttribute(new Float32Array(positions), 3)
        );
        geometry.setAttribute(
          "normal",
          new S123.THREE.BufferAttribute(new Float32Array(normals), 3)
        );
        geometry.setAttribute(
          "uv",
          new S123.THREE.BufferAttribute(new Float32Array(uvs), 2)
        );
        geometry.setAttribute(
          "uv2",
          new S123.THREE.BufferAttribute(new Float32Array(uvs2), 2)
        );

        cacheGeometryMesh.set(node_geometry.guid, geometry);
      } else {
        console.log("cache was used");
      }

      local_container = new S123.THREE.Mesh(geometry, material);
      local_container.name = node_key;
      local_container.material = material;
    }

    if (node_json.type == "model_node" || node_json.type == "model") {
      //загрузка на случай, когда нода является 3Д моделью или нодой 3Д модели
      //Создание 3Д объекта движка
      let org_mesh;

      const model_path = `calculationResults/${calculation_guid}/${node_json.file}`;

      if (node_json.type == "model_node") {
        const node = node_json.node.split("/").pop();

        if (node == "") {
          org_mesh = S123.model_cache[`${model_path}:def_node`];
          //console.log(`${model_path}:def_node`)
        } else {
          org_mesh = S123.model_cache[`${model_path}:${node}`];
          //console.log(`${model_path}:${node}`)
        }
        if (org_mesh == null)
          S123.Log("Указаная нода модели не найдена!", node_json.file, node);
        else {
          const parent = org_mesh.parent;

          local_container = org_mesh.clone();
          local_container.material = material;
        }
      } else {
        org_mesh = S123.model_cache[model_path];

        local_container = org_mesh.clone();
        local_container.traverse(function (child) {
          if (child.type == "Mesh") {
            child.material = material;
          }
        });
      }

      local_container.userData.type = "detail";
      local_container.userData.guid = node_key;
    }

    if (node_json.type == "array") {
      local_container = new S123.THREE.Group();
      const group = new S123.THREE.Group();

      await this.create_calculation_node(
        node_json.component,
        group,
        calculation_guid,
        node_json.component.node
      );

      const prototype = group.children[0];

      for (const transform of node_json.component_transforms) {
        const instance = prototype.clone();
        assignTransform(instance, transform);

        instance.userData.type = "detail";
        instance.userData.guid = node_key;

        local_container.add(instance);
      }
    }

    if (local_container) {
      //добавляем созданный объект в ноду - родителя

      if ("related_guid" in container)
        local_container.related_guid = container.related_guid;

      container.add(local_container);

      //Задаем трансформацию созданного объекта
      assignTransform(local_container, node_json.transform);
    }

    //Рекурсивно подгружаем дочерние объекты, если таковые присутствуют
    if (typeof node_json.children == "object" && node_json.children != null) {
      for (const key of Object.keys(node_json.children)) {
        const object = node_json.children[key];
        object.key = key;

        await this.create_calculation_node(
          object,
          local_container,
          calculation_guid,
          key
        ).then((mesh) => {
          if (node_json.type == "model_group" || node_json.type == "group") {
            mesh.userData.type = "detail";
            mesh.userData.guid = node_json.key ?? object.key;
          }
        });
      }
    }

    if (
      typeof node_json.virtual_objects == "object" &&
      node_json.virtual_objects != null
    ) {
      for (const key of Object.keys(node_json.virtual_objects)) {
        const object = node_json.virtual_objects[key];
        object.key = key;

        await this.create_calculation_node(
          object,
          local_container,
          calculation_guid,
          key
        );
      }
    }

    return local_container;
  }

  remove() {
    //console.log("remove", this.calculation_guid);

    this.block_update = true;
    clearTimeout(this.check_timeout);

    //удалsяем лишние данные
    this.product_container.remove(this.load_cube);

    //Удаляем лишние спрайты если они есть
    this.product_container.remove(this.load_sprite);
    this.product_container.remove(this.stat_sprite);
    this.product_container.remove(this.error_sprite);

    S123.load_sprites = S123.load_sprites.filter(
      (sprite) =>
        ![this.load_sprite, this.stat_sprite, this.error_sprite].includes(
          sprite
        )
    );

    this.container.removeFromParent();
  }
}

function assignTransform(object3d, transform) {
  if (transform) {
    const ts = transform;
    if (ts.scale) object3d.scale.set(ts.scale.x, ts.scale.y, ts.scale.z);

    if (ts.position) {
      ts.position.x = ts.position.x || 0;
      ts.position.y = ts.position.y || 0;
      ts.position.z = ts.position.z || 0;
      //console.log(ts.position)
      object3d.position.set(ts.position.x, ts.position.y, ts.position.z);
    }
    if (ts.rotation) {
      ts.rotation.x = ts.rotation.x || 0;
      ts.rotation.y = ts.rotation.y || 0;
      ts.rotation.z = ts.rotation.z || 0;

      if ("w" in ts.rotation) {
        ts.rotation.w = ts.rotation.w || 0;
        object3d.quaternion.set(
          ts.rotation.x,
          ts.rotation.y,
          ts.rotation.z,
          ts.rotation.w
        );
      } else
        object3d.rotation.set(
          S123.THREE.Math.degToRad(ts.rotation.x),
          S123.THREE.Math.degToRad(ts.rotation.y),
          S123.THREE.Math.degToRad(ts.rotation.z)
        );
    }
  }
}

class Cache extends Map {
  constructor(options = {}) {
    super();

    options.sizeLimit ??= Infinity;
    this.sizeLimit = options.sizeLimit;
  }

  set(key, value) {
    if (this.size >= this.sizeLimit) {
      this.delete(this.keys().next().value);
    }

    super.set(key, value);
  }
}

const cacheGeometryMesh = new Cache({ sizeLimit: 1000 });

function searchForGuids(node) {
  if (typeof node == "object" && node !== null) {
    for (const value of Object.values(node)) {
      searchForGuids(value);
    }
  } else if (typeof node == "string") {
    const execResult = /s123mat:\/\/([a-fA-F0-9\-]+)/.exec(node);

    if (execResult != null) {
      guidsCache.add(execResult[1]);
    }
  }
}

const { repeatTexture } = (() => {
  const canvas = document.createElement("canvas");
  const ctx = canvas.getContext("2d");
  const patternCanvas = document.createElement("canvas");
  const patternCanvasCtx = patternCanvas.getContext("2d");

  function repeatTexture(texture, repeatX, repeatY = repeatX) {
    patternCanvas.width = texture.image.width / repeatX;
    patternCanvas.height = texture.image.height / repeatY;

    patternCanvasCtx.drawImage(
      texture.image,
      0,
      0,
      patternCanvas.width,
      patternCanvas.height
    );

    canvas.width = texture.image.width;
    canvas.height = texture.image.height;

    const pattern = ctx.createPattern(patternCanvas, "repeat");
    ctx.fillStyle = pattern;
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    texture.image = canvas;
    // texture.wrapS = THREE.RepeatWrapping;
    // texture.wrapT = THREE.RepeatWrapping;
    texture.needsUpdate = true;
  }

  return { repeatTexture };
})();

import mainIcon from "./assets/main.ico";
import labIcon from "./assets/lab.ico";

(function () {
  var link =
    document.querySelector("link[rel*='icon']") ||
    document.createElement("link");
  link.type = "image/x-icon";
  link.rel = "shortcut icon";
  link.href = S123.HOST.includes("lab") ? labIcon : mainIcon;
  document.getElementsByTagName("head")[0].appendChild(link);
})();
