const loadedModules = new Map();

export default async function fetchUMD(url, module = {exports: {}}) {
  if (loadedModules.has(url)) {
    return loadedModules.get(url);
  }

  const moduleSource = await (await fetch(url)).text();
  new Function('module', 'exports', moduleSource).call(module, module, module.exports);
  return module.exports;
}
