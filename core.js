(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["S123Core"] = factory();
	else
		root["S123Core"] = factory();
})(this, function() {
return /******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/Core/changable.ts":
/*!*******************************!*\
  !*** ./src/Core/changable.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.Changable = void 0;
class Changable {
    UpdateFrom(data) {
        const keys = Object.keys(this);
        for (const key of keys) {
            if (data.hasOwnProperty(key)) {
                if ((typeof this[key]) == "object" && data[key] != null && this[key] instanceof Changable) {
                    this[key].UpdateFrom(data[key]);
                }
                else {
                    this[key] = data[key];
                }
            }
        }
    }
}
exports.Changable = Changable;


/***/ }),

/***/ "./src/MeshUtils/generator.ts":
/*!************************************!*\
  !*** ./src/MeshUtils/generator.ts ***!
  \************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.Generator = void 0;
const math_1 = __webpack_require__(/*! ../math */ "./src/math.ts");
const unwrapper_1 = __webpack_require__(/*! ./unwrapper */ "./src/MeshUtils/unwrapper.ts");
const utils_1 = __webpack_require__(/*! ../utils */ "./src/utils.ts");
var Generator;
(function (Generator) {
    var Vector3 = math_1.VMath.Vector3;
    var Vector2 = math_1.VMath.Vector2;
    var TriplanarUnwrapper = unwrapper_1.Unwrapper.TriplanarUnwrapper;
    var Quaternion = math_1.VMath.Quaternion;
    class Triangulator {
        constructor(points) {
            this.m_points = points;
        }
        Triangulate() {
            const indices = [];
            const n = this.m_points.length;
            if (n < 3)
                return indices;
            const V = [];
            if (this.Area() > 0) {
                for (let v = 0; v < n; v++)
                    V[v] = v;
            }
            else {
                for (let v = 0; v < n; v++)
                    V[v] = (n - 1) - v;
            }
            let nv = n;
            let count = 2 * nv;
            for (let m = 0, v = nv - 1; nv > 2;) {
                if ((count--) <= 0)
                    return indices;
                let u = v;
                if (nv <= u)
                    u = 0;
                v = u + 1;
                if (nv <= v)
                    v = 0;
                let w = v + 1;
                if (nv <= w)
                    w = 0;
                if (this.Snip(u, v, w, nv, V)) {
                    let a, b, c, s, t;
                    a = V[u];
                    b = V[v];
                    c = V[w];
                    indices.push(a);
                    indices.push(b);
                    indices.push(c);
                    m++;
                    for (s = v, t = v + 1; t < nv; s++, t++)
                        V[s] = V[t];
                    nv--;
                    count = 2 * nv;
                }
            }
            return indices.reverse();
        }
        Area() {
            const n = this.m_points.length;
            let A = 0;
            let p, q;
            for (p = n - 1, q = 0; q < n; p = q++) {
                const pval = this.m_points[p];
                const qval = this.m_points[q];
                A += pval.x * qval.y - qval.x * pval.y;
            }
            return A * 0.5;
        }
        Snip(u, v, w, n, V) {
            let p = 0;
            const A = this.m_points[V[u]];
            const B = this.m_points[V[v]];
            const C = this.m_points[V[w]];
            if (Number.EPSILON > (((B.x - A.x) * (C.y - A.y)) - ((B.y - A.y) * (C.x - A.x))))
                return false;
            for (p = 0; p < n; p++) {
                if ((p == u) || (p == v) || (p == w))
                    continue;
                const P = this.m_points[V[p]];
                if (Triangulator.InsideTriangle(A, B, C, P))
                    return false;
            }
            return true;
        }
        static InsideTriangle(A, B, C, P) {
            const ax = C.x - B.x;
            const ay = C.y - B.y;
            const bx = A.x - C.x;
            const by = A.y - C.y;
            const cx = B.x - A.x;
            const cy = B.y - A.y;
            const apx = P.x - A.x;
            const apy = P.y - A.y;
            const bpx = P.x - B.x;
            const bpy = P.y - B.y;
            const cpx = P.x - C.x;
            const cpy = P.y - C.y;
            const aCROSSbp = ax * bpy - ay * bpx;
            const cCROSSap = cx * apy - cy * apx;
            const bCROSScp = bx * cpy - by * cpx;
            return ((aCROSSbp >= 0.0) && (bCROSScp >= 0.0) && (cCROSSap >= 0.0));
        }
    }
    Generator.Triangulator = Triangulator;
    class Mesh {
        constructor() {
            this.vertices = [];
            this.normals = [];
            this.uvs = [];
            this.uvs2 = [];
            this.indices = [];
            this.current_index = 0;
            this.guid = utils_1.Utils.CreateUUID();
        }
        AddTri(v1, v2, v3, uv1, uv2, uv3) {
            this.vertices.push(v1);
            this.vertices.push(v2);
            this.vertices.push(v3);
            this.uvs.push(uv1);
            this.uvs.push(uv2);
            this.uvs.push(uv3);
            const normal = Vector3.cross(v1.sub(v2), v1.sub(v3)).normalized;
            this.normals.push(normal);
            this.normals.push(normal);
            this.normals.push(normal);
            this.indices = this.indices.concat([this.current_index, this.current_index + 1, this.current_index + 2]);
            this.current_index += 3;
        }
        AddFace(v1, v2, v3, v4, uv1, uv2, uv3, uv4) {
            const normal = Vector3.cross(v1.sub(v2), v1.sub(v3)).normalized;
            if (!normal.isFinite())
                return;
            this.vertices.push(v1);
            this.vertices.push(v2);
            this.vertices.push(v3);
            this.vertices.push(v4);
            this.uvs.push(uv1);
            this.uvs.push(uv2);
            this.uvs.push(uv3);
            this.uvs.push(uv4);
            this.normals.push(normal);
            this.normals.push(normal);
            this.normals.push(normal);
            this.normals.push(normal);
            for (let i of Mesh.local_indices)
                this.indices.push(i + this.current_index);
            this.current_index += 4;
        }
        ApplyAdditionalData(mesh_data) {
            if ((mesh_data === null || mesh_data === void 0 ? void 0 : mesh_data.uv2) != null) {
                if (mesh_data === null || mesh_data === void 0 ? void 0 : mesh_data.uv2.some(function (d) {
                    const v = new Vector2(d);
                    return v.isNaN() || !v.isFinite();
                })) {
                    return;
                }
                this.uvs2 = mesh_data.uv2.map(function (d) { return new Vector2(d); });
            }
        }
        RecalculateNormals() {
            this.normals = [];
            if (this.vertices.length % 3 != 0)
                return;
            for (let i = 0; i < this.vertices.length; i += 3) {
                const v1 = this.vertices[i];
                const v2 = this.vertices[i + 1];
                const v3 = this.vertices[i + 2];
                const n = Vector3.cross(v2.sub(v1), v3.sub(v1));
                this.normals.push(n);
                this.normals.push(n);
                this.normals.push(n);
            }
        }
    }
    Mesh.local_indices = [0, 1, 3, 3, 1, 2];
    Generator.Mesh = Mesh;
    class PrimitiveMesh extends Mesh {
        constructor(size, angle1, angle2, offset = new Vector3(), preserve_uv_aspect = false, texture_scale = 1) {
            super();
            this.size = size;
            this.offset = offset.invertX();
            this.angle1 = angle1 / 180.0 * Math.PI;
            this.angle2 = angle2 / 180.0 * Math.PI;
            this.texture_scale = texture_scale;
            this.preserve_uv_aspect = preserve_uv_aspect;
            if (size.isZero() || !size.isFinite())
                return;
            this.Generate();
        }
        Generate() {
            const hs = this.size.div(2);
            //const u_size_div: number = 2.4 / 7.0;
            //const v_size_div: number = 10.0 / 7.0;
            const u_size_div = 1;
            const v_size_div = 1;
            const angle_offset1 = this.angle1 == 0 ? 0 : this.size.x * Math.tan(Math.PI - this.angle1);
            const angle_offset2 = this.angle2 == 0 ? 0 : this.size.x * Math.tan(this.angle2);
            const raw_verts = [
                this.offset.add(new Vector3(-hs.x, -hs.y, -hs.z).invertX()),
                this.offset.add(new Vector3(-hs.x, -hs.y, hs.z).invertX()),
                this.offset.add(new Vector3(hs.x, -hs.y + angle_offset1, hs.z).invertX()),
                this.offset.add(new Vector3(hs.x, -hs.y + angle_offset1, -hs.z).invertX()),
                this.offset.add(new Vector3(-hs.x, hs.y, -hs.z).invertX()),
                this.offset.add(new Vector3(-hs.x, hs.y, hs.z)).invertX(),
                this.offset.add(new Vector3(hs.x, hs.y - angle_offset2, hs.z).invertX()),
                this.offset.add(new Vector3(hs.x, hs.y - angle_offset2, -hs.z).invertX())
            ];
            //FRONT
            {
                let uv1, uv2, uv3, uv4;
                if (!this.preserve_uv_aspect) {
                    uv1 = new Vector2(0, 0);
                    uv2 = new Vector2(0, this.size.y / v_size_div);
                    uv3 = new Vector2(this.size.x / u_size_div, this.size.y / v_size_div);
                    uv4 = new Vector2(this.size.x / u_size_div, 0);
                }
                else {
                    uv1 = new Vector2(0, 0);
                    uv2 = new Vector2(0, 1);
                    uv3 = new Vector2(1, 1);
                    uv4 = new Vector2(1, 0);
                }
                super.AddFace(raw_verts[0], raw_verts[4], raw_verts[7], raw_verts[3], uv1, uv2, uv3, uv4);
            }
            //TOP
            {
                let uv1, uv2, uv3, uv4;
                if (!this.preserve_uv_aspect) {
                    uv1 = new Vector2(0, this.size.x / v_size_div);
                    uv2 = new Vector2(this.size.z / u_size_div, this.size.x / v_size_div);
                    uv3 = new Vector2(this.size.z / u_size_div, 0);
                    uv4 = new Vector2();
                }
                else {
                    uv1 = new Vector2(0, 0);
                    uv2 = new Vector2(0, 1);
                    uv3 = new Vector2(1, 1);
                    uv4 = new Vector2(1, 0);
                }
                super.AddFace(raw_verts[4], raw_verts[5], raw_verts[6], raw_verts[7], uv1, uv2, uv3, uv4);
            }
            //BACK
            {
                let uv1, uv2, uv3, uv4;
                if (!this.preserve_uv_aspect) {
                    uv1 = new Vector2(this.size.x / u_size_div, 0);
                    uv2 = new Vector2(this.size.x / u_size_div, this.size.y / v_size_div);
                    uv3 = new Vector2(0, this.size.y / v_size_div);
                    uv4 = new Vector2();
                }
                else {
                    uv1 = new Vector2(1, 0);
                    uv2 = new Vector2(1, 1);
                    uv3 = new Vector2(0, 1);
                    uv4 = new Vector2(0, 0);
                }
                super.AddFace(raw_verts[2], raw_verts[6], raw_verts[5], raw_verts[1], uv1, uv2, uv3, uv4);
            }
            //BOTTOM
            {
                let uv1, uv2, uv3, uv4;
                if (!this.preserve_uv_aspect) {
                    uv1 = new Vector2(0, this.size.x / v_size_div);
                    uv2 = new Vector2(this.size.z / u_size_div, this.size.x / v_size_div);
                    uv3 = new Vector2(this.size.z / u_size_div, 0);
                    uv4 = new Vector2();
                }
                else {
                    uv1 = new Vector2(0, 0);
                    uv2 = new Vector2(0, 1);
                    uv3 = new Vector2(1, 1);
                    uv4 = new Vector2(1, 0);
                }
                super.AddFace(raw_verts[3], raw_verts[2], raw_verts[1], raw_verts[0], uv1, uv2, uv3, uv4);
            }
            //LEFT
            {
                let uv1, uv2, uv3, uv4;
                if (!this.preserve_uv_aspect) {
                    uv1 = new Vector2();
                    uv2 = new Vector2(0, this.size.y / v_size_div);
                    uv3 = new Vector2(this.size.z / u_size_div, this.size.y / v_size_div);
                    uv4 = new Vector2(this.size.z / u_size_div, 0);
                }
                else {
                    uv1 = new Vector2(0, 0);
                    uv2 = new Vector2(0, 1);
                    uv3 = new Vector2(1, 1);
                    uv4 = new Vector2(1, 0);
                }
                super.AddFace(raw_verts[1], raw_verts[5], raw_verts[4], raw_verts[0], uv1, uv2, uv3, uv4);
            }
            //RIGHT
            {
                let uv1, uv2, uv3, uv4;
                if (!this.preserve_uv_aspect) {
                    uv1 = new Vector2();
                    uv2 = new Vector2(0, this.size.y / v_size_div);
                    uv3 = new Vector2(this.size.z / u_size_div, this.size.y / v_size_div);
                    uv4 = new Vector2(this.size.z / u_size_div, 0);
                }
                else {
                    uv1 = new Vector2(0, 0);
                    uv2 = new Vector2(0, 1);
                    uv3 = new Vector2(1, 1);
                    uv4 = new Vector2(1, 0);
                }
                super.AddFace(raw_verts[3], raw_verts[7], raw_verts[6], raw_verts[2], uv1, uv2, uv3, uv4);
            }
        }
    }
    Generator.PrimitiveMesh = PrimitiveMesh;
    class CurvedMesh extends Mesh {
        constructor(shape_points, radius, start_angle, end_angle, precision, centered) {
            super();
            const segment_size = 3 / (radius * precision);
            const count = Math.round((end_angle - start_angle) / segment_size);
            const curve_points = [];
            for (let i = 0; i < count; i++) {
                const alpha = start_angle + (end_angle - start_angle) * (i / count);
                curve_points.push(new Vector2(Math.cos(alpha / 180.0 * Math.PI), Math.sin(alpha / 180.0 * Math.PI)).mult(radius));
                if (i == count - 1)
                    curve_points.push(new Vector2(Math.cos(end_angle / 180.0 * Math.PI), Math.sin(end_angle / 180.0 * Math.PI)).mult(radius));
            }
            if (centered) {
                const min = new Vector2(Math.min(...Array.from(curve_points, function (cp) { return cp.x; })), Math.min(...Array.from(curve_points, function (cp) { return cp.y; })));
                const max = new Vector2(Math.max(...Array.from(curve_points, function (cp) { return cp.x; })), Math.max(...Array.from(curve_points, function (cp) { return cp.y; })));
                const offset = min.add(max.sub(min).div(2.0));
                for (let i = 0; i < curve_points.length; i++)
                    curve_points[i] = curve_points[i].sub(offset);
            }
            if (centered) {
                const min = new Vector2(Math.min(...Array.from(shape_points, function (cp) { return cp.x; })), Math.min(...Array.from(shape_points, function (cp) { return cp.y; })));
                const max = new Vector2(Math.max(...Array.from(shape_points, function (cp) { return cp.x; })), Math.max(...Array.from(shape_points, function (cp) { return cp.y; })));
                const offset = min.add(max.sub(min).div(2.0));
                for (let i = 0; i < shape_points.length; i++) {
                    shape_points[i] = shape_points[i].sub(offset);
                    shape_points[i].x = -shape_points[i].x;
                    shape_points[i].y = -shape_points[i].y;
                }
            }
            this.Setup(shape_points, curve_points, false);
            const unwrapper = new TriplanarUnwrapper(this, { group: "" }, null, new Vector3(), new Quaternion(), new Vector3(1, 1, 1));
            this.uvs = unwrapper.Process();
        }
        static Construct(shape_points, radius, start_angle, end_angle, precision, centered) {
            const temp = {
                "shape_points": shape_points,
                "radius": radius,
                "start_angle": start_angle,
                "end_angle": end_angle,
                "precision": precision,
                "centered": centered
            };
            const temp_data = JSON.stringify(temp);
            if (temp_data in CurvedMesh.cache)
                return CurvedMesh.cache[temp_data];
            const result = new CurvedMesh(shape_points, radius, start_angle, end_angle, precision, centered);
            CurvedMesh.cache[temp_data] = result;
            return result;
        }
        Setup(shape_points, curve_points, loop) {
            const temp_vertices = [];
            function GetForwardVector(i) {
                let v1;
                let v2;
                if (i == curve_points.length - 1) {
                    if (loop) {
                        v1 = new Vector3(curve_points[i]);
                        v2 = new Vector3(curve_points[0]);
                    }
                    else {
                        v1 = new Vector3(curve_points[i - 1]);
                        v2 = new Vector3(curve_points[i]);
                    }
                }
                else if (i == 0) {
                    if (loop) {
                        v1 = new Vector3(curve_points[curve_points.length - 1]);
                        v2 = new Vector3(curve_points[1]);
                    }
                    else {
                        v1 = new Vector3(curve_points[0]);
                        v2 = new Vector3(curve_points[1]);
                    }
                }
                else {
                    v1 = new Vector3(curve_points[i - 1]);
                    v2 = new Vector3(curve_points[i + 1]);
                }
                return v2.sub(v1).normalized;
            }
            for (let i = 0; i < curve_points.length; i++) {
                const pos = new Vector3(curve_points[i]);
                const forward = GetForwardVector(i);
                const rotation = Quaternion.lookRotation(forward, new Vector3(0, 0, 1));
                for (let j = 0; j < shape_points.length; j++)
                    temp_vertices.push(pos.add(rotation.rotate(new Vector3(shape_points[j]))).invertX());
            }
            //let ind = 0;
            for (let i = 0; i < curve_points.length - (loop ? 0 : 1); i++) {
                for (let j = 0; j < shape_points.length; j++) {
                    let rj = j + 1;
                    if (j == shape_points.length - 1)
                        rj = 0;
                    let i1 = i * shape_points.length + j;
                    let i2 = i * shape_points.length + rj;
                    let ri = i + 1;
                    if (i == curve_points.length - 1)
                        ri = 0;
                    let i3 = ri * shape_points.length + rj;
                    let i4 = ri * shape_points.length + j;
                    this.AddTri(temp_vertices[i1], temp_vertices[i2], temp_vertices[i3], new Vector2(), new Vector2(), new Vector2());
                    this.AddTri(temp_vertices[i1], temp_vertices[i3], temp_vertices[i4], new Vector2(), new Vector2(), new Vector2());
                }
            }
        }
    }
    CurvedMesh.cache = {};
    Generator.CurvedMesh = CurvedMesh;
    class ShapedMesh extends Mesh {
        constructor(size, shape, angle1, angle2, offset = new Vector3(), texture_scale = 1) {
            super();
            this.size = size;
            this.shape = shape;
            this.offset = offset.invertX();
            this.angle1 = angle1 / 180.0 * Math.PI;
            this.angle2 = angle2 / 180.0 * Math.PI;
            this.texture_scale = texture_scale;
            this.Generate();
        }
        static Construct(size, shape, angle1, angle2, offset = new Vector3(), texture_scale = 1) {
            const temp = {
                "size": size,
                "shape": shape,
                "angle1": angle1,
                "angle2": angle2,
                "offset": offset,
                "texture_scale": texture_scale
            };
            const temp_data = JSON.stringify(temp);
            if (temp_data in CurvedMesh.cache)
                return CurvedMesh.cache[temp_data];
            const result = new ShapedMesh(size, shape, angle1, angle2, offset, texture_scale);
            CurvedMesh.cache[temp_data] = result;
            return result;
        }
        static NormalizeShape(shape) {
            const result = [];
            let min = new Vector2(Number.MAX_VALUE, Number.MAX_VALUE);
            for (const s of shape) {
                if (s.x < min.x)
                    min.x = s.x;
                if (s.y < min.y)
                    min.y = s.y;
            }
            for (const s of shape)
                result.push(s.sub(min));
            return result;
        }
        GetWavePoints(size, s1, s2) {
            const half_size = size.div(2.0);
            let angle_offset1_s1 = 0;
            let angle_offset2_s1 = 0;
            let angle_offset1_s2 = 0;
            let angle_offset2_s2 = 0;
            if (this.angle1 != 0) {
                angle_offset1_s1 = s1.x * Math.tan(Math.PI - this.angle1);
                angle_offset1_s2 = s2.x * Math.tan(Math.PI - this.angle1);
            }
            if (this.angle2 != 0) {
                angle_offset2_s1 = s1.x * Math.tan(this.angle2);
                angle_offset2_s2 = s2.x * Math.tan(this.angle2);
            }
            return [
                new Vector3(s2.x - half_size.x, -half_size.y + angle_offset1_s2, -s2.y + half_size.z).invertX(),
                new Vector3(s2.x - half_size.x, half_size.y - angle_offset2_s2, -s2.y + half_size.z).invertX(),
                new Vector3(s1.x - half_size.x, half_size.y - angle_offset2_s1, -s1.y + half_size.z).invertX(),
                new Vector3(s1.x - half_size.x, -half_size.y + angle_offset1_s1, -s1.y + half_size.z).invertX()
            ];
        }
        GetWavePoint(size, s1) {
            const half_size = size.div(2.0);
            let angle_offset1_s1 = 0;
            let angle_offset2_s1 = 0;
            if (this.angle1 != 0)
                angle_offset1_s1 = s1.x * Math.tan(Math.PI - this.angle1);
            if (this.angle2 != 0)
                angle_offset2_s1 = s1.x * Math.tan(this.angle2);
            return [
                new Vector3(s1.x - half_size.x, -half_size.y + angle_offset1_s1, -s1.y + half_size.z).invertX().add(this.offset),
                new Vector3(s1.x - half_size.x, half_size.y - angle_offset2_s1, -s1.y + half_size.z).invertX().add(this.offset)
            ];
        }
        Generate() {
            let nshape = ShapedMesh.NormalizeShape(this.shape);
            let orient = 0;
            for (let i = 0; i < nshape.length; i++) {
                let j = i == nshape.length - 1 ? 0 : i + 1;
                if (Vector2.distance(nshape[i], nshape[j]) < 0.0001) {
                    nshape.splice(j, 1);
                    i--;
                }
                else {
                    orient += (nshape[j].x - nshape[i].x) * (nshape[j].y + nshape[i].y);
                }
            }
            if (orient > 0)
                nshape = nshape.reverse();
            let uv_offset = 0;
            const upper_points = [];
            const lower_points = [];
            let s1, s2;
            const u_size_div = 2.4 / 7.0 / this.texture_scale;
            const v_size_div = 10.0 / 7.0 / this.texture_scale;
            for (let i = 0; i < nshape.length; i++) {
                s1 = nshape[i];
                if (i + 1 != nshape.length)
                    s2 = nshape[i + 1];
                else
                    s2 = nshape[0];
                let uv1, uv2, uv3, uv4;
                const vs = this.GetWavePoints(this.size, s1, s2);
                upper_points.push(vs[1]);
                upper_points.push(vs[2]);
                lower_points.push(vs[0]);
                lower_points.push(vs[3]);
                const dvs = vs[1].sub(vs[3]);
                const duv_x = Math.abs(new Vector2(dvs.x, dvs.z).length);
                uv1 = new Vector2(uv_offset / u_size_div, 0);
                uv2 = new Vector2(uv_offset / u_size_div, this.size.y / v_size_div);
                uv3 = new Vector2((uv_offset + duv_x) / u_size_div, this.size.y / v_size_div);
                uv4 = new Vector2((uv_offset + duv_x) / u_size_div, 0);
                this.AddFace(vs[0], vs[1], vs[2], vs[3], uv1, uv2, uv3, uv4);
                uv_offset += duv_x;
            }
            const triang = new Triangulator(nshape);
            const tr = triang.Triangulate();
            for (let j = 0; j < tr.length; j += 3) {
                const vs0 = this.GetWavePoint(this.size, nshape[tr[j + 0]]);
                const vs1 = this.GetWavePoint(this.size, nshape[tr[j + 1]]);
                const vs2 = this.GetWavePoint(this.size, nshape[tr[j + 2]]);
                this.AddTri(vs2[0], vs1[0], vs0[0], new Vector2(), new Vector2(), new Vector2());
                this.AddTri(vs0[1], vs1[1], vs2[1], new Vector2(), new Vector2(), new Vector2());
            }
        }
    }
    ShapedMesh.cache = {};
    Generator.ShapedMesh = ShapedMesh;
})(Generator = exports.Generator || (exports.Generator = {}));


/***/ }),

/***/ "./src/MeshUtils/unwrapper.ts":
/*!************************************!*\
  !*** ./src/MeshUtils/unwrapper.ts ***!
  \************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.Unwrapper = void 0;
const math_1 = __webpack_require__(/*! ../math */ "./src/math.ts");
var Vector2 = math_1.VMath.Vector2;
var Vector3 = math_1.VMath.Vector3;
var Matrix4x4 = math_1.VMath.Matrix4x4;
const utils_1 = __webpack_require__(/*! ../utils */ "./src/utils.ts");
var Unwrapper;
(function (Unwrapper) {
    var Random = math_1.VMath.Random;
    var GetHash = utils_1.Utils.GetHash;
    class Polygon {
        constructor(...args) {
            if (args.length == 1) {
                this.mesh = args[0].mesh;
                this.indices = Object.assign([], args[0].indices);
                this.Areas = Object.assign({}, args[0].Areas);
            }
            else {
                this.mesh = args[0];
                this.indices = Object.assign([], args[1]);
                this.Areas = {};
            }
            this.vertices = this.mesh.vertices;
            this.normals = this.mesh.normals;
        }
        IsNeighbour(p, threshold) {
            for (const i of p.indices) {
                for (const k of this.indices) {
                    const v1 = this.vertices[i];
                    const v2 = this.vertices[k];
                    const n1 = this.normals[i];
                    const n2 = this.normals[k];
                    if (v1.equals(v2) && Vector3.angle(n1, n2) <= threshold)
                        return true;
                }
            }
            return false;
        }
        Combine(p) {
            this.indices = this.indices.concat(p.indices);
        }
        GenerateAreas() {
            this.Areas = {};
            const o = this.vertices[this.indices[0]];
            let normal = new Vector3();
            for (const i of this.indices)
                normal = normal.add(this.normals[i]);
            normal = normal.div(this.normals.length);
            const oz = normal.normalized;
            const ox = this.vertices[this.indices[1]].sub(o).normalized;
            const oy = Vector3.cross(ox, oz).normalized;
            const mat = new Matrix4x4();
            mat[0][0] = ox.x;
            mat[1][0] = ox.y;
            mat[2][0] = ox.z;
            mat[0][1] = oy.x;
            mat[1][1] = oy.y;
            mat[2][1] = oy.z;
            mat[0][2] = oz.x;
            mat[1][2] = oz.y;
            mat[2][2] = oz.z;
            for (const i of this.indices) {
                const tr = mat.mult(this.vertices[i].sub(o));
                this.Areas[i] = new Vector2(tr.x, tr.y);
            }
            const min = new Vector2(Number.MAX_VALUE, Number.MAX_VALUE);
            for (const i of Object.keys(this.Areas).map(k => Number.parseInt(k))) {
                if (this.Areas[i].x < min.x)
                    min.x = this.Areas[i].x;
                if (this.Areas[i].y < min.y)
                    min.y = this.Areas[i].y;
            }
            for (const i of Object.keys(this.Areas).map(k => Number.parseInt(k)))
                this.Areas[i] = this.Areas[i].sub(min);
        }
        Resize(size) {
            for (const i of Object.keys(this.Areas).map(k => Number.parseInt(k)))
                this.Areas[i] = this.Areas[i].div(size);
        }
        Scale(m) {
            for (const i of Object.keys(this.Areas).map(k => Number.parseInt(k)))
                this.Areas[i] = this.Areas[i].mult(m);
        }
        ApplyOffset(offset) {
            for (const i of Object.keys(this.Areas).map(k => Number.parseInt(k)))
                this.Areas[i] = this.Areas[i].add(offset);
        }
        IsOutOfBounds() {
            for (const i of Object.keys(this.Areas).map(k => Number.parseInt(k))) {
                if (this.Areas[i].x > 1 || this.Areas[i].y > 1)
                    return true;
            }
            return false;
        }
    }
    class LightmapUnwrapper {
        constructor(mesh, threshold = 0, margin = 0.05) {
            this.threshold = threshold;
            this.mesh = mesh;
            this.margin = margin;
            this.indices = this.mesh.indices;
        }
        GeneratePolygons() {
            const result = [];
            for (let i = 0; i < this.indices.length; i += 6) {
                const poly = new Polygon(this.mesh, this.indices[i], this.indices[i + 1], this.indices[i + 2], this.indices[i + 3], this.indices[i + 4], this.indices[i + 5]);
                const local_threshold = this.threshold;
                const found = result.find(function (r) {
                    return r.IsNeighbour(poly, local_threshold);
                });
                if (found == null)
                    result.push(poly);
                else
                    found.Combine(poly);
            }
            return result;
        }
        GetPolygonSize(...polys) {
            const min = new Vector2(Number.MAX_VALUE, Number.MAX_VALUE);
            const max = new Vector2(Number.MIN_VALUE, Number.MIN_VALUE);
            for (const p of polys) {
                for (const a of Object.keys(p.Areas).map(key => p.Areas[parseInt(key)])) {
                    if (a.x < min.x)
                        min.x = a.x;
                    if (a.x > max.x)
                        max.x = a.x;
                    if (a.y < min.y)
                        min.y = a.y;
                    if (a.y > max.y)
                        max.y = a.y;
                }
            }
            return max.sub(min);
        }
        GetPolygonAspect(...polys) {
            const size = this.GetPolygonSize(...polys);
            if (size.y > size.x)
                return 1 / size.y;
            return 1 / size.x;
        }
        Pack(polys, mult = 1) {
            const clone = polys.map(p => new Polygon(p));
            const offset = new Vector2(this.margin, this.margin);
            let next_offset_y = null;
            for (let i = 0; i < clone.length; i++) {
                const p = clone[i];
                let temp = new Polygon(p);
                temp.Scale(mult);
                temp.ApplyOffset(offset);
                if (temp.IsOutOfBounds()) {
                    if (next_offset_y == null) {
                        this.Pack(polys, mult * 0.9);
                        return;
                    }
                    offset.y += next_offset_y + this.margin;
                    next_offset_y = 0;
                    offset.x = this.margin;
                    temp = new Polygon(p);
                    temp.Scale(mult);
                    temp.ApplyOffset(offset);
                    if (temp.IsOutOfBounds()) {
                        this.Pack(polys, mult * 0.9);
                    }
                }
                const next_size_y = this.GetPolygonSize(temp).y;
                if (next_offset_y == null || next_size_y > next_offset_y)
                    next_offset_y = next_size_y;
                offset.x += this.GetPolygonSize(temp).x + this.margin;
                clone[i] = temp;
            }
            for (let i = 0; i < polys.length; i++)
                polys[i].Areas = clone[i].Areas;
        }
        Process() {
            const result = new Array(this.mesh.vertices.length);
            const polys = this.GeneratePolygons();
            for (const p of polys)
                p.GenerateAreas();
            const aspect = this.GetPolygonAspect(...polys);
            for (const p of polys)
                p.Scale(aspect);
            this.Pack(polys);
            for (const p of polys) {
                for (let k in p.Areas) {
                    result[k] = p.Areas[k];
                }
            }
            return result;
        }
    }
    Unwrapper.LightmapUnwrapper = LightmapUnwrapper;
    class TriplanarUnwrapper {
        constructor(mesh, fitting, proc, position, rotation, scale) {
            this.mesh = mesh;
            this.fitting = fitting;
            this.proc = proc;
            this.position = position;
            this.rotation = rotation;
            this.scale = scale;
        }
        GetFittingOffset() {
            let offset = new Vector2(0.25, 0.25);
            if (this.fitting.group != null && this.fitting.group != "") {
                const r = new Random(GetHash(this.fitting.group));
                offset = offset.add(new Vector2(r.next(), r.next()));
            }
            return offset;
        }
        Process() {
            const result = [];
            const scale = this.proc == null ? new Vector2(1, 1) : new Vector2(this.proc.scale);
            const uv_rotation = this.proc == null ? 0 : this.proc.rotation;
            for (let i = 0; i < this.mesh.vertices.length; i++) {
                let offset = this.proc == null ? new Vector2 : new Vector2(this.proc.offset);
                let uv = this.mesh.uvs[i];
                if (this.fitting != null) {
                    const vertex = Vector3.scale(this.mesh.vertices[i], this.scale);
                    const normal = this.mesh.normals[i];
                    let bf = (this.rotation.rotate(normal)).abs().normalized;
                    bf = bf.div(Vector3.dot(bf, Vector3.one));
                    offset = offset.add(this.GetFittingOffset());
                    const wpos = this.position.add(this.rotation.rotate(vertex));
                    let tx = wpos.zy();
                    tx.x += wpos.x;
                    let ty = wpos.xz();
                    ty.y += wpos.y;
                    let tz = wpos.xy();
                    if (uv_rotation != 0) {
                        tx = tx.rotate(uv_rotation);
                        ty = ty.rotate(uv_rotation);
                        tz = tz.rotate(uv_rotation);
                    }
                    tx = Vector2.scale(tx, scale).add(offset).mult(bf.x);
                    ty = Vector2.scale(ty, scale).add(offset).mult(bf.y);
                    tz = Vector2.scale(tz, scale).add(offset).mult(bf.z);
                    uv = tx.add(ty).add(tz);
                }
                else if (this.proc != null) {
                    uv = uv.rotate(uv_rotation);
                    uv = Vector2.scale(uv, scale).add(offset);
                }
                result.push(uv);
            }
            return result;
        }
    }
    Unwrapper.TriplanarUnwrapper = TriplanarUnwrapper;
})(Unwrapper = exports.Unwrapper || (exports.Unwrapper = {}));


/***/ }),

/***/ "./src/filesystem.ts":
/*!***************************!*\
  !*** ./src/filesystem.ts ***!
  \***************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.Filesystem = void 0;
const logger_1 = __webpack_require__(/*! ./logger */ "./src/logger.ts");
var DebugLevel = logger_1.Logger.DebugLevel;
var Filesystem;
(function (Filesystem) {
    class Cache {
        static Add(key, value) {
            this.downloaded_files[key] = value;
        }
        static Has(key) {
            return key in this.downloaded_files && this.downloaded_files[key] != undefined;
        }
        static Get(key) {
            if (!this.Has(key))
                return "";
            return this.downloaded_files[key];
        }
        static Clear() {
            for (let k in this.downloaded_files)
                delete this.downloaded_files[k];
            this.downloaded_files = {};
        }
    }
    Cache.downloaded_files = {};
    Filesystem.Cache = Cache;
    Filesystem.GetRelativePath = function (p) {
        if (p == null)
            return "";
        if (typeof p != "string")
            return "";
        return p.replace("file://", "");
    };
    Filesystem.GetFileContents = null;
    Filesystem.GetFileContentsAsync = null;
    Filesystem.ApiGet = null;
    Filesystem.ApiPost = null;
    const loading_paths = [];
    function Get(url) {
        return __awaiter(this, void 0, void 0, function* () {
            if (Cache.Has(url)) {
                logger_1.Logger.Log(url + " is in cache", logger_1.Logger.DebugLevel.VERBOSE);
                return Cache.Get(url);
            }
            if (Filesystem.ApiGet == null) {
                logger_1.Logger.Log("ApiGet is not set", logger_1.Logger.DebugLevel.ERROR);
                return "";
            }
            let result = "";
            if (Filesystem.ApiGet.ToPromise != undefined)
                result = yield Filesystem.ApiGet(url).ToPromise();
            else
                result = yield Filesystem.ApiGet(url);
            if (result == "")
                result = null;
            else
                result = JSON.parse(result);
            Cache.Add(url, result);
            return result;
        });
    }
    Filesystem.Get = Get;
    function Post(url, data) {
        return __awaiter(this, void 0, void 0, function* () {
            if (Cache.Has(url + "?" + data)) {
                logger_1.Logger.Log(url + "?" + data + " is in cache", logger_1.Logger.DebugLevel.VERBOSE);
                return Cache.Get(url);
            }
            if (Filesystem.ApiPost == null) {
                logger_1.Logger.Log("ApiPost is not set", logger_1.Logger.DebugLevel.ERROR);
                return "";
            }
            let result = "";
            if (Filesystem.ApiPost.ToPromise != undefined)
                result = yield Filesystem.ApiPost(url, data).ToPromise();
            else
                result = yield Filesystem.ApiPost(url, data);
            if (result == "")
                result = null;
            else
                result = JSON.parse(result);
            Cache.Add(url + "?" + data, result);
            return result;
        });
    }
    Filesystem.Post = Post;
    function GetFile(path) {
        return __awaiter(this, void 0, void 0, function* () {
            if (Cache.Has(path)) {
                logger_1.Logger.Log(path + " is in cache", logger_1.Logger.DebugLevel.VERBOSE);
                return Cache.Get(path);
            }
            const gfc = Filesystem.GetFileContentsAsync != null ? Filesystem.GetFileContentsAsync : Filesystem.GetFileContents;
            if (gfc == null) {
                logger_1.Logger.Log("GetFileContents is not set", logger_1.Logger.DebugLevel.ERROR);
                return "";
            }
            logger_1.Logger.Log("Calling GetFileContents with argument \"" + path + "\"", logger_1.Logger.DebugLevel.VERBOSE);
            let result = "";
            try {
                if (gfc.ToPromise != undefined)
                    result = yield gfc(path).ToPromise();
                else
                    result = yield gfc(path);
            }
            catch (_a) {
                //pass
            }
            if (result == "") {
                logger_1.Logger.Log("No data received", DebugLevel.VERBOSE);
                result = null;
            }
            else {
                logger_1.Logger.Log("Received data length: " + result.length, DebugLevel.VERBOSE);
                result = JSON.parse(result);
            }
            Cache.Add(path, result);
            return result;
        });
    }
    Filesystem.GetFile = GetFile;
})(Filesystem = exports.Filesystem || (exports.Filesystem = {}));


/***/ }),

/***/ "./src/graph.ts":
/*!**********************!*\
  !*** ./src/graph.ts ***!
  \**********************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.Graph = void 0;
const math_1 = __webpack_require__(/*! ./math */ "./src/math.ts");
const utils_1 = __webpack_require__(/*! ./utils */ "./src/utils.ts");
var Graph;
(function (Graph_1) {
    var Vector2 = math_1.VMath.Vector2;
    let ArgumentType;
    (function (ArgumentType) {
        ArgumentType[ArgumentType["NONE"] = 0] = "NONE";
        ArgumentType[ArgumentType["ANY"] = 1] = "ANY";
        ArgumentType[ArgumentType["NODE"] = 2] = "NODE";
        ArgumentType[ArgumentType["STRING"] = 3] = "STRING";
        ArgumentType[ArgumentType["INT"] = 4] = "INT";
        ArgumentType[ArgumentType["FLOAT"] = 5] = "FLOAT";
        ArgumentType[ArgumentType["COMPONENT"] = 6] = "COMPONENT";
        ArgumentType[ArgumentType["POSITIONING_POINT"] = 7] = "POSITIONING_POINT";
        ArgumentType[ArgumentType["CONNECTION_POINT"] = 8] = "CONNECTION_POINT";
        ArgumentType[ArgumentType["PROCESSING"] = 10] = "PROCESSING";
        ArgumentType[ArgumentType["TEXTURE_FITTING"] = 11] = "TEXTURE_FITTING";
        ArgumentType[ArgumentType["COMPONENT_FIELD"] = 12] = "COMPONENT_FIELD";
        ArgumentType[ArgumentType["POSITIONING_POINT_FIELD"] = 13] = "POSITIONING_POINT_FIELD";
        ArgumentType[ArgumentType["CONNECTION_POINT_FIELD"] = 14] = "CONNECTION_POINT_FIELD";
        ArgumentType[ArgumentType["PROCESSING_FIELD"] = 16] = "PROCESSING_FIELD";
        ArgumentType[ArgumentType["TEXTURE_FITTING_FIELD"] = 17] = "TEXTURE_FITTING_FIELD";
        ArgumentType[ArgumentType["DICTIONARY"] = 18] = "DICTIONARY";
        ArgumentType[ArgumentType["SCRIPT"] = 19] = "SCRIPT";
        ArgumentType[ArgumentType["INPUT"] = 20] = "INPUT";
        ArgumentType[ArgumentType["OUTPUT"] = 21] = "OUTPUT";
    })(ArgumentType = Graph_1.ArgumentType || (Graph_1.ArgumentType = {}));
    class GraphNodeMethodArgumentNodeValue {
        constructor(node_guid, pair_key) {
            this.node_guid = node_guid;
            this.pair_key = pair_key;
        }
    }
    class GraphNodeMethodArgumentKeyValuePair {
        constructor(type, key, value) {
            this.type = type;
            this.key = key;
            this.value = value;
        }
    }
    class GraphNodeMethodArgument {
        constructor(name, value, type) {
            this.name = name;
            this.value = value;
            this.type = type;
        }
    }
    class GraphNodeMethod {
        constructor(...args) {
            if (args.length == 1) {
                this.name = args[0].name;
                this.arguments = args[0].arguments;
                this.result = args[0].result;
                return;
            }
            this.name = args[0];
            this.arguments = args[1];
            this.result = args[2];
        }
    }
    class GraphNode {
        constructor(...args) {
            if (args.length == 1) {
                this.guid = args[0].guid;
                this.name = args[0].name;
                this.position = args[0].position;
                this.method = new GraphNodeMethod(args[0].method);
                return;
            }
            this.guid = args[0];
            this.name = args[1];
            this.position = args[2];
            this.method = new GraphNodeMethod(args[3]);
        }
    }
    let GraphInputType;
    (function (GraphInputType) {
        GraphInputType[GraphInputType["FLOAT"] = 1] = "FLOAT";
        GraphInputType[GraphInputType["STRING"] = 2] = "STRING";
        GraphInputType[GraphInputType["INT"] = 3] = "INT";
        GraphInputType[GraphInputType["FILES"] = 4] = "FILES";
        GraphInputType[GraphInputType["ENUM"] = 5] = "ENUM";
        GraphInputType[GraphInputType["FILES_TAG"] = 6] = "FILES_TAG";
    })(GraphInputType || (GraphInputType = {}));
    class GraphInputSettings {
        constructor(...args) {
            if (args.length == 1) {
                this.tag = args[0].tag;
                this.is_interactive = args[0].is_interactive;
                this.event = args[0].event;
                this.show_in_preview = args[0].show_in_preview;
                this.show_in_consult = args[0].show_in_consult;
                return;
            }
            this.tag = args[0];
            this.is_interactive = args[1];
            this.event = args[2];
            this.show_in_preview = false;
            this.show_in_consult = true;
        }
    }
    class GraphInputStringSettings extends GraphInputSettings {
    }
    class GraphInputFloatSettings extends GraphInputSettings {
        constructor(...args) {
            if (args.length == 1) {
                super(args[0]);
                this.min = args[0].min;
                this.max = args[0].max;
                return;
            }
            super(args[0], args[1], args[2]);
            this.min = args[3];
            this.max = args[4];
        }
    }
    class GraphInputStringValue {
        constructor(value) {
            this.value = value;
        }
    }
    class GraphInputIntSettings extends GraphInputSettings {
        constructor(...args) {
            if (args.length == 1) {
                super(args[0]);
                this.min = args[0].min;
                this.max = args[0].max;
                return;
            }
            super(args[0], args[1], args[2]);
            this.min = args[3];
            this.max = args[4];
        }
    }
    class GraphInputFilesTagSettings extends GraphInputSettings {
        constructor(...args) {
            if (args.length == 1) {
                super(args[0]);
                this.search_tags = args[0].search_tags;
                return;
            }
            super(args[0], args[1], args[2]);
            this.search_tags = args[3];
        }
    }
    class GraphInputFileValue {
        constructor(name, icon, value) {
            this.name = name;
            this.icon = icon;
            this.value = value;
        }
    }
    class GraphInputFilesSettings extends GraphInputSettings {
        constructor(...args) {
            if (args.length == 1) {
                super(args[0]);
                this.values = args[0].values;
                return;
            }
            super(args[0], args[1], args[2]);
            this.values = args[3];
        }
    }
    class GraphInputEnumSettings extends GraphInputSettings {
        constructor(...args) {
            if (args.length == 1) {
                super(args[0]);
                this.values = args[0].values;
                return;
            }
            super(args[0], args[1], args[2]);
            this.values = args[3];
        }
    }
    class GraphInput {
        constructor(...args) {
            this.is_active = true;
            this.is_hidden = false;
            this.order = 0;
            if (args.length == 1) {
                const input_obj = args[0];
                this.guid = input_obj.guid;
                this.name = input_obj.name;
                this.type = input_obj.type;
                this.hint = input_obj.hint;
                this.value = input_obj.value;
                this.is_active = input_obj.is_active;
                this.is_hidden = input_obj.is_hidden;
                this.order = input_obj.order;
                switch (this.type) {
                    case GraphInputType.FLOAT:
                        this.settings = new GraphInputFloatSettings(input_obj.settings);
                        break;
                    case GraphInputType.INT:
                        this.settings = new GraphInputIntSettings(input_obj.settings);
                        break;
                    case GraphInputType.STRING:
                        this.settings = new GraphInputStringSettings(input_obj.settings);
                        break;
                    case GraphInputType.FILES_TAG:
                        this.settings = new GraphInputFilesTagSettings(input_obj.settings);
                        break;
                    case GraphInputType.FILES:
                        this.settings = new GraphInputFilesSettings(input_obj.settings);
                        break;
                    case GraphInputType.ENUM:
                        this.settings = new GraphInputEnumSettings(input_obj.settings);
                        break;
                }
                return;
            }
            this.guid = args[0];
            this.name = args[1];
            this.type = args[2];
            this.value = args[3];
            this.settings = args[4];
            this.is_active = args[5];
            this.hint = "";
        }
    }
    Graph_1.GraphInput = GraphInput;
    class GraphOutput {
        constructor(output_obj) {
            this.name = output_obj.name;
            this.hint = output_obj.hint;
            this.value = output_obj.value;
            this.show_in_preview = output_obj.show_in_preview;
            this.show_in_consult = output_obj.show_in_consult;
            this.is_hidden = output_obj.is_hidden;
            this.order = output_obj.order;
            this.tag = output_obj.tag;
        }
    }
    class Graph {
        constructor(...args) {
            this.related_inputs = null;
            this.is_active = true;
            this.inputs = [];
            this.outputs = [];
            this.nodes = [];
            if (args.length == 1) {
                this.related_inputs = {};
                const graph_obj = args[0];
                this.is_active = graph_obj.is_active;
                for (const i of graph_obj.inputs)
                    this.inputs.push(new GraphInput(i));
                for (const o of graph_obj.outputs)
                    this.outputs.push(new GraphOutput(o));
                for (const n of graph_obj.nodes)
                    this.nodes.push(new GraphNode(n));
                for (let i in graph_obj.related_inputs)
                    this.related_inputs[i] = graph_obj.related_inputs[i];
            }
        }
        AddMaterialInput(name, comp_full_names, materials) {
            //INPUT
            let input_settings = new GraphInputFilesSettings("", true, "", materials);
            let input = new GraphInput(utils_1.Utils.CreateUUID(), name, GraphInputType.FILES, materials[0].value, input_settings, true);
            //INPUT NODE
            let input_node_argument = new GraphNodeMethodArgument("input", name, ArgumentType.INPUT);
            let input_node_result = new GraphNodeMethodArgument("", "", ArgumentType.ANY);
            let input_node_method = new GraphNodeMethod("GetInputValue", [input_node_argument], input_node_result);
            let input_node = new GraphNode(utils_1.Utils.CreateUUID(), "РџРѕР»СѓС‡РёС‚СЊ РІС…РѕРґ", new Vector2(), input_node_method);
            //SET MATERIAL NODE
            let set_material_node_component_argument_values = [];
            for (let i in comp_full_names) {
                const pair = new GraphNodeMethodArgumentKeyValuePair(ArgumentType.COMPONENT, (i + 1).toString(), comp_full_names[i]);
                set_material_node_component_argument_values.push(pair);
            }
            let set_material_node_component_argument = new GraphNodeMethodArgument("components", set_material_node_component_argument_values, ArgumentType.DICTIONARY);
            let set_field_node_component_argument_value = new GraphNodeMethodArgumentKeyValuePair(ArgumentType.NODE, "material", new GraphNodeMethodArgumentNodeValue(input_node.guid, null));
            let set_field_node_component_argument = new GraphNodeMethodArgument("fields", [set_field_node_component_argument_value], ArgumentType.DICTIONARY);
            let set_material_node_result = new GraphNodeMethodArgument("", "", ArgumentType.NONE);
            let set_material_node_method = new GraphNodeMethod("SetComponentsFields", [set_material_node_component_argument, set_field_node_component_argument], set_material_node_result);
            let set_material_node = new GraphNode(utils_1.Utils.CreateUUID(), "Р—Р°РґР°С‚СЊ РїР°СЂР°РјРµС‚СЂ РґРµС‚Р°Р»Рё", new Vector2(), set_material_node_method);
            this.inputs.push(input);
            this.nodes.push(input_node);
            this.nodes.push(set_material_node);
        }
    }
    Graph_1.Graph = Graph;
})(Graph = exports.Graph || (exports.Graph = {}));


/***/ }),

/***/ "./src/iik.ts":
/*!********************!*\
  !*** ./src/iik.ts ***!
  \********************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.IIK = void 0;
const logger_1 = __webpack_require__(/*! ./logger */ "./src/logger.ts");
const math_1 = __webpack_require__(/*! ./math */ "./src/math.ts");
const project_1 = __webpack_require__(/*! ./project */ "./src/project.ts");
const graph_1 = __webpack_require__(/*! ./graph */ "./src/graph.ts");
const filesystem_1 = __webpack_require__(/*! ./filesystem */ "./src/filesystem.ts");
const generator_1 = __webpack_require__(/*! ./MeshUtils/generator */ "./src/MeshUtils/generator.ts");
const material_1 = __webpack_require__(/*! ./material */ "./src/material.ts");
const performance_1 = __webpack_require__(/*! ./performance */ "./src/performance.ts");
var IIK;
(function (IIK) {
    var Project = project_1.Project.Project;
    var ProjectComponent = project_1.Project.ProjectComponent;
    var ProjectComponentModifierType = project_1.Project.ProjectComponentModifierType;
    var Log = logger_1.Logger.Log;
    var DebugLevel = logger_1.Logger.DebugLevel;
    var ArgumentType = graph_1.Graph.ArgumentType;
    var Quaternion = math_1.VMath.Quaternion;
    var Vector2 = math_1.VMath.Vector2;
    var PrimitiveMesh = generator_1.Generator.PrimitiveMesh;
    var ShapedMesh = generator_1.Generator.ShapedMesh;
    var LDSPEdgeType = project_1.Project.LDSPEdgeType;
    var Transform = math_1.VMath.Transform;
    var Vector3 = math_1.VMath.Vector3;
    var PerformanceCall = performance_1.PerformanceCore.PerformanceCall;
    class IIKCore {
        constructor() {
            this.related_calculations = {};
            this.related_names = {};
            this.has_frontend = true;
            this.has_related = true;
            this.is_detailed_materials = false;
            this.max_texture_size = 512;
            this.profiler = new PerformanceCall(undefined, null);
            this.method_cache = {};
            this.default_ldsp_material = "s123mat://b1e0b85e-c613-452f-8b49-0b0dc89d3543";
            this.default_ldsp_cut_material = "s123mat://93d343ca-ce11-4614-a0bb-50f5ff65fe96";
            this.Blocks = [
                {
                    "verbose_name": "РљРѕРЅСЃС‚Р°РЅС‚Р°",
                    "name": "MakeConstant",
                    "color": "#ffffff",
                    "arguments": [
                        {
                            "verbose_name": "Р—РЅР°С‡РµРЅРёРµ",
                            "name": "v",
                            "type": ArgumentType.FLOAT,
                            "value_only": true
                        }
                    ],
                    "result": {
                        "type": ArgumentType.FLOAT
                    }
                },
                {
                    "verbose_name": "РџРѕР»СѓС‡РёС‚СЊ РїР°СЂР°РјРµС‚СЂ РґРµС‚Р°Р»Рё",
                    "name": "GetComponentField",
                    "color": "#ffad6a",
                    "arguments": [
                        {
                            "name": "component",
                            "type": ArgumentType.COMPONENT
                        },
                        {
                            "name": "field_name",
                            "type": ArgumentType.COMPONENT_FIELD
                        }
                    ],
                    "result": {
                        "type": ArgumentType.ANY
                    }
                },
                {
                    "verbose_name": "Р—Р°РґР°С‚СЊ РїР°СЂР°РјРµС‚СЂ РґРµС‚Р°Р»Рё",
                    "color": "#788cff",
                    "name": "SetComponentsFields",
                    "arguments": [
                        {
                            "name": "components",
                            "type": ArgumentType.DICTIONARY,
                            "settings": {
                                "key": ArgumentType.STRING,
                                "value": ArgumentType.COMPONENT,
                            },
                            "is_array": true,
                            "value_only": true
                        },
                        {
                            "name": "fields",
                            "type": ArgumentType.DICTIONARY,
                            "settings": {
                                "key": ArgumentType.COMPONENT_FIELD,
                                "value": ArgumentType.ANY,
                            },
                        },
                    ],
                    "result": {
                        "type": ArgumentType.NONE
                    }
                },
                {
                    "verbose_name": "Р—Р°РґР°С‚СЊ РїРѕРІРѕСЂРѕС‚",
                    "name": "SetComponentRotation",
                    "color": "#df6aff",
                    "arguments": [
                        {
                            "name": "comp",
                            "type": ArgumentType.COMPONENT,
                            "value_only": true
                        },
                        {
                            "name": "ax",
                            "type": ArgumentType.FLOAT
                        },
                        {
                            "name": "ay",
                            "type": ArgumentType.FLOAT
                        },
                        {
                            "name": "az",
                            "type": ArgumentType.FLOAT
                        },
                    ],
                    "result": {
                        "type": ArgumentType.NONE
                    }
                },
                {
                    "verbose_name": "Р—Р°РґР°С‚СЊ РїР°СЂР°РјРµС‚СЂ С‚РѕС‡РєРё",
                    "color": "#6affe9",
                    "name": "SetPositioningPointsFields",
                    "arguments": [
                        {
                            "name": "points",
                            "type": ArgumentType.DICTIONARY,
                            "settings": {
                                "key": ArgumentType.STRING,
                                "value": ArgumentType.POSITIONING_POINT,
                            },
                            "is_array": true,
                            "value_only": true
                        },
                        {
                            "name": "fields",
                            "type": ArgumentType.DICTIONARY,
                            "settings": {
                                "key": ArgumentType.POSITIONING_POINT_FIELD,
                                "value": ArgumentType.ANY,
                            },
                        },
                    ],
                    "result": {
                        "type": ArgumentType.NONE
                    }
                },
                {
                    "verbose_name": "РЎСѓРјРјР°С‚РѕСЂ",
                    "color": "#ff5e5e",
                    "name": "SumRun",
                    "arguments": [
                        {
                            "name": "positive_variables",
                            "verbose_name": "РџРѕР»РѕР¶РёС‚РµР»СЊРЅС‹Рµ",
                            "type": ArgumentType.DICTIONARY,
                            "settings": {
                                "key": ArgumentType.STRING,
                                "value": ArgumentType.ANY,
                            },
                        },
                        {
                            "name": "negative_variables",
                            "verbose_name": "РћС‚СЂРёС†Р°С‚РµР»СЊРЅС‹Рµ",
                            "type": ArgumentType.DICTIONARY,
                            "settings": {
                                "key": ArgumentType.STRING,
                                "value": ArgumentType.ANY,
                            },
                        },
                        {
                            "name": "script",
                            "type": ArgumentType.SCRIPT
                        }
                    ],
                    "result": {
                        "type": ArgumentType.DICTIONARY,
                        "settings": {
                            "key": ArgumentType.STRING,
                            "value": ArgumentType.ANY,
                            "fixed_keys": [
                                "ОЈ"
                            ],
                        },
                    }
                },
                {
                    "verbose_name": "РџРѕР»СѓС‡РёС‚СЊ РІС…РѕРґ",
                    "color": "#7dff63",
                    "name": "GetInputValue",
                    "arguments": [
                        {
                            "name": "input",
                            "type": ArgumentType.INPUT
                        }
                    ],
                    "result": {
                        "type": ArgumentType.ANY
                    }
                },
                {
                    "verbose_name": "Р—Р°РґР°С‚СЊ РІС‹С…РѕРґ",
                    "color": "#fff86a",
                    "name": "SetOutput",
                    "arguments": [
                        {
                            "name": "name",
                            "type": ArgumentType.STRING
                        },
                        {
                            "name": "value",
                            "type": ArgumentType.ANY
                        }
                    ],
                    "result": {
                        "type": ArgumentType.NONE
                    }
                },
                {
                    "verbose_name": "Р—Р°РґР°С‚СЊ РіР°Р±Р°СЂРёС‚С‹ РїСЂРѕРµРєС‚Р°",
                    "name": "SetProjectBounds",
                    "arguments": [
                        {
                            "name": "width",
                            "verbose_name": "РЁРёСЂРёРЅР°",
                            "type": ArgumentType.FLOAT
                        },
                        {
                            "name": "height",
                            "verbose_name": "Р’С‹СЃРѕС‚Р°",
                            "type": ArgumentType.FLOAT
                        },
                        {
                            "name": "depth",
                            "verbose_name": "Р“Р»СѓР±РёРЅР°",
                            "type": ArgumentType.FLOAT
                        },
                    ],
                    "result": {
                        "type": ArgumentType.NONE
                    }
                },
                {
                    "verbose_name": "Р—Р°РґР°С‚СЊ РјР°С‚РµСЂРёР°Р» Р›Р”РЎРџ",
                    "name": "SetLDSPMaterial",
                    "arguments": [
                        {
                            "name": "components",
                            "type": ArgumentType.DICTIONARY,
                            "settings": {
                                "key": ArgumentType.STRING,
                                "value": ArgumentType.COMPONENT,
                            },
                            "is_array": true,
                            "value_only": true
                        },
                        {
                            "name": "material",
                            "type": ArgumentType.STRING
                        },
                    ],
                    "result": {
                        "type": ArgumentType.NONE
                    }
                },
                {
                    "verbose_name": "РЎРѕР±С‹С‚РёРµ",
                    "name": "InvokeEvent",
                    "color": "#b26aff",
                    "arguments": [
                        {
                            "name": "name",
                            "verbose_name": "РРјСЏ С„СѓРЅРєС†РёРё",
                            "type": ArgumentType.STRING,
                        },
                        {
                            "name": "script",
                            "type": ArgumentType.SCRIPT
                        }
                    ],
                    "result": {
                        "type": ArgumentType.NONE
                    }
                },
            ];
        }
        static UpdateOldScript(script) {
            return script.replace(/IIK\.project/g, "this.project");
        }
        SetObjectFieldByString(target, path, value) {
            if (path.includes(".")) {
                const splitted = path.split(".");
                target[splitted[0]][splitted[1]] = value;
            }
            else if (path in target) {
                target[path] = value;
            }
        }
        GetObjectFieldByString(target, path) {
            if (path.includes(".")) {
                const splitted = path.split(".");
                return target[splitted[0]][splitted[1]];
            }
            return target[path];
        }
        InvokeMethod(method_info) {
            return __awaiter(this, void 0, void 0, function* () {
                logger_1.Logger.Log("Invoking method: " + method_info.name, logger_1.Logger.DebugLevel.VERBOSE);
                const args = [];
                for (let a of method_info.arguments)
                    args.push(a);
                if (method_info.result.type === ArgumentType.DICTIONARY)
                    args.push(method_info.result);
                const func = eval("IIKCore.prototype." + method_info.name);
                return yield func.apply(this, args);
            });
        }
        FindNodeByGUID(guid) {
            return this.project.graph.nodes.find(function (n) {
                return n.guid === guid;
            });
        }
        GetValue(attr) {
            return __awaiter(this, void 0, void 0, function* () {
                if (attr.type === ArgumentType.NODE) {
                    const guid = attr.value.node_guid;
                    let result = null;
                    if (guid in this.method_cache)
                        result = this.method_cache[guid];
                    if (result == null) {
                        const m = this.FindNodeByGUID(guid);
                        if (m == null)
                            result = "";
                        else
                            result = yield this.InvokeMethod(m.method);
                        this.method_cache[guid] = result;
                    }
                    if (attr.value.pair_key != null && attr.value.pair_key.length > 0) {
                        for (const r of result)
                            if (r.key === attr.value.pair_key)
                                return r.value;
                        return null;
                    }
                    return result;
                }
                return attr.value;
            });
        }
        MakeConstant(v) {
            return __awaiter(this, void 0, void 0, function* () {
                let result = yield this.GetValue(v);
                if (typeof (result) === "string" && !isNaN(Number(result))) {
                    if (result.includes(","))
                        result = result.replace(",", ".");
                    if (result.includes("."))
                        result = parseFloat(result);
                    else
                        result = parseInt(result);
                }
                return result;
            });
        }
        GetInputValue(name) {
            return __awaiter(this, void 0, void 0, function* () {
                name = yield this.GetValue(name);
                const input = this.project.graph.inputs.find(function (i) {
                    return i.name === name;
                });
                if (input != null) {
                    let result = input.value;
                    if ((input.type === 1 || input.type === 3) && (typeof (result) === "string")) {
                        if (result.includes(","))
                            result = result.replace(",", ".");
                        if (result.includes("."))
                            result = parseFloat(result);
                        else
                            result = parseInt(result);
                    }
                    return result;
                }
                return 0;
            });
        }
        GetComponentField(_comp, _field_name) {
            return __awaiter(this, void 0, void 0, function* () {
                const comp_path = yield this.GetValue(_comp);
                const comp = this.project.GetComponentByPath(comp_path);
                const field_name = yield this.GetValue(_field_name);
                if (field_name in comp)
                    return comp[field_name];
                return comp.modifier[field_name];
            });
        }
        SetComponentsFields(components, fields) {
            return __awaiter(this, void 0, void 0, function* () {
                for (const c of components.value) {
                    const comp_path = c.value;
                    const comp = this.project.GetComponentByPath(comp_path);
                    if (comp == null) {
                        logger_1.Logger.Log(`РџРѕРїС‹С‚РєР° РІС‹СЃС‚Р°РІРёС‚СЊ РїР°СЂР°РјРµС‚СЂС‹ РґР»СЏ РЅРµСЃСѓС‰РµСЃС‚РІСѓСЋС‰РµР№ РґРµС‚Р°Р»Рё: ${comp_path}`, DebugLevel.ERROR);
                        continue;
                    }
                    for (const field of fields.value) {
                        const v = yield this.GetValue(field);
                        if (v == undefined)
                            continue;
                        if (field.key.includes(".")) {
                            const splitted = field.key.split(".");
                            comp[splitted[0]][splitted[1]] = v;
                        }
                        else if (field.key in comp) {
                            comp[field.key] = v;
                            if (field.key == "material" || field.key == "bake")
                                yield this.UpdateMaterialContent(comp); //TODO РєРѕСЃС‚С‹Р»СЊ
                        }
                        else if (field.key in comp.modifier) {
                            comp.modifier[field.key] = v;
                        }
                        else
                            logger_1.Logger.Log(`РџРѕРїС‹С‚РєР° РІС‹СЃС‚Р°РІРёС‚СЊ РЅРµСЃСѓС‰РµСЃС‚РІСѓСЋС‰РёР№ РїР°СЂР°РјРµС‚СЂ ${field.key} РґР»СЏ РґРµС‚Р°Р»Рё ${comp_path}`, DebugLevel.ERROR);
                    }
                }
            });
        }
        SetLDSPMaterial(components, material) {
            return __awaiter(this, void 0, void 0, function* () {
                for (const c of components.value) {
                    const comp_path = c.value;
                    const comp = this.project.GetComponentByPath(comp_path);
                    if (comp.modifier.type != ProjectComponentModifierType.LDSP)
                        continue;
                    const actual_material = yield this.GetValue(material);
                    comp.material = actual_material;
                    comp.modifier.back_material = actual_material;
                    for (var edge of comp.modifier.edges)
                        edge.material = actual_material;
                    yield this.UpdateMaterialContent(comp);
                }
            });
        }
        SetComponentRotation(_comp, ax, ay, az) {
            return __awaiter(this, void 0, void 0, function* () {
                const comp_path = yield this.GetValue(_comp);
                const comp = this.project.GetComponentByPath(comp_path);
                const rx = yield this.GetValue(ax);
                const ry = yield this.GetValue(ay);
                const rz = yield this.GetValue(az);
                comp.rotation = math_1.VMath.Quaternion.euler(rx, ry, rz);
            });
        }
        SetPositioningPointsFields(points, fields) {
            return __awaiter(this, void 0, void 0, function* () {
                for (const v of points.value) {
                    const pp_path = v.value;
                    const pp = this.project.GetPositioningPointByPath(pp_path);
                    if (pp == null) {
                        logger_1.Logger.Log(`РџРѕРїС‹С‚РєР° РІС‹СЃС‚Р°РІРёС‚СЊ РїР°СЂР°РјРµС‚СЂС‹ РґР»СЏ РЅРµСЃСѓС‰РµСЃС‚РІСѓСЋС‰РµР№ С‚РѕС‡РєРё РїРѕР·РёС†РёРѕРЅРёСЂРѕРІР°РЅРёСЏ: ${pp_path}`, DebugLevel.ERROR);
                        continue;
                    }
                    for (const field of fields.value) {
                        const v = yield this.GetValue(field);
                        if (v == undefined)
                            continue;
                        if (field.key.includes(".")) {
                            const splitted = field.key.split(".");
                            pp[splitted[0]][splitted[1]] = v;
                        }
                        else if (field.key in pp)
                            pp[field.key] = v;
                        else
                            logger_1.Logger.Log(`РџРѕРїС‹С‚РєР° РІС‹СЃС‚Р°РІРёС‚СЊ РЅРµСЃСѓС‰РµСЃС‚РІСѓСЋС‰РёР№ РїР°СЂР°РјРµС‚СЂ ${field.key} РґР»СЏ С‚РѕС‡РєРё РїРѕР·РёС†РёРѕРЅРёСЂРѕРІР°РЅРёСЏ ${pp_path}`, DebugLevel.ERROR);
                    }
                }
            });
        }
        SumRun(positive_variables, negative_variables, script, result) {
            return __awaiter(this, void 0, void 0, function* () {
                let var_string = "";
                let buffer_string = "";
                let sum = 0;
                let buffer = {};
                if (positive_variables.value != null) {
                    for (let pval of positive_variables.value) {
                        const key = pval.key;
                        let value = yield this.GetValue(pval);
                        if (value == null)
                            value = "";
                        if (typeof value === 'string') {
                            if (value == "" || isNaN(Number(value)))
                                value = "\"" + value + "\"";
                        }
                        var_string += "const " + key + " = " + value + ";\n";
                        sum += value;
                    }
                }
                if (negative_variables.value != null) {
                    for (let nval of negative_variables.value) {
                        const key = nval.key;
                        let value = yield this.GetValue(nval);
                        if (value == null)
                            value = "";
                        if (typeof value === 'string') {
                            if (value == "" || isNaN(Number(value)))
                                value = "\"" + value + "\"";
                        }
                        var_string += "const " + key + " = " + value + ";\n";
                        sum -= value;
                    }
                }
                for (let val of result.value) {
                    if (val.key === "ОЈ")
                        continue;
                    var_string += "let " + val.key + ";\n";
                    buffer_string += "buffer." + val.key + " = " + val.key + ";\n";
                }
                let target_string = var_string;
                if (script.value != null)
                    target_string += IIKCore.UpdateOldScript(script.value) + "\n";
                target_string += buffer_string;
                const self = this;
                if (target_string.includes("await")) {
                    let async_script_func = null;
                    target_string = `async_script_func = async function(){ ${target_string} };`;
                    eval(target_string);
                    yield async_script_func();
                }
                else {
                    eval(target_string);
                }
                for (let val of result.value) {
                    if (val.key === "ОЈ")
                        continue;
                    val.value = buffer[val.key];
                }
                for (let val of result.value) {
                    if (val.key === "ОЈ")
                        val.value = sum;
                }
                return result.value;
            });
        }
        SetOutput(name, value) {
            return __awaiter(this, void 0, void 0, function* () {
                this.outputs[name.value] = yield this.GetValue(value);
            });
        }
        SetProjectBounds(width, height, depth) {
            //TODO remove
        }
        CreateComponent(name, type) {
            const comp = new ProjectComponent(this.project);
            comp.name = name;
            comp.path = "Р”РµС‚Р°Р»Рё";
            comp.modifier = { "type": type };
            this.project.components.push(comp);
            return comp;
        }
        DeleteComponent(path) {
            const comp_index = this.project.components.findIndex(function (c) {
                return c.path + "/" + c.name === path;
            });
            delete this.project.components[comp_index];
        }
        GetSerializedBlocks() {
            return JSON.stringify(this.Blocks);
        }
        PrepareInputs() {
            if (this.inputs != null) {
                for (let input of this.inputs) {
                    const pr_input = this.project.graph.inputs.find(function (i) {
                        return i.guid == input.guid;
                    });
                    if (pr_input === undefined)
                        continue;
                    pr_input.value = input.value;
                    if (input.settings != null)
                        pr_input.settings = input.settings;
                    if (input.is_active != null)
                        pr_input.is_active = input.is_active;
                }
            }
            if (this.parent_project != null && this.parent_project.graph.outputs != null) {
                for (let input of this.project.graph.inputs) {
                    if (input.settings.tag != null && input.settings.tag != "") {
                        const tagged_output = this.parent_project.graph.outputs.find(function (o) { return o.tag == input.settings.tag; });
                        if (tagged_output != null) {
                            input.value = tagged_output.value;
                        }
                    }
                }
            }
            this.inputs = this.project.graph.inputs;
        }
        Prepare(parent_profiler = null) {
            if (parent_profiler == null)
                this.profiler = new PerformanceCall(undefined, null);
            else
                this.profiler = parent_profiler;
            const call = new PerformanceCall("Prepare", this.profiler);
            call.StartSample();
            Log("Preparing project copy", DebugLevel.INFO);
            this.initial_project = new Project(JSON.parse(JSON.stringify(this.project)));
            Log("Preparing main project", DebugLevel.INFO);
            this.project = new Project(this.project);
            if (this.related_inputs == null || Object.keys(this.related_inputs).length == 0)
                this.related_inputs = this.project.graph.related_inputs;
            Log("Preparing inputs", DebugLevel.INFO);
            this.PrepareInputs();
            Log("Done preparing inputs", DebugLevel.INFO);
            this.method_cache = {};
            this.outputs = {};
            this.out_nodes = this.FindOutputNodes(this.project.graph.nodes);
            Log("Prepare done", DebugLevel.INFO);
            call.EndSample();
        }
        GetEdgeCutAngles(index, edges) {
            let cut_angle1 = 0;
            let cut_angle2 = 0;
            if (edges[index].type == project_1.Project.LDSPEdgeType.MM2) {
                if (index == 0) {
                    if (edges[1].type == project_1.Project.LDSPEdgeType.MM2)
                        cut_angle2 = -45;
                    if (edges[3].type == project_1.Project.LDSPEdgeType.MM2)
                        cut_angle1 = 45;
                }
                if (index == 1) {
                    if (edges[0].type == project_1.Project.LDSPEdgeType.MM2)
                        cut_angle2 = -45;
                    if (edges[2].type == project_1.Project.LDSPEdgeType.MM2)
                        cut_angle1 = 45;
                }
                if (index == 2) {
                    if (edges[1].type == project_1.Project.LDSPEdgeType.MM2)
                        cut_angle2 = -45;
                    if (edges[3].type == project_1.Project.LDSPEdgeType.MM2)
                        cut_angle1 = 45;
                }
                if (index == 3) {
                    if (edges[0].type == project_1.Project.LDSPEdgeType.MM2)
                        cut_angle1 = 45;
                    if (edges[2].type == project_1.Project.LDSPEdgeType.MM2)
                        cut_angle2 = -45;
                }
            }
            return [cut_angle1, cut_angle2];
        }
        PrepareFrontend(parent_call) {
            return __awaiter(this, void 0, void 0, function* () {
                const call = parent_call.GetOrCreateChild("PrepareFrontend");
                logger_1.Logger.Log("Start preparing content", logger_1.Logger.DebugLevel.INFO);
                logger_1.Logger.Log("MaxTextureSize: " + this.max_texture_size, logger_1.Logger.DebugLevel.INFO);
                const container = {};
                const children = {};
                for (let comp of this.project.components) {
                    if ("is_active" in comp && !comp.is_active)
                        continue;
                    const child = {};
                    child.transform = math_1.VMath.Transform.identity();
                    child.name = comp.name;
                    child.build_order = 0;
                    if (comp.build_order != null)
                        child.build_order = comp.build_order;
                    const comp_processings = comp.processings == null ? [] : comp.processings;
                    if (comp.modifier.type === ProjectComponentModifierType.ARRAY) {
                        const array_call = call.GetOrCreateChild("ProcessArray");
                        array_call.StartSample();
                        child.type = "array";
                        child.transform.position = comp.position.div(1000.0).invertX();
                        child.transform.rotation = comp.rotation.toRH();
                        child.component = {};
                        child.component.transform = math_1.VMath.Transform.identity();
                        child.component_transforms = [];
                        if (comp.modifier.type == ProjectComponentModifierType.MESH) {
                            const mesh_call = array_call.GetOrCreateChild("ProcessExtendedMesh");
                            mesh_call.StartSample();
                            child.component.type = "model_node";
                            child.component.file = comp.modifier.child.modifier.file;
                            child.component.node = comp.modifier.child.guid;
                            child.component.transform.position = comp.modifier.child.position.div(1000.0).invertX();
                            child.component.transform.rotation = comp.modifier.child.rotation.toRH();
                            //child.component.transform.scale = this.GetComponentScale(comp);
                            mesh_call.EndSample();
                        }
                        else {
                            const simple_call = array_call.GetOrCreateChild("ProcessDummyOrShape");
                            simple_call.StartSample();
                            child.component.type = "geometry";
                            child.component.file = "model.fbx";
                            child.component.node = comp.modifier.child.guid;
                            child.component.transform.position = comp.modifier.child.position.div(1000.0).invertX();
                            child.component.transform.rotation = comp.modifier.child.rotation.toRH();
                            child.component.geometry = yield comp.modifier.child.GetGeneratedMesh();
                            simple_call.EndSample();
                        }
                        child.component.material_content = comp.material_content;
                        const offset = new Vector3(comp.modifier.offset);
                        for (let i = 0; i < comp.modifier.count; i++) {
                            const tr = math_1.VMath.Transform.identity();
                            tr.position = offset.mult(i).sub(offset.mult(comp.modifier.count - 1).div(2.0)).div(1000.0);
                            tr.rotation = new Quaternion(comp.modifier.child.rotation).toRH();
                            child.component_transforms.push(tr);
                        }
                        array_call.EndSample();
                    }
                    else if (comp.modifier.type === ProjectComponentModifierType.PERFORATION) {
                        const perforation_call = call.GetOrCreateChild("ProcessPerforation");
                        perforation_call.StartSample();
                        child.type = "model_group";
                        child.children = {};
                        child.transform.position = comp.position.div(1000.0).invertX();
                        child.transform.rotation = comp.rotation.toRH();
                        let plane_size = comp.size.div(1000.0);
                        //const mesh : Mesh = PerformanceCall.Invoke( `Mesh generation for ${comp.name}`, () => new PrimitiveMesh( plane_size, 0, 0 ), perforation_call );
                        const mesh = new PrimitiveMesh(plane_size, 0, 0);
                        const main = {};
                        main.type = "geometry";
                        main.transform = Transform.identity();
                        main.geometry = mesh;
                        main.material = comp.material;
                        main.material_content = comp.material_content;
                        main.material_content.web.alphaTest = 0.5;
                        main.material_content.web.transparent = true;
                        child.children["main"] = main;
                        let plane_material = new material_1.Material();
                        plane_material.UpdateFrom(main.material_content);
                        delete plane_material.ao;
                        delete plane_material.diffuse;
                        delete plane_material.metallic;
                        delete plane_material.normal;
                        delete plane_material.roughness;
                        delete plane_material.emissive;
                        plane_material.color = "#000000";
                        plane_material.web.alphaTest = 0.5;
                        plane_material.web.transparent = true;
                        for (let i = 0; i < comp.modifier.depth; i++) {
                            const z = comp.size.z / 1000.0;
                            const plane1 = {};
                            plane1.type = "geometry";
                            plane1.transform = Transform.identity();
                            plane1.geometry = mesh;
                            plane1.material = comp.material;
                            plane1.material_content = plane_material;
                            const plane2 = {};
                            plane2.type = "geometry";
                            plane2.geometry = mesh;
                            plane2.transform = Transform.identity();
                            plane2.material = comp.material;
                            plane2.material_content = plane_material;
                            const offset = (i + 2.0) / comp.modifier.depth * (z / 2.1);
                            plane1.transform.position = new math_1.VMath.Vector3(0, 0, z / 2.0 - offset);
                            plane2.transform.position = new math_1.VMath.Vector3(0, 0, -z / 2.0 + offset);
                            plane1.transform.scale = new Vector3(1, 1, 0.00001);
                            plane2.transform.scale = new Vector3(1, 1, 0.00001);
                            child.children["plane1_" + i] = plane1;
                            child.children["plane2_" + i] = plane2;
                        }
                        perforation_call.EndSample();
                    }
                    else if (comp.modifier.type === ProjectComponentModifierType.LDSP) {
                        const ldsp_call = call.GetOrCreateChild("ProcessLDSP");
                        ldsp_call.StartSample();
                        child.type = "model_group";
                        child.children = {};
                        child.transform.position = comp.position.div(1000.0).invertX();
                        child.transform.rotation = comp.rotation.toRH();
                        let plane_size = new math_1.VMath.Vector3(comp.size.x, comp.size.y, 0.01).div(1000.0);
                        const shape_2x16 = yield filesystem_1.Filesystem.GetFile("https://lab.system123.ru/common/models/2x16.s123drawing");
                        for (let i in shape_2x16)
                            shape_2x16[i] = new Vector2(shape_2x16[i]);
                        //FRONT SIDE
                        {
                            const side = {};
                            side.type = "geometry";
                            side.transform = math_1.VMath.Transform.identity();
                            side.transform.position = new math_1.VMath.Vector3(0, 0, comp.size.z / 2000.0).invertX();
                            side.geometry = new PrimitiveMesh(plane_size, 0, 0);
                            side.material = this.default_ldsp_material;
                            if (comp.material != null && comp.material != "")
                                side.material = comp.material;
                            side.material_content = yield ProjectComponent.GetMaterialData(side.material, "", this.is_detailed_materials, this.max_texture_size);
                            child.children["front"] = side;
                        }
                        //BACK SIDE
                        {
                            const side = {};
                            side.type = "geometry";
                            side.transform = math_1.VMath.Transform.identity();
                            side.transform.position = new math_1.VMath.Vector3(0, 0, -comp.size.z / 2000.0).invertX();
                            side.geometry = new PrimitiveMesh(plane_size, 0, 0);
                            side.material = this.default_ldsp_material;
                            if (comp.modifier.back_material != null && comp.modifier.back_material != "")
                                side.material = comp.modifier.back_material;
                            side.material_content = yield ProjectComponent.GetMaterialData(side.material, "", this.is_detailed_materials, this.max_texture_size);
                            child.children["back"] = side;
                        }
                        //LEFT EDGE
                        {
                            const edge = {};
                            edge.type = "geometry";
                            edge.transform = math_1.VMath.Transform.identity();
                            edge.transform.rotation = math_1.VMath.Quaternion.euler(0, 0, 0).toRH();
                            if (comp.modifier.edges[0].type == project_1.Project.LDSPEdgeType.MM2) {
                                const cut_angles = this.GetEdgeCutAngles(0, comp.modifier.edges);
                                edge.geometry = ShapedMesh.Construct(new math_1.VMath.Vector3(2, comp.size.y, comp.size.z).div(1000.0), shape_2x16, cut_angles[0], cut_angles[1]);
                                edge.transform.position = new math_1.VMath.Vector3((-comp.size.x - 2) / 2000.0, 0, 0);
                            }
                            else {
                                edge.geometry = new PrimitiveMesh(new math_1.VMath.Vector3(0.03, comp.size.y, comp.size.z).div(1000.0), 0, 0);
                                edge.transform.position = new math_1.VMath.Vector3((-comp.size.x - 0.03) / 2000.0, 0, 0);
                            }
                            edge.material = this.default_ldsp_cut_material;
                            if (comp.modifier.edges[0].type != project_1.Project.LDSPEdgeType.NONE)
                                edge.material = comp.modifier.edges[0].material;
                            edge.material_content = yield ProjectComponent.GetMaterialData(edge.material, "", this.is_detailed_materials, this.max_texture_size);
                            child.children["left"] = edge;
                        }
                        //TOP EDGE
                        {
                            const edge = {};
                            edge.type = "geometry";
                            edge.transform = math_1.VMath.Transform.identity();
                            edge.transform.rotation = math_1.VMath.Quaternion.euler(-180, 0, -90).toRH();
                            if (comp.modifier.edges[1].type == project_1.Project.LDSPEdgeType.MM2) {
                                const cut_angles = this.GetEdgeCutAngles(1, comp.modifier.edges);
                                edge.geometry = ShapedMesh.Construct(new math_1.VMath.Vector3(2, comp.size.x, comp.size.z).div(1000.0), shape_2x16, cut_angles[0], cut_angles[1]);
                                edge.transform.position = new math_1.VMath.Vector3(0, (comp.size.y + 2) / 2000.0, 0);
                            }
                            else {
                                edge.geometry = new PrimitiveMesh(new math_1.VMath.Vector3(0.03, comp.size.x, comp.size.z).div(1000.0), 0, 0);
                                edge.transform.position = new math_1.VMath.Vector3(0, (comp.size.y + 0.03) / 2000.0, 0);
                            }
                            edge.material = this.default_ldsp_cut_material;
                            if (comp.modifier.edges[1].type != project_1.Project.LDSPEdgeType.NONE)
                                edge.material = comp.modifier.edges[1].material;
                            edge.material_content = yield ProjectComponent.GetMaterialData(edge.material, "", this.is_detailed_materials, this.max_texture_size);
                            child.children["top"] = edge;
                        }
                        //RIGHT EDGE
                        {
                            const edge = {};
                            edge.type = "geometry";
                            edge.transform = math_1.VMath.Transform.identity();
                            edge.transform.rotation = math_1.VMath.Quaternion.euler(0, 180, 0).toRH();
                            if (comp.modifier.edges[2].type == project_1.Project.LDSPEdgeType.MM2) {
                                const cut_angles = this.GetEdgeCutAngles(2, comp.modifier.edges);
                                edge.geometry = ShapedMesh.Construct(new math_1.VMath.Vector3(2, comp.size.y, comp.size.z).div(1000.0), shape_2x16, cut_angles[0], cut_angles[1]);
                                edge.transform.position = new math_1.VMath.Vector3((comp.size.x + 2) / 2000.0, 0, 0);
                            }
                            else {
                                edge.geometry = new PrimitiveMesh(new math_1.VMath.Vector3(0.03, comp.size.y, comp.size.z).div(1000.0), 0, 0);
                                edge.transform.position = new math_1.VMath.Vector3((comp.size.x + 0.03) / 2000.0, 0, 0);
                            }
                            edge.material = this.default_ldsp_cut_material;
                            if (comp.modifier.edges[2].type != project_1.Project.LDSPEdgeType.NONE)
                                edge.material = comp.modifier.edges[2].material;
                            edge.material_content = yield ProjectComponent.GetMaterialData(edge.material, "", this.is_detailed_materials, this.max_texture_size);
                            child.children["right"] = edge;
                        }
                        //BOTTOM EDGE
                        {
                            const edge = {};
                            edge.type = "geometry";
                            edge.transform = math_1.VMath.Transform.identity();
                            edge.transform.rotation = math_1.VMath.Quaternion.euler(180, 0, 90).toRH();
                            if (comp.modifier.edges[3].type == project_1.Project.LDSPEdgeType.MM2) {
                                const cut_angles = this.GetEdgeCutAngles(3, comp.modifier.edges);
                                edge.geometry = ShapedMesh.Construct(new math_1.VMath.Vector3(2, comp.size.x, comp.size.z).div(1000.0), shape_2x16, cut_angles[0], cut_angles[1]);
                                edge.transform.position = new math_1.VMath.Vector3(0, (-comp.size.y - 2) / 2000.0, 0);
                            }
                            else {
                                edge.geometry = new PrimitiveMesh(new math_1.VMath.Vector3(0.03, comp.size.x, comp.size.z).div(1000.0), 0, 0);
                                edge.transform.position = new math_1.VMath.Vector3(0, (-comp.size.y - 0.03) / 2000.0, 0);
                            }
                            edge.material = this.default_ldsp_cut_material;
                            if (comp.modifier.edges[3].type != project_1.Project.LDSPEdgeType.NONE)
                                edge.material = comp.modifier.edges[3].material;
                            edge.material_content = yield ProjectComponent.GetMaterialData(edge.material, "", this.is_detailed_materials, this.max_texture_size);
                            child.children["bottom"] = edge;
                        }
                        ldsp_call.EndSample();
                    }
                    else if (comp.modifier.type === ProjectComponentModifierType.MESH && (!("processings" in comp) || comp_processings.find(function (p) {
                        return p.type === 3;
                    }) == null)) {
                        const mesh_call = call.GetOrCreateChild("ProcessMesh");
                        mesh_call.StartSample();
                        child.type = "model_node";
                        child.file = filesystem_1.Filesystem.GetRelativePath(comp.modifier.mesh);
                        child.node = comp.modifier.node_name;
                        if (child.node == null)
                            child.node = "";
                        child.transform.position = comp.position.div(1000.0).invertX();
                        child.transform.rotation = comp.rotation.toRH();
                        if (comp.modifier.use_scale === true) {
                            const size = comp.size.div(1000.0);
                            const nsize = new math_1.VMath.Vector3(comp.modifier.mesh_size);
                            child.transform.scale = size.div(nsize);
                        }
                        if (!comp.modifier.apply_offset && comp.modifier.mesh_offset !== undefined) {
                            let n = new math_1.VMath.Vector3(comp.modifier.mesh_offset).invertX();
                            if (comp.modifier.use_scale)
                                n = math_1.VMath.Vector3.scale(n, child.transform.scale);
                            n = new math_1.VMath.Quaternion(child.transform.rotation).rotate(n);
                            child.transform.position = comp.position.div(1000.0).invertX().sub(n);
                        }
                        mesh_call.EndSample();
                    }
                    else if (comp.modifier.type == ProjectComponentModifierType.BUILTIN) {
                        const builting_call = call.GetOrCreateChild("ProcessBuiltin");
                        builting_call.StartSample();
                        child.type = "calculation";
                        child.file = comp.modifier.related_project;
                        child.transform.position = comp.position.div(1000.0).invertX();
                        child.transform.rotation = comp.rotation.toRH();
                        const child_iik = this.related_calculations[comp.guid];
                        child.transform.position = child.transform.position.sub(child_iik.frontend.product_container.transform.position);
                        child.transform.rotation = child.transform.rotation.mult(child_iik.frontend.product_container.transform.rotation.inversed());
                        if (this.has_related)
                            child.content = child_iik.frontend;
                        builting_call.EndSample();
                    }
                    else if (comp.modifier.type == ProjectComponentModifierType.MESH) {
                        const mesh_call = call.GetOrCreateChild("ProcessExtendedMesh");
                        mesh_call.StartSample();
                        child.type = "model_node";
                        child.file = "model.fbx";
                        child.node = comp.guid;
                        child.transform.position = comp.position.div(1000.0).invertX();
                        child.transform.rotation = comp.rotation.toRH();
                        child.transform.scale = this.GetComponentScale(comp);
                        mesh_call.EndSample();
                    }
                    else {
                        const simple_call = call.GetOrCreateChild("ProcessDummy");
                        simple_call.StartSample();
                        child.type = "geometry";
                        child.file = "model.fbx";
                        child.node = comp.guid;
                        child.transform.position = comp.position.div(1000.0).invertX();
                        child.transform.rotation = comp.rotation.toRH();
                        child.geometry = yield comp.GetGeneratedMesh();
                        simple_call.EndSample();
                    }
                    if (child.type == "model_node" || child.type == "geometry") {
                        const materialsuv_call = call.GetOrCreateChild("ProcessMaterialsAndUVs");
                        materialsuv_call.StartSample();
                        child.material = filesystem_1.Filesystem.GetRelativePath(comp.material);
                        child.material_content = comp.material_content;
                        child.bake = filesystem_1.Filesystem.GetRelativePath(comp.bake);
                        if (this.max_texture_size == 0) {
                            if ("max_texture_size" in comp && comp.max_texture_size != 0)
                                child.max_texture_size = comp.max_texture_size;
                        }
                        else
                            child.max_texture_size = this.max_texture_size;
                        const uvs = {};
                        const tex_coord_proc = comp_processings.find(function (p) {
                            return p.type === 2;
                        });
                        if (tex_coord_proc != null) {
                            uvs.offset = tex_coord_proc.offset;
                            uvs.scale = tex_coord_proc.scale;
                            uvs.rotation = tex_coord_proc.rotation;
                        }
                        //if( this.project.global_uv_offset )
                        //{
                        //    if( uvs.offset )
                        //        uvs.offset = uvs.offset.add( this.project.global_uv_offset );
                        //    else
                        //        uvs.offset = this.project.global_uv_offset;
                        //}
                        child["uvs"] = uvs;
                        materialsuv_call.EndSample();
                    }
                    children[comp.guid] = child;
                }
                const virtual_objects_call = call.GetOrCreateChild("ProcessVirtualObjects");
                virtual_objects_call.StartSample();
                const virtual_objects = {};
                for (let vo of this.project.virtual_objects) {
                    const child = {};
                    if (vo.modifier.type === 1) {
                        child.type = "shadow_plane";
                        child.guid = vo.guid;
                        child.transform = math_1.VMath.Transform.identity();
                        child.transform.position = vo.position.div(1000.0).invertX();
                        child.transform.rotation = vo.modifier.rotation.toRH();
                        child.transform.scale = new math_1.VMath.Vector3(vo.modifier.size.x / 1000.0, 1.0, vo.modifier.size.y / 1000.0);
                        child.data = {};
                        child.data.shadow_type = vo.modifier.shadow_type;
                        if (vo.modifier.shadow_type === 0)
                            child.data.path = vo.modifier.settings.path;
                    }
                    else if (vo.modifier.type === 3) {
                        child.type = "camera";
                        child.guid = vo.guid;
                        child.transform = math_1.VMath.Transform.identity();
                        child.transform.position = vo.position.div(1000.0).invertX();
                        child.transform.rotation = vo.modifier.rotation.toRH();
                        child.data = {};
                        child.data.camera_type = vo.modifier.camera_type;
                        const spherical_rotation = vo.modifier.spheric_rotation;
                        if (child.data.camera_type === 1 && spherical_rotation !== undefined)
                            child.data.spheric_rotation = {
                                "x": spherical_rotation.x / 180.0 * Math.PI,
                                "y": spherical_rotation.y / 180.0 * Math.PI
                            };
                        child.data.order = vo.modifier.order;
                        let is_pov = 0;
                        if (vo.modifier.is_pov === true)
                            is_pov = 1;
                        child.data.is_pov = is_pov;
                        if (child.data.camera_type === 2)
                            child.data.radius = vo.modifier.settings.distance / 1000.0;
                    }
                    else if (vo.modifier.type == 2) {
                        child.type = "light";
                        child.guid = vo.guid;
                        child.transform = math_1.VMath.Transform.identity();
                        child.transform.position = vo.position.div(1000.0).invertX();
                        if ("rotation" in vo.modifier.settings)
                            child.transform.rotation = new Quaternion(vo.modifier.settings.rotation).toRH();
                        child.data = {};
                        child.data.light_type = vo.modifier.light_type;
                        child.data.color = vo.modifier.settings.color;
                        child.data.intensity = vo.modifier.settings.intensity;
                        child.data.temperature = vo.modifier.settings.temperature;
                        child.data.range = vo.modifier.settings.range;
                        child.data.angle = vo.modifier.settings.angle;
                        child.data.isCastingShadows = vo.modifier.isCastingShadows;
                        child.data.spotInnerAngle = vo.modifier.settings.spotInnerAngle;
                        child.data.spotOuterAngle = vo.modifier.settings.spotOuterAngle;
                    }
                    else {
                        continue;
                    }
                    virtual_objects[vo.name] = child;
                }
                virtual_objects_call.EndSample();
                container.virtual_objects = virtual_objects;
                container.children = children;
                container.type = "group";
                container.transform = math_1.VMath.Transform.identity();
                const rotation = this.project.GetRotation().toRH();
                const offset = rotation.rotate(this.project.GetOffset().invertX());
                container.transform.position = offset.inversed();
                container.transform.rotation = rotation;
                container.normal = this.project.normal;
                this.frontend = {};
                this.frontend.product_container = container;
                logger_1.Logger.Log("Done preparing content", logger_1.Logger.DebugLevel.VERBOSE);
            });
        }
        GetComponentScale(comp) {
            const init_comp = this.initial_project.GetComponentByPath(comp.path + "/" + comp.name);
            if (init_comp == null)
                throw new Error("ERROR");
            const scale = comp.size.div(init_comp.size);
            if (scale.isZero() || !scale.isFinite())
                return math_1.VMath.Vector3.one;
            return scale;
        }
        FindOutputNodes(arr) {
            const iikSetComponentsFields = [];
            const iikSetPositioningPointsFields = [];
            const iikSetConnectionPointsFields = [];
            const iikSetProcessingsFields = [];
            const iikSetLightSourcesFields = [];
            const iikSetProjectBounds = [];
            const iikSetOutput = [];
            const iikSetGenerationRequired = [];
            //const iikSumRun = [];
            const iikSetComponentRotation = [];
            const iikSetLDSPMaterial = [];
            for (const a of arr) {
                switch (a["method"]["name"]) {
                    case "SetComponentsFields":
                        iikSetComponentsFields.push(a);
                        break;
                    case "SetPositioningPointsFields":
                        iikSetPositioningPointsFields.push(a);
                        break;
                    case "SetConnectionPointsFields":
                        iikSetConnectionPointsFields.push(a);
                        break;
                    case "SetProcessingsFields":
                        iikSetProcessingsFields.push(a);
                        break;
                    case "SetLightSourcesFields":
                        iikSetLightSourcesFields.push(a);
                        break;
                    case "SetProjectBounds":
                        iikSetProjectBounds.push(a);
                        break;
                    case "SetOutput":
                        iikSetOutput.push(a);
                        break;
                    case "SetGenerationRequired":
                        iikSetGenerationRequired.push(a);
                        break;
                    //case "SumRun":
                    //    iikSumRun.push( a );
                    //    break;
                    case "SetComponentRotation":
                        iikSetComponentRotation.push(a);
                        break;
                    case "SetLDSPMaterial":
                        iikSetLDSPMaterial.push(a);
                        break;
                    default:
                        break;
                }
            }
            return iikSetComponentsFields.concat(iikSetPositioningPointsFields, iikSetConnectionPointsFields, iikSetProcessingsFields, iikSetLightSourcesFields, iikSetProjectBounds, iikSetOutput, iikSetGenerationRequired, iikSetComponentRotation, iikSetLDSPMaterial
            //iikSumRun
            );
        }
        UpdateInputs() {
            for (const input of this.inputs) {
                if ("min" in input.settings) {
                    if (input.value < input.settings.min)
                        input.value = input.settings.min;
                }
                if ("max" in input.settings) {
                    if (input.value > input.settings.max)
                        input.value = input.settings.max;
                }
            }
        }
        UpdateOutputs() {
            for (let output of this.project.graph.outputs) {
                if (output.tag != null && output.tag != "") {
                    const tagged_input = this.project.graph.inputs.find(function (i) { return i.settings.tag == output.tag; });
                    if (tagged_input != null) {
                        output.value = tagged_input.value;
                        continue;
                    }
                }
                if (this.outputs[output.name] != undefined)
                    output.value = this.outputs[output.name];
            }
            this.outputs = this.project.graph.outputs;
        }
        UpdateMaterialContent(comp) {
            return __awaiter(this, void 0, void 0, function* () {
                const self = this;
                function GetMaterialData(material) {
                    return __awaiter(this, void 0, void 0, function* () {
                        return yield ProjectComponent.GetMaterialData(material, self.project.guid, self.is_detailed_materials, self.max_texture_size);
                    });
                }
                comp.material_content = yield comp.GetMaterialData(this.is_detailed_materials, this.max_texture_size);
                if (comp.modifier.type == 9) {
                    if (comp.material == null || comp.material == "")
                        comp.material_content = yield GetMaterialData(this.default_ldsp_material);
                    else
                        comp.material_content = yield comp.GetMaterialData(this.is_detailed_materials, this.max_texture_size);
                    if (comp.modifier.back_material == null || comp.modifier.back_material == "")
                        comp.modifier.back_material_content = yield GetMaterialData(this.default_ldsp_material);
                    else
                        comp.modifier.back_material_content = yield GetMaterialData(comp.modifier.back_material);
                    for (let e of comp.modifier.edges) {
                        if (e.type == LDSPEdgeType.NONE)
                            e.material_content = yield GetMaterialData(this.default_ldsp_cut_material);
                        else
                            e.material_content = yield GetMaterialData(e.material);
                    }
                }
                else {
                    comp.material_content = yield comp.GetMaterialData(this.is_detailed_materials, this.max_texture_size);
                }
            });
        }
        GetMaterialContent(url) {
            return __awaiter(this, void 0, void 0, function* () {
                const result = yield ProjectComponent.GetMaterialData(url, this.project.guid, this.is_detailed_materials, this.max_texture_size);
                return JSON.stringify(result);
            });
        }
        PrepareMaterials() {
            return __awaiter(this, void 0, void 0, function* () {
                logger_1.Logger.Log("Preparing material contents", logger_1.Logger.DebugLevel.VERBOSE);
                for (const comp of this.project.components)
                    yield this.UpdateMaterialContent(comp);
                logger_1.Logger.Log("Done preparing materials", logger_1.Logger.DebugLevel.VERBOSE);
            });
        }
        CalculateRelated(comp, parent_call) {
            return __awaiter(this, void 0, void 0, function* () {
                const call = parent_call.GetOrCreateChild("CalculateRelated");
                call.StartSample();
                if (comp.modifier.related_project == null || comp.modifier.related_project == "")
                    return null;
                const project_guid = comp.modifier.related_project.replace("s123calc://", "");
                const info = yield filesystem_1.Filesystem.GetFile(`s123://calculationResults/${project_guid}/info.json`);
                if (info == null)
                    return null;
                const project_fname = info.project;
                const child_iik = new IIKCore();
                child_iik.inputs = null;
                child_iik.related_inputs = null;
                child_iik.project = yield filesystem_1.Filesystem.GetFile(`s123://calculationResults/${project_guid}/${project_fname}`);
                child_iik.project.guid = project_guid;
                child_iik.project.global_uv_offset = new Vector2(Math.random(), Math.random() * 100);
                child_iik.has_frontend = this.has_frontend;
                child_iik.max_texture_size = this.max_texture_size;
                child_iik.is_detailed_materials = this.is_detailed_materials;
                child_iik.parent_project = this.project;
                if (this.related_inputs != null && comp.guid in this.related_inputs) {
                    let not_found = false;
                    for (let ri of this.related_inputs[comp.guid]) {
                        if (child_iik.project.graph.inputs.find(function (i) { return i.guid == ri.guid; }) == null) {
                            not_found = true;
                            break;
                        }
                    }
                    if (not_found)
                        this.related_inputs[comp.guid] = child_iik.project.graph.inputs;
                    else {
                        const child_inputs = child_iik.project.graph.inputs;
                        for (let i = 0; i < child_inputs.length; i++) {
                            const related_input = this.related_inputs[comp.guid].find((inp) => inp.guid == child_inputs[i].guid);
                            if (related_input != null)
                                child_inputs[i] = related_input;
                        }
                    }
                }
                for (let input of child_iik.project.graph.inputs) {
                    if (input.settings.tag != null && input.settings.tag != "") {
                        const value = this.GetObjectFieldByString(comp, input.settings.tag);
                        if (value != undefined)
                            input.value = value;
                    }
                }
                child_iik.Prepare(call);
                if (this.related_event != null && this.related_event.guid == comp.guid) {
                    yield child_iik.Invoke(this.related_event.name, this.related_event.value);
                    this.related_event = null;
                }
                yield child_iik.Calculate();
                if (this.related_inputs == null)
                    this.related_inputs = {};
                this.related_inputs[comp.guid] = child_iik.inputs;
                comp.size = child_iik.project.bounds.size.mult(1000.0);
                call.EndSample();
                return child_iik;
            });
        }
        UpdateComponentsSize() {
            return __awaiter(this, void 0, void 0, function* () {
                for (let comp of this.project.components) {
                    if (comp.modifier.type == ProjectComponentModifierType.SHAPE) {
                        if (comp.modifier.shape == null || comp.modifier.shape == "")
                            continue;
                        if (comp.modifier.radius == 0)
                            continue;
                        const shape = yield comp.GetShape();
                        if (shape == null)
                            continue;
                        const shape_size = ProjectComponent.GetShapeSize(shape);
                        if (comp.modifier.radius == undefined || comp.modifier.radius == 0) {
                            if (shape_size.x != null && shape_size.y != null)
                                comp.size = new Vector3(shape_size).mult(1000.0);
                        }
                        else {
                            const angle1 = comp.modifier.start_angle / 180.0 * Math.PI;
                            const angle2 = comp.modifier.end_angle / 180.0 * Math.PI;
                            const dangle = (comp.modifier.start_angle + (comp.modifier.end_angle - comp.modifier.start_angle) / 2.0) / 180.0 * Math.PI;
                            const v1 = new Vector2(Math.cos(angle1), Math.sin(angle1)).mult(comp.modifier.radius);
                            const v2 = new Vector2(Math.cos(angle2), Math.sin(angle2)).mult(comp.modifier.radius);
                            const v3 = new Vector2(Math.cos(dangle), Math.sin(dangle)).mult(comp.modifier.radius);
                            const min = new Vector2(Math.min(v1.x, v2.x, v3.x), Math.min(v1.y, v2.y, v3.y));
                            const max = new Vector2(Math.max(v1.x, v2.x, v3.x), Math.max(v1.y, v2.y, v3.y));
                            comp.size.x = max.x - min.x;
                            comp.size.y = max.y - min.y;
                        }
                    }
                    else if (comp.modifier.type == ProjectComponentModifierType.LDSP) {
                        comp.modifier.real_size = new Vector3(comp.size);
                        if (comp.modifier.edges[0].type == LDSPEdgeType.MM2)
                            comp.modifier.real_size.x -= 2;
                        if (comp.modifier.edges[2].type == LDSPEdgeType.MM2)
                            comp.modifier.real_size.x -= 2;
                        if (comp.modifier.edges[1].type == LDSPEdgeType.MM2)
                            comp.modifier.real_size.y -= 2;
                        if (comp.modifier.edges[3].type == LDSPEdgeType.MM2)
                            comp.modifier.real_size.y -= 2;
                        {
                            comp.modifier.edges[0].size = new Vector3();
                            comp.modifier.edges[0].size.x = comp.size.z;
                            comp.modifier.edges[0].size.y = comp.size.y;
                            comp.modifier.edges[0].size.z = 0;
                            if (comp.modifier.edges[0].type == LDSPEdgeType.MM04)
                                comp.modifier.edges[0].size.z = 0.4;
                            else if (comp.modifier.edges[0].type == LDSPEdgeType.MM2)
                                comp.modifier.edges[0].size.z = 2;
                        }
                        {
                            comp.modifier.edges[2].size = new Vector3();
                            comp.modifier.edges[2].size.x = comp.size.z;
                            comp.modifier.edges[2].size.y = comp.size.y;
                            comp.modifier.edges[2].size.z = 0;
                            if (comp.modifier.edges[2].type == LDSPEdgeType.MM04)
                                comp.modifier.edges[2].size.z = 0.4;
                            else if (comp.modifier.edges[2].type == LDSPEdgeType.MM2)
                                comp.modifier.edges[2].size.z = 2;
                        }
                        {
                            comp.modifier.edges[1].size = new Vector3();
                            comp.modifier.edges[1].size.x = comp.size.z;
                            comp.modifier.edges[1].size.y = comp.size.x;
                            comp.modifier.edges[1].size.z = 0;
                            if (comp.modifier.edges[1].type == LDSPEdgeType.MM04)
                                comp.modifier.edges[1].size.z = 0.4;
                            else if (comp.modifier.edges[1].type == LDSPEdgeType.MM2)
                                comp.modifier.edges[1].size.z = 2;
                        }
                        {
                            comp.modifier.edges[3].size = new Vector3();
                            comp.modifier.edges[3].size.x = comp.size.z;
                            comp.modifier.edges[3].size.y = comp.size.x;
                            comp.modifier.edges[3].size.z = 0;
                            if (comp.modifier.edges[3].type == LDSPEdgeType.MM04)
                                comp.modifier.edges[3].size.z = 0.4;
                            else if (comp.modifier.edges[3].type == LDSPEdgeType.MM2)
                                comp.modifier.edges[3].size.z = 2;
                        }
                    }
                }
            });
        }
        PrepareRelatedInputs() {
            if (this.related_inputs == null)
                this.related_inputs = this.project.graph.related_inputs;
            else
                this.project.graph.related_inputs = this.related_inputs;
            for (let key in this.related_inputs) {
                const comp = this.project.components.find((c) => c.guid == key);
                if (comp == null) {
                    delete this.related_inputs[key];
                    continue;
                }
                if (comp.modifier.show_inputs == false)
                    delete this.related_inputs[key];
            }
        }
        Calculate(callback = null) {
            return __awaiter(this, void 0, void 0, function* () {
                const call = this.profiler.GetOrCreateChild("Calculate");
                call.StartSample();
                logger_1.Logger.Log("Calculation started", logger_1.Logger.DebugLevel.INFO);
                logger_1.Logger.Log("Preparing materials", logger_1.Logger.DebugLevel.INFO);
                yield this.PrepareMaterials();
                logger_1.Logger.Log("Processing nodes", logger_1.Logger.DebugLevel.INFO);
                if (this.project.graph.is_active == undefined || this.project.graph.is_active) {
                    for (const n of this.out_nodes)
                        yield PerformanceCall.InvokeAsync("InvokeMethod", () => __awaiter(this, void 0, void 0, function* () { return (yield this.InvokeMethod(n.method)); }), call, n.method);
                }
                logger_1.Logger.Log("Updating inputs", logger_1.Logger.DebugLevel.INFO);
                this.UpdateInputs();
                logger_1.Logger.Log("Updating outputs", logger_1.Logger.DebugLevel.INFO);
                this.UpdateOutputs();
                this.related_calculations = {};
                this.related_names = {};
                logger_1.Logger.Log("Calculating related projects", logger_1.Logger.DebugLevel.INFO);
                for (let comp of this.project.components) {
                    if (comp.modifier.type != ProjectComponentModifierType.BUILTIN)
                        continue;
                    this.related_calculations[comp.guid] = yield this.CalculateRelated(comp, call);
                    this.related_names[comp.guid] = comp.name;
                }
                logger_1.Logger.Log("Updating components size", logger_1.Logger.DebugLevel.INFO);
                yield this.UpdateComponentsSize();
                logger_1.Logger.Log("Assembling project", logger_1.Logger.DebugLevel.INFO);
                this.project.Assemble();
                if (this.has_frontend)
                    yield PerformanceCall.InvokeAsync("PrepareFrontend", () => __awaiter(this, void 0, void 0, function* () { yield this.PrepareFrontend(call); }), call);
                logger_1.Logger.Log("Updating inputs and outputs", logger_1.Logger.DebugLevel.INFO);
                this.inputs = this.project.graph.inputs;
                this.PrepareRelatedInputs();
                logger_1.Logger.Log("Calculation done", logger_1.Logger.DebugLevel.INFO);
                call.EndSample();
                if (callback != null)
                    callback();
            });
        }
        SetInputValue(input_name, value) {
            for (const i of this.project.graph.inputs) {
                if (i.name === input_name)
                    i.value = value;
            }
        }
        Invoke(event_name, value) {
            return __awaiter(this, void 0, void 0, function* () {
                const self = this;
                const event = this.project.graph.nodes.find(function (node) {
                    if (node.method.name !== "InvokeEvent")
                        return false;
                    const name_arg = node.method.arguments.find(function (a) {
                        return a.name === "name";
                    });
                    return name_arg.value == event_name;
                });
                if (event == null)
                    return;
                let script = yield this.GetValue(event.method.arguments.find(function (a) {
                    return a.name === "script";
                }));
                if (isNaN(Number(value)))
                    value = "\"" + value + "\"";
                script = "var value=" + value + ";\n" + IIKCore.UpdateOldScript(script);
                eval(script);
            });
        }
        InvokeScript(script) {
            eval(script);
        }
    }
    IIK.IIKCore = IIKCore;
    IIK.instance = new IIKCore();
})(IIK = exports.IIK || (exports.IIK = {}));


/***/ }),

/***/ "./src/logger.ts":
/*!***********************!*\
  !*** ./src/logger.ts ***!
  \***********************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.Logger = void 0;
var Logger;
(function (Logger) {
    let DebugLevel;
    (function (DebugLevel) {
        DebugLevel[DebugLevel["NONE"] = -1] = "NONE";
        DebugLevel[DebugLevel["ERROR"] = 0] = "ERROR";
        DebugLevel[DebugLevel["WARNING"] = 1] = "WARNING";
        DebugLevel[DebugLevel["INFO"] = 2] = "INFO";
        DebugLevel[DebugLevel["VERBOSE"] = 3] = "VERBOSE";
    })(DebugLevel = Logger.DebugLevel || (Logger.DebugLevel = {}));
    Logger.debug = DebugLevel.ERROR;
    //export let debug = DebugLevel.VERBOSE;
    Logger.externalLog = console.log;
    Logger.Log = function (data, level) {
        if (level > Logger.debug)
            return;
        Logger.externalLog(JSON.stringify(data));
    };
})(Logger = exports.Logger || (exports.Logger = {}));


/***/ }),

/***/ "./src/material.ts":
/*!*************************!*\
  !*** ./src/material.ts ***!
  \*************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.Material = exports.UEMaterialParams = exports.Color = exports.WebMaterialParams = exports.Texture = void 0;
const math_1 = __webpack_require__(/*! ./math */ "./src/math.ts");
const filesystem_1 = __webpack_require__(/*! ./filesystem */ "./src/filesystem.ts");
const changable_1 = __webpack_require__(/*! ./Core/changable */ "./src/Core/changable.ts");
const logger_1 = __webpack_require__(/*! ./logger */ "./src/logger.ts");
var Vector2 = math_1.VMath.Vector2;
var DebugLevel = logger_1.Logger.DebugLevel;
class Texture extends changable_1.Changable {
    constructor() {
        super(...arguments);
        this.fileName = "";
        this.resolution = new math_1.VMath.Vector2(2048, 2048);
        this.realSize = undefined;
    }
}
exports.Texture = Texture;
class WebMaterialParams extends changable_1.Changable {
    constructor() {
        super(...arguments);
        this.lightMapIntensity = 1;
        this.envMapIntensity = 1;
        this.aoMapIntensity = 1;
        this.transparent = false;
        this.alphaTest = 0;
        this.depthTest = true;
        this.depthWrite = true;
        this.side = "FrontSide";
    }
}
exports.WebMaterialParams = WebMaterialParams;
var UEMaterialType;
(function (UEMaterialType) {
    UEMaterialType[UEMaterialType["OPAQUE"] = 1] = "OPAQUE";
    UEMaterialType[UEMaterialType["TRANSPARENT"] = 2] = "TRANSPARENT";
    UEMaterialType[UEMaterialType["MASK"] = 3] = "MASK";
    UEMaterialType[UEMaterialType["MASK_DOUBLESIDED"] = 4] = "MASK_DOUBLESIDED";
})(UEMaterialType || (UEMaterialType = {}));
class Color extends changable_1.Changable {
    constructor() {
        super(...arguments);
        this.r = 1;
        this.g = 1;
        this.b = 1;
        this.a = 1;
    }
}
exports.Color = Color;
class UEMaterialParams extends changable_1.Changable {
    constructor() {
        super(...arguments);
        this.metallicIntensity = 1;
        this.roughnessIntensity = 1;
        this.emissiveIntensity = 1;
        this.normalIntensity = 1;
        this.displacement = new Texture();
        this.displacementIntensity = 1;
        this.hue = new Color();
        this.type = UEMaterialType.OPAQUE;
    }
}
exports.UEMaterialParams = UEMaterialParams;
class Material extends changable_1.Changable {
    constructor() {
        super(...arguments);
        this.guid = "";
        this.name = "";
        this.ao = new Texture();
        this.aoValue = 1;
        this.diffuse = new Texture();
        this.color = "#ffffff";
        this.metallic = new Texture();
        this.metallicValue = 0;
        this.normal = new Texture();
        this.normalValue = 1;
        this.roughness = new Texture();
        this.roughnessValue = 1;
        this.emissive = new Texture();
        this.emissiveColor = "#000000";
        this.alpha = new Texture();
        this.opacity = 1;
        this.realSize = new math_1.VMath.Vector2(1, 1);
        this.isMask = false;
        this.bake = "";
        this.web = new WebMaterialParams();
        this.ue = new UEMaterialParams();
    }
    GetAllTextures() {
        const result = [];
        const keys = Object.keys(this);
        for (const key of keys) {
            if ((typeof this[key]) == "object" && this[key] instanceof Texture)
                result.push(this[key]);
        }
        return result;
    }
    ConvertToNew(data) {
        const keys = Object.keys(this);
        for (const key of keys) {
            if (data.hasOwnProperty(key)) {
                if ((typeof this[key]) == "object" && this[key] instanceof Texture) {
                    if (typeof (data[key]) == "string") {
                        const url = data[key];
                        data[key] = new Texture();
                        data[key].fileName = url;
                        if (data.real_size != null && data.real_size[key] != null)
                            data[key].realSize = data.real_size[key];
                    }
                }
            }
        }
        if (data.real_size != null && data.real_size.default != null)
            data.realSize = data.real_size.default;
        return data;
    }
    static GetByDirectLink(url) {
        return __awaiter(this, void 0, void 0, function* () {
            const guid = url.replace("s123mat://", "");
            const info = yield filesystem_1.Filesystem.GetFile(`s123://calculationResults/${guid}/info.json`);
            if (info == null)
                return new Material();
            const material = yield filesystem_1.Filesystem.GetFile(`s123://calculationResults/${guid}/${info.project}`);
            const result = Material.GetByData(material, guid);
            if (filesystem_1.Filesystem.ApiGet != null) {
                const calc_stat = yield filesystem_1.Filesystem.Get("api/Calculation/GetCalculationStat?guid=" + guid);
                if (calc_stat != null)
                    result.name = calc_stat.name;
            }
            return result;
        });
    }
    static GetByData(data, guid) {
        const result = new Material();
        result.guid = guid;
        const updated_data = result.ConvertToNew(data);
        result.UpdateFrom(updated_data);
        for (let t of result.GetAllTextures()) {
            if (t.realSize == null)
                t.realSize = result.realSize;
        }
        return result;
    }
    GetDirectTextureLink(filename, maxSize, crop) {
        logger_1.Logger.Log("Preparing direct texture link", DebugLevel.VERBOSE);
        logger_1.Logger.Log("GUID: " + this.guid, DebugLevel.VERBOSE);
        logger_1.Logger.Log("filename: " + filename, DebugLevel.VERBOSE);
        if (this.guid == null)
            return filename;
        let result = `calculationResults/${this.guid}/${filename}`;
        if (maxSize != 0) {
            if (!crop)
                result += `&maxSize=${maxSize}`;
            else
                result += `&cropSize=${maxSize}`;
        }
        return result;
    }
    PrepareForContent() {
        const keys = Object.keys(this);
        for (const key of keys) {
            if ((typeof this[key]) == "object" && this[key] instanceof Texture) {
                if (this[key].fileName == "") {
                    delete this[key];
                    continue;
                }
                delete this[key].resolution;
            }
            if (this[key] == null)
                delete this[key];
        }
    }
    ToContent(max_resolution, is_detailed) {
        const result = Material.GetByData(this, this.guid);
        let max_material_resolution = 0;
        for (let t of this.GetAllTextures()) {
            if (t.resolution.x > max_material_resolution)
                max_material_resolution = t.resolution.x;
        }
        let aspect = 1;
        if (max_material_resolution > max_resolution)
            aspect = max_resolution / max_material_resolution;
        for (let t of result.GetAllTextures()) {
            t.resolution = t.resolution.mult(aspect);
            if (t.fileName != null && t.fileName != "")
                t.fileName = result.GetDirectTextureLink(t.fileName, t.resolution.x, is_detailed);
            if (is_detailed)
                t.realSize = new Vector2(t.realSize).mult(aspect);
        }
        if (is_detailed)
            result.realSize = result.realSize.mult(aspect);
        result.PrepareForContent();
        return result;
    }
}
exports.Material = Material;


/***/ }),

/***/ "./src/math.ts":
/*!*********************!*\
  !*** ./src/math.ts ***!
  \*********************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.VMath = void 0;
const changable_1 = __webpack_require__(/*! ./Core/changable */ "./src/Core/changable.ts");
var VMath;
(function (VMath) {
    class Vector2 extends changable_1.Changable {
        constructor(...args) {
            super();
            this.x = 0;
            this.y = 0;
            if (args.length == 1) {
                const vector_obj = args[0];
                if (!("x" in vector_obj) || !("y" in vector_obj))
                    throw new TypeError("Trying to create Vector3 from unsupported object");
                this.UpdateFrom(vector_obj);
                return;
            }
            else if (args.length == 2) {
                if (args[0] == null || args[1] == null)
                    throw new TypeError("Trying to set null value for Vector2 property");
                this.x = args[0];
                this.y = args[1];
                return;
            }
        }
        get length() {
            return Math.sqrt(this.x * this.x + this.y * this.y);
        }
        add(v) {
            return new Vector2(this.x + v.x, this.y + v.y);
        }
        sub(v) {
            return new Vector2(this.x - v.x, this.y - v.y);
        }
        mult(n) {
            return new Vector2(this.x * n, this.y * n);
        }
        div(n) {
            if (n instanceof Vector2)
                return new Vector2(this.x / n.x, this.y / n.y);
            return new Vector2(this.x / n, this.y / n);
        }
        ;
        rotate(angle) {
            const sin = Math.sin(angle / 180 * Math.PI);
            const cos = Math.cos(angle / 180 * Math.PI);
            return new Vector2((cos * this.x) - (sin * this.y), (sin * this.x) + (cos * this.y));
        }
        static scale(v1, v2) {
            return new Vector2(v1.x * v2.x, v1.y * v2.y);
        }
        static distance(v1, v2) {
            const dv = v2.sub(v1);
            return dv.length;
        }
        isFinite() {
            return Number.isFinite(this.x) && Number.isFinite(this.y);
        }
        ;
        isNaN() {
            return isNaN(this.x) || isNaN(this.y);
        }
    }
    VMath.Vector2 = Vector2;
    class Vector3 {
        constructor(...args) {
            if (args.length == 1) {
                const vector_obj = args[0];
                if (vector_obj instanceof Vector2) {
                    this.x = vector_obj.x;
                    this.y = vector_obj.y;
                    this.z = 0;
                    return;
                }
                else {
                    if (!("x" in vector_obj) || !("y" in vector_obj) || !("z" in vector_obj))
                        throw new TypeError("Trying to create Vector3 from unsupported object");
                    this.x = vector_obj.x;
                    this.y = vector_obj.y;
                    this.z = vector_obj.z;
                    return;
                }
            }
            else if (args.length == 3) {
                if (args[0] == null || args[1] == null || args[2] == null)
                    throw new TypeError("Trying to set null value for Vector3 property");
                this.x = args[0];
                this.y = args[1];
                this.z = args[2];
                return;
            }
            this.x = 0;
            this.y = 0;
            this.z = 0;
        }
        get length() {
            return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
        }
        get normalized() {
            return this.div(this.length);
        }
        add(v) {
            return new Vector3(this.x + v.x, this.y + v.y, this.z + v.z);
        }
        ;
        sub(v) {
            return new Vector3(this.x - v.x, this.y - v.y, this.z - v.z);
        }
        ;
        div(n) {
            if (n instanceof Vector3)
                return new Vector3(this.x / n.x, this.y / n.y, this.z / n.z);
            return new Vector3(this.x / n, this.y / n, this.z / n);
        }
        ;
        mult(n) {
            return new Vector3(this.x * n, this.y * n, this.z * n);
        }
        ;
        inversed() {
            return new Vector3(-this.x, -this.y, -this.z);
        }
        ;
        equals(v) {
            return this.x === v.x && this.y === v.y && this.z === v.z;
        }
        ;
        abs() {
            return new Vector3(Math.abs(this.x), Math.abs(this.y), Math.abs(this.z));
        }
        ;
        isZero() {
            return this.x === 0 && this.y === 0 && this.z === 0;
        }
        ;
        isFinite() {
            return Number.isFinite(this.x) && Number.isFinite(this.y) && Number.isFinite(this.z);
        }
        ;
        iNaN() {
            return isNaN(this.x) || isNaN(this.y) || isNaN(this.z);
        }
        invertX() {
            return new Vector3(-this.x, this.y, this.z);
        }
        ;
        invertY() {
            return new Vector3(this.x, -this.y, this.z);
        }
        ;
        invertZ() {
            return new Vector3(this.x, this.y, -this.z);
        }
        ;
        setX(x) {
            return new Vector3(x, this.y, this.z);
        }
        ;
        setY(y) {
            return new Vector3(this.x, y, this.z);
        }
        ;
        setZ(z) {
            return new Vector3(this.x, this.y, z);
        }
        ;
        static get one() {
            return new Vector3(1, 1, 1);
        }
        static cross(lhs, rhs) {
            return new Vector3(lhs.y * rhs.z - lhs.z * rhs.y, lhs.z * rhs.x - lhs.x * rhs.z, lhs.x * rhs.y - lhs.y * rhs.x);
        }
        static dot(lhs, rhs) {
            return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
        }
        static scale(v1, v2) {
            return new Vector3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
        }
        static clamp(value, min, max) {
            if (value < min)
                value = min;
            else if (value > max)
                value = max;
            return value;
        }
        static angle(from, to) {
            const num = Math.sqrt(from.length * from.length * to.length * to.length);
            return num < 1.0000000036274937E-15 ? 0.0 : Math.acos(Vector3.clamp(Vector3.dot(from, to) / num, -1, 1)) * 57.29578;
        }
        xy() {
            return new Vector2(this.x, this.y);
        }
        xz() {
            return new Vector2(this.x, this.z);
        }
        zy() {
            return new Vector2(this.z, this.y);
        }
    }
    VMath.Vector3 = Vector3;
    class Matrix4x4 {
        constructor() {
            this[0] = [1, 0, 0, 0];
            this[1] = [0, 1, 0, 0];
            this[2] = [0, 0, 1, 0];
            this[3] = [0, 0, 0, 1];
        }
        mult(v) {
            const res = new Vector3();
            res.x = this[0][0] * v.x + this[0][1] * v.y + this[0][2] * v.z;
            res.y = this[1][0] * v.x + this[1][1] * v.y + this[1][2] * v.z;
            res.z = this[2][0] * v.x + this[2][1] * v.y + this[2][2] * v.z;
            return res;
        }
    }
    VMath.Matrix4x4 = Matrix4x4;
    class Quaternion {
        constructor(...args) {
            if (args.length == 1) {
                const quat_obj = args[0];
                if (!("x" in quat_obj) || !("y" in quat_obj) || !("z" in quat_obj) || !("w" in quat_obj))
                    throw new TypeError("Trying to create Quaternion from unsupported object");
                this.x = quat_obj.x;
                this.y = quat_obj.y;
                this.z = quat_obj.z;
                this.w = quat_obj.w;
                return;
            }
            else if (args.length == 4) {
                this.x = args[0];
                this.y = args[1];
                this.z = args[2];
                this.w = args[3];
                return;
            }
            this.x = 0;
            this.y = 0;
            this.z = 0;
            this.w = 1;
        }
        mult(q) {
            const x = this.w * q.x + this.x * q.w + this.y * q.z - this.z * q.y;
            const y = this.w * q.y + this.y * q.w + this.z * q.x - this.x * q.z;
            const z = this.w * q.z + this.z * q.w + this.x * q.y - this.y * q.x;
            const w = this.w * q.w - this.x * q.x - this.y * q.y - this.z * q.z;
            return new Quaternion(x, y, z, w);
        }
        equals(v) {
            return this.x === v.x && this.y === v.y && this.z === v.z && this.w == v.w;
        }
        ;
        inversed() {
            const ls = this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w;
            const invNorm = 1.0 / ls;
            const x = -this.x * invNorm;
            const y = -this.y * invNorm;
            const z = -this.z * invNorm;
            const w = this.w * invNorm;
            return new Quaternion(x, y, z, w);
        }
        toRH() {
            return new Quaternion(this.x, -this.y, -this.z, this.w);
        }
        static euler(_x, _y, _z) {
            const yaw = _y / 180 * Math.PI;
            const pitch = _x / 180 * Math.PI;
            const roll = _z / 180 * Math.PI;
            const rollOver2 = roll * 0.5;
            const sinRollOver2 = Math.sin(rollOver2);
            const cosRollOver2 = Math.cos(rollOver2);
            const pitchOver2 = pitch * 0.5;
            const sinPitchOver2 = Math.sin(pitchOver2);
            const cosPitchOver2 = Math.cos(pitchOver2);
            const yawOver2 = yaw * 0.5;
            const sinYawOver2 = Math.sin(yawOver2);
            const cosYawOver2 = Math.cos(yawOver2);
            const w = cosYawOver2 * cosPitchOver2 * cosRollOver2 + sinYawOver2 * sinPitchOver2 * sinRollOver2;
            const x = cosYawOver2 * sinPitchOver2 * cosRollOver2 + sinYawOver2 * cosPitchOver2 * sinRollOver2;
            const y = sinYawOver2 * cosPitchOver2 * cosRollOver2 - cosYawOver2 * sinPitchOver2 * sinRollOver2;
            const z = cosYawOver2 * cosPitchOver2 * sinRollOver2 - sinYawOver2 * sinPitchOver2 * cosRollOver2;
            return new Quaternion(x, y, z, w);
        }
        static lookRotation(_forward, up) {
            const vector = _forward.normalized;
            const vector2 = VMath.Vector3.cross(up, vector).normalized;
            const vector3 = VMath.Vector3.cross(vector, vector2);
            const m00 = vector2.x;
            const m01 = vector2.y;
            const m02 = vector2.z;
            const m10 = vector3.x;
            const m11 = vector3.y;
            const m12 = vector3.z;
            const m20 = vector.x;
            const m21 = vector.y;
            const m22 = vector.z;
            const num8 = (m00 + m11) + m22;
            const quaternion = new VMath.Quaternion(0, 0, 0, 1);
            if (num8 > 0) {
                let num = Math.sqrt(num8 + 1);
                quaternion.w = num * 0.5;
                num = 0.5 / num;
                quaternion.x = (m12 - m21) * num;
                quaternion.y = (m20 - m02) * num;
                quaternion.z = (m01 - m10) * num;
                return quaternion;
            }
            if ((m00 >= m11) && (m00 >= m22)) {
                const num7 = Math.sqrt(((1 + m00) - m11) - m22);
                const num4 = 0.5 / num7;
                quaternion.x = 0.5 * num7;
                quaternion.y = (m01 + m10) * num4;
                quaternion.z = (m02 + m20) * num4;
                quaternion.w = (m12 - m21) * num4;
                return quaternion;
            }
            if (m11 > m22) {
                const num6 = Math.sqrt(((1 + m11) - m00) - m22);
                const num3 = 0.5 / num6;
                quaternion.x = (m10 + m01) * num3;
                quaternion.y = 0.5 * num6;
                quaternion.z = (m21 + m12) * num3;
                quaternion.w = (m20 - m02) * num3;
                return quaternion;
            }
            const num5 = Math.sqrt(((1 + m22) - m00) - m11);
            const num2 = 0.5 / num5;
            quaternion.x = (m20 + m02) * num2;
            quaternion.y = (m21 + m12) * num2;
            quaternion.z = 0.5 * num5;
            quaternion.w = (m01 - m10) * num2;
            return quaternion;
        }
        ;
        rotate(vec) {
            const num = this.x * 2;
            const num2 = this.y * 2;
            const num3 = this.z * 2;
            const num4 = this.x * num;
            const num5 = this.y * num2;
            const num6 = this.z * num3;
            const num7 = this.x * num2;
            const num8 = this.x * num3;
            const num9 = this.y * num3;
            const num10 = this.w * num;
            const num11 = this.w * num2;
            const num12 = this.w * num3;
            const result = {
                "x": (1 - (num5 + num6)) * vec.x + (num7 - num12) * vec.y + (num8 + num11) * vec.z,
                "y": (num7 + num12) * vec.x + (1 - (num4 + num6)) * vec.y + (num9 - num10) * vec.z,
                "z": (num8 - num11) * vec.x + (num9 + num10) * vec.y + (1 - (num4 + num5)) * vec.z
            };
            return new VMath.Vector3(result);
        }
        ;
    }
    VMath.Quaternion = Quaternion;
    class Transform {
        constructor(position, rotation, scale) {
            this.position = position;
            this.rotation = rotation;
            this.scale = scale;
        }
        ;
        static identity() {
            const position = new Vector3(0, 0, 0);
            const rotation = new Quaternion(0, 0, 0, 1);
            const scale = new Vector3(1, 1, 1);
            return new Transform(position, rotation, scale);
        }
    }
    VMath.Transform = Transform;
    class Bounds {
        constructor(min, max) {
            this.min = min;
            this.max = max;
        }
        get center() {
            return this.min.add(this.size.div(2.0));
        }
        get size() {
            return this.max.sub(this.min);
        }
        ;
        static default() {
            return new Bounds(new Vector3(0, 0, 0), new Vector3(0, 0, 0));
        }
        static fromCenterAndSize(center, size) {
            return new Bounds(center.sub(size.div(2.0)), center.add(size.div(2.0)));
        }
        ;
        encapsulate(b) {
            this.min.x = Math.min(this.min.x, b.min.x);
            this.min.y = Math.min(this.min.y, b.min.y);
            this.min.z = Math.min(this.min.z, b.min.z);
            this.max.x = Math.max(this.max.x, b.max.x);
            this.max.y = Math.max(this.max.y, b.max.y);
            this.max.z = Math.max(this.max.z, b.max.z);
        }
        ;
    }
    VMath.Bounds = Bounds;
    class Random {
        constructor(seed) {
            this.seed = seed;
        }
        next() {
            const x = Math.sin(this.seed++) * 10000;
            return x - Math.floor(x);
        }
    }
    VMath.Random = Random;
})(VMath = exports.VMath || (exports.VMath = {}));


/***/ }),

/***/ "./src/performance.ts":
/*!****************************!*\
  !*** ./src/performance.ts ***!
  \****************************/
/***/ (function(__unused_webpack_module, exports) {


var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.PerformanceCore = void 0;
var PerformanceCore;
(function (PerformanceCore) {
    PerformanceCore.allowProfiler = false;
    PerformanceCore.currentTime = null;
    PerformanceCore.CurrentTime = function () {
        if (PerformanceCore.currentTime == null)
            return 0;
        return PerformanceCore.currentTime();
    };
    class PerformanceDelta {
        constructor(time, user_data) {
            this.time = time;
            this.user_data = user_data;
        }
    }
    PerformanceCore.PerformanceDelta = PerformanceDelta;
    class PerformanceCall {
        constructor(name, parent) {
            this.deltas = [];
            this.children = [];
            this.t1 = 0;
            this.name = name;
            if (parent == null)
                return;
            if (parent.GetChild(name) == null)
                parent.children.push(this);
        }
        StartSample() {
            this.t1 = PerformanceCore.CurrentTime();
        }
        EndSample() {
            this.AddDelta(PerformanceCore.CurrentTime() - this.t1);
        }
        AddDelta(dt, user_data = undefined) {
            this.deltas.push(new PerformanceDelta(dt, user_data));
        }
        GetChild(name) {
            return this.children.find((c) => c.name == name);
        }
        GetOrCreateChild(name) {
            let result = this.GetChild(name);
            if (result == null)
                result = new PerformanceCall(name, this);
            return result;
        }
        static Invoke(name, func, parent, user_data = undefined) {
            if (!PerformanceCore.allowProfiler)
                return func();
            let item = parent.GetChild(name);
            if (item == null)
                item = new PerformanceCall(name, parent);
            const t1 = PerformanceCore.CurrentTime();
            const result = func();
            const t2 = PerformanceCore.CurrentTime();
            item.AddDelta(t2 - t1, user_data);
        }
        static InvokeAsync(name, func, parent, user_data = undefined) {
            return __awaiter(this, void 0, void 0, function* () {
                if (!PerformanceCore.allowProfiler)
                    return yield func();
                let item = parent.GetChild(name);
                if (item == null)
                    item = new PerformanceCall(name, parent);
                const t1 = PerformanceCore.CurrentTime();
                const result = yield func();
                const t2 = PerformanceCore.CurrentTime();
                item.AddDelta(t2 - t1, user_data);
            });
        }
    }
    PerformanceCore.PerformanceCall = PerformanceCall;
})(PerformanceCore = exports.PerformanceCore || (exports.PerformanceCore = {}));


/***/ }),

/***/ "./src/project.ts":
/*!************************!*\
  !*** ./src/project.ts ***!
  \************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.Project = void 0;
const math_1 = __webpack_require__(/*! ./math */ "./src/math.ts");
const logger_1 = __webpack_require__(/*! ./logger */ "./src/logger.ts");
const generator_1 = __webpack_require__(/*! ./MeshUtils/generator */ "./src/MeshUtils/generator.ts");
const graph_1 = __webpack_require__(/*! ./graph */ "./src/graph.ts");
const utils_1 = __webpack_require__(/*! ./utils */ "./src/utils.ts");
const filesystem_1 = __webpack_require__(/*! ./filesystem */ "./src/filesystem.ts");
const unwrapper_1 = __webpack_require__(/*! ./MeshUtils/unwrapper */ "./src/MeshUtils/unwrapper.ts");
const material_1 = __webpack_require__(/*! ./material */ "./src/material.ts");
const changable_1 = __webpack_require__(/*! ./Core/changable */ "./src/Core/changable.ts");
var Project;
(function (Project_1) {
    var PrimitiveMesh = generator_1.Generator.PrimitiveMesh;
    var Vector3 = math_1.VMath.Vector3;
    var Log = logger_1.Logger.Log;
    var DebugLevel = logger_1.Logger.DebugLevel;
    var Graph = graph_1.Graph.Graph;
    var Quaternion = math_1.VMath.Quaternion;
    var Vector2 = math_1.VMath.Vector2;
    var ShapedMesh = generator_1.Generator.ShapedMesh;
    var TriplanarUnwrapper = unwrapper_1.Unwrapper.TriplanarUnwrapper;
    var CurvedMesh = generator_1.Generator.CurvedMesh;
    let LDSPEdgeType;
    (function (LDSPEdgeType) {
        LDSPEdgeType[LDSPEdgeType["NONE"] = 0] = "NONE";
        LDSPEdgeType[LDSPEdgeType["MM04"] = 1] = "MM04";
        LDSPEdgeType[LDSPEdgeType["MM2"] = 2] = "MM2";
    })(LDSPEdgeType = Project_1.LDSPEdgeType || (Project_1.LDSPEdgeType = {}));
    let Face;
    (function (Face) {
        Face[Face["NONE"] = -1] = "NONE";
        Face[Face["FRONT"] = 0] = "FRONT";
        Face[Face["TOP"] = 1] = "TOP";
        Face[Face["BACK"] = 2] = "BACK";
        Face[Face["BOTTOM"] = 3] = "BOTTOM";
        Face[Face["LEFT"] = 4] = "LEFT";
        Face[Face["RIGHT"] = 5] = "RIGHT";
    })(Face || (Face = {}));
    let Anchor;
    (function (Anchor) {
        Anchor[Anchor["MIN"] = 1] = "MIN";
        Anchor[Anchor["CENTER"] = 2] = "CENTER";
        Anchor[Anchor["MAX"] = 3] = "MAX";
    })(Anchor || (Anchor = {}));
    let ProjectComponentModifierType;
    (function (ProjectComponentModifierType) {
        ProjectComponentModifierType[ProjectComponentModifierType["DUMMY"] = 0] = "DUMMY";
        ProjectComponentModifierType[ProjectComponentModifierType["SHAPE"] = 1] = "SHAPE";
        ProjectComponentModifierType[ProjectComponentModifierType["PERFORATION"] = 2] = "PERFORATION";
        ProjectComponentModifierType[ProjectComponentModifierType["MESH"] = 3] = "MESH";
        ProjectComponentModifierType[ProjectComponentModifierType["LIGHT_SOURCE"] = 4] = "LIGHT_SOURCE";
        ProjectComponentModifierType[ProjectComponentModifierType["BUILTIN"] = 5] = "BUILTIN";
        ProjectComponentModifierType[ProjectComponentModifierType["WALL"] = 6] = "WALL";
        ProjectComponentModifierType[ProjectComponentModifierType["FLOOR"] = 7] = "FLOOR";
        ProjectComponentModifierType[ProjectComponentModifierType["CEILING"] = 8] = "CEILING";
        ProjectComponentModifierType[ProjectComponentModifierType["LDSP"] = 9] = "LDSP";
        ProjectComponentModifierType[ProjectComponentModifierType["MDF_WITH_PAINT"] = 10] = "MDF_WITH_PAINT";
        ProjectComponentModifierType[ProjectComponentModifierType["MDF_WITH_FITTING"] = 11] = "MDF_WITH_FITTING";
        ProjectComponentModifierType[ProjectComponentModifierType["SOLID_WOOD"] = 12] = "SOLID_WOOD";
        ProjectComponentModifierType[ProjectComponentModifierType["GLASS"] = 13] = "GLASS";
        ProjectComponentModifierType[ProjectComponentModifierType["ARRAY"] = 14] = "ARRAY";
    })(ProjectComponentModifierType = Project_1.ProjectComponentModifierType || (Project_1.ProjectComponentModifierType = {}));
    let VirtualObjectModifierType;
    (function (VirtualObjectModifierType) {
        VirtualObjectModifierType[VirtualObjectModifierType["SHADOW_PLANE"] = 1] = "SHADOW_PLANE";
        VirtualObjectModifierType[VirtualObjectModifierType["LIGHT"] = 2] = "LIGHT";
        VirtualObjectModifierType[VirtualObjectModifierType["CAMERA"] = 3] = "CAMERA";
    })(VirtualObjectModifierType = Project_1.VirtualObjectModifierType || (Project_1.VirtualObjectModifierType = {}));
    class FacePoint {
        constructor(position, comp_position, comp_rotation) {
            if (comp_rotation == null)
                comp_rotation = new Quaternion();
            else if (!(comp_rotation instanceof Quaternion))
                comp_rotation = new Quaternion(comp_rotation);
            const ax = Math.sign(position.x) * 0.5;
            const ay = Math.sign(position.y) * 0.5;
            const az = -Math.sign(position.z) * 0.5;
            this.anchor = new math_1.VMath.Vector3(ax, ay, az);
            this.faces = FacePoint.GetFacesFromAnchor(this.anchor);
            this.orths =
                [
                    new math_1.VMath.Vector3(this.anchor.x > 0 ? -1 : 1, 0, 0),
                    new math_1.VMath.Vector3(0, this.anchor.y > 0 ? -1 : 1, 0),
                    new math_1.VMath.Vector3(0, 0, this.anchor.z < 0 ? -1 : 1)
                ];
            this.position = comp_position.add(comp_rotation.rotate(position));
            this.orths[0] = comp_rotation.rotate(this.orths[0]);
            this.orths[1] = comp_rotation.rotate(this.orths[1]);
            this.orths[2] = comp_rotation.rotate(this.orths[2]);
        }
        static GetFacesFromAnchor(anchor) {
            const result = [];
            if (anchor.x < 0)
                result.push(Face.LEFT);
            else if (anchor.x > 0)
                result.push(Face.RIGHT);
            if (anchor.y < 0)
                result.push(Face.BOTTOM);
            else if (anchor.y > 0)
                result.push(Face.TOP);
            if (anchor.z < 0)
                result.push(Face.BACK);
            else if (anchor.z > 0)
                result.push(Face.FRONT);
            return result;
        }
    }
    class ConnectableProjectItem extends changable_1.Changable {
        constructor(obj) {
            super();
            this.children = undefined;
            this.position = new Vector3();
            this.positioning_points = [];
            this.path = "";
            this.name = "";
            if (obj != null) {
                if ("path" in obj)
                    this.path = obj.path;
                if ("name" in obj)
                    this.name = obj.name;
                if ("positioning_points" in obj) {
                    for (let pp of obj.positioning_points)
                        this.positioning_points.push(new PositioningPoint(pp, this));
                }
            }
        }
        GetFacePoints() {
            return [];
        }
        TranslateWithChildren(dv, processed) {
            if (processed != null && processed.includes(this))
                return;
            if (processed == null)
                processed = [];
            this.position = this.position.add(dv);
            processed.push(this);
            if (this.children != undefined) {
                for (const child of this.children)
                    child.TranslateWithChildren(dv, processed);
            }
        }
        GetPointByPath(path) {
            const self = this;
            if (this.positioning_points == undefined)
                return undefined;
            return this.positioning_points.find(function (pp) {
                return self.path + "/" + self.name + "/" + pp.path + "/" + pp.name === path;
            });
        }
    }
    class VirtualObject extends ConnectableProjectItem {
        constructor(...args) {
            if (args.length == 0) {
                super(null);
                this.guid = utils_1.Utils.CreateUUID();
                this.modifier = {};
                //pass
            }
            else {
                const vo = args[0];
                super(vo);
                for (let key in vo) {
                    if (!vo.hasOwnProperty(key))
                        continue;
                    if (vo[key] === Object(vo[key]))
                        continue;
                    this[key] = vo[key];
                }
                Log("Constructing virtual object " + vo.name, DebugLevel.VERBOSE);
                this.position = new math_1.VMath.Vector3(vo.position);
                this.modifier = vo.modifier;
                if ("rotation" in this.modifier)
                    this.modifier.rotation = new math_1.VMath.Quaternion(this.modifier.rotation);
                if ("size" in this.modifier && "z" in this.modifier.size)
                    this.modifier.size = new math_1.VMath.Vector3(vo.modifier.size);
                Log("Done constructing virtual object " + vo.name, DebugLevel.VERBOSE);
            }
        }
        GetFacePoints() {
            if (this.modifier.type == VirtualObjectModifierType.CAMERA) {
                const position = new Vector3();
                const self_position = this.position;
                const self_rotation = this.modifier.rotation;
                return [new FacePoint(position, self_position, self_rotation)];
            }
            if (this.modifier.type == VirtualObjectModifierType.LIGHT) {
                const position = new Vector3();
                const self_position = this.position;
                const self_rotation = "rotation" in this.modifier.settings ? this.modifier.settings.rotation : new Quaternion();
                return [new FacePoint(position, self_position, self_rotation)];
            }
            //if SHADOW_PLANE
            const half_size = new Vector2(this.modifier.size).div(2.0);
            const raw_verts = [
                new Vector3(-half_size.x, 0, half_size.y),
                new Vector3(-half_size.x, 0, -half_size.y),
                new Vector3(half_size.x, 0, -half_size.y),
                new Vector3(half_size.x, 0, half_size.y),
            ];
            return [
                new FacePoint(raw_verts[0], this.position, this.modifier.rotation),
                new FacePoint(raw_verts[1], this.position, this.modifier.rotation),
                new FacePoint(raw_verts[2], this.position, this.modifier.rotation),
                new FacePoint(raw_verts[3], this.position, this.modifier.rotation),
                new FacePoint(raw_verts[1].add(raw_verts[0]).div(2.0), this.position, this.modifier.rotation),
                new FacePoint(raw_verts[2].add(raw_verts[1]).div(2.0), this.position, this.modifier.rotation),
                new FacePoint(raw_verts[3].add(raw_verts[2]).div(2.0), this.position, this.modifier.rotation),
                new FacePoint(raw_verts[0].add(raw_verts[3]).div(2.0), this.position, this.modifier.rotation),
                new FacePoint(new Vector3(), this.position, this.modifier.rotation)
            ];
        }
        UpdateFromFrontend(vo) {
            switch (vo.type) {
                case "shadow_plane":
                    this.modifier.type = 1;
                    this.position = new Vector3(vo.transform.position).mult(1000.0).invertX();
                    this.modifier.rotation = new Quaternion(vo.transform.rotation).toRH();
                    this.modifier.size = new Vector2(vo.transform.scale.x, vo.transform.scale.z).mult(1000.0);
                    this.modifier.shadow_type = vo.data.shadow_type;
                    if (vo.data.shadow_type == 0)
                        this.modifier.settings = { "path": vo.data.path };
                    break;
                case "camera":
                    this.modifier.type = 3;
                    this.position = new Vector3(vo.transform.position).mult(1000.0).invertX();
                    this.modifier.rotation = new Quaternion(vo.transform.rotation).toRH();
                    this.modifier.camera_type = vo.data.camera_type;
                    this.modifier.order = vo.data.order;
                    this.modifier.is_pov = vo.data.is_pov == 1;
                    if (vo.data.camera_type == 2)
                        this.modifier.settings = { "distance": vo.data.radius * 1000.0 };
                    break;
                case "light":
                    this.modifier.type = 2;
                    this.position = new Vector3(vo.transform.position).mult(1000.0).invertX();
                    this.modifier.settings = { "rotation": new Quaternion(vo.transform.rotation).toRH() };
                    this.modifier.light_type = vo.data.light_type;
                    this.modifier.settings.color = vo.data.color;
                    this.modifier.settings.intensity = vo.data.intensity;
                    this.modifier.settings.temperature = vo.data.temperature;
                    this.modifier.settings.range = vo.data.range;
                    this.modifier.settings.angle = vo.data.angle;
                    this.modifier.isCastingShadows = vo.data.isCastingShadows;
                    this.modifier.settings.spotInnerAngle = vo.data.spotInnerAngle;
                    this.modifier.settings.spotOuterAngle = vo.data.spotOuterAngle;
                    break;
            }
        }
    }
    Project_1.VirtualObject = VirtualObject;
    class ConnectionPoint {
        constructor(cp, project) {
            for (let key in cp) {
                if (!cp.hasOwnProperty(key))
                    continue;
                if (cp[key] === Object(cp[key]))
                    continue;
                this[key] = cp[key];
            }
            Log("Constructing connection point " + cp.name, DebugLevel.VERBOSE);
            this.project = project;
            Log("Done constructing connection point " + cp.name, DebugLevel.VERBOSE);
        }
        get p1() {
            return this.project.GetPositioningPointByPath(this["point1"]);
        }
        get p2() {
            return this.project.GetPositioningPointByPath(this["point2"]);
        }
        Connect(is_reversed) {
            Log("Connecting point " + this.name + (is_reversed ? ":reversed" : ""), DebugLevel.VERBOSE);
            if (this.p1 == null || this.p2 == null)
                return;
            const p1 = this.p1.GetPosition();
            const p2 = this.p2.GetPosition();
            Log("Point1 position: " + JSON.stringify(p1), DebugLevel.VERBOSE);
            Log("Point2 position: " + JSON.stringify(p2), DebugLevel.VERBOSE);
            if (p1 == null || p2 == null)
                return;
            const dv = p1.sub(p2);
            Log("dv: " + JSON.stringify(dv), DebugLevel.VERBOSE);
            if (is_reversed)
                this.p1.connectable.TranslateWithChildren(dv.inversed(), null);
            else
                this.p2.connectable.TranslateWithChildren(dv, null);
        }
    }
    Project_1.ConnectionPoint = ConnectionPoint;
    class PositioningPoint {
        constructor(pp, comp) {
            for (let key in pp) {
                if (!pp.hasOwnProperty(key))
                    continue;
                if (pp[key] === Object(pp[key]))
                    continue;
                this[key] = pp[key];
            }
            Log("Constructing positioning point " + pp.name, DebugLevel.VERBOSE);
            this.connectable = comp;
            this.offset = new math_1.VMath.Vector3(pp.offset);
            Log("Anchor: " + JSON.stringify(this.anchor), DebugLevel.VERBOSE);
            Log("Offset: " + JSON.stringify(this.offset), DebugLevel.VERBOSE);
            Log("Done constructing positioning point " + pp.name, DebugLevel.VERBOSE);
        }
        get anchor() {
            return Project.GetAnchorVector(this);
        }
        set anchor(v) {
            this.anchor_x = Project.GetAnchor(v.x);
            this.anchor_y = Project.GetAnchor(v.y);
            this.anchor_z = Project.GetAnchor(v.z);
        }
        GetPosition() {
            if (this.connectable == null)
                return new Vector3();
            const points = this.connectable.GetFacePoints();
            const self = this;
            const target_point = points.find(p => p.anchor.equals(self.anchor));
            if (target_point == null)
                return null;
            const ox = target_point.orths[0].mult(this.offset.x);
            const oy = target_point.orths[1].mult(this.offset.y);
            const oz = target_point.orths[2].mult(this.offset.z);
            return target_point.position.add(ox).add(oy).add(oz);
        }
    }
    Project_1.PositioningPoint = PositioningPoint;
    class ProjectComponent extends ConnectableProjectItem {
        constructor(...args) {
            if (args.length == 1) {
                super(null);
                this.guid = utils_1.Utils.CreateUUID();
                this.position = new Vector3();
                this.rotation = new Quaternion();
                this.size = new Vector3(100, 100, 100);
                this.project = args[0];
                this.processings = [];
                this.material_content = null;
            }
            else {
                const comp = args[0];
                super(comp);
                this.project = args[1];
                for (let key in comp) {
                    if (!comp.hasOwnProperty(key))
                        continue;
                    if (comp[key] === Object(comp[key]))
                        continue;
                    this[key] = comp[key];
                }
                Log("Constructing component " + comp.name, DebugLevel.VERBOSE);
                this.position = new math_1.VMath.Vector3(comp.position);
                this.rotation = new math_1.VMath.Quaternion(comp.rotation);
                this.size = new math_1.VMath.Vector3(comp.size);
                this.processings = comp.processings; //TODO Processing to class
                this.modifier = comp.modifier; //TODO To class
                if (this.modifier.mesh_offset != null)
                    this.modifier.mesh_offset = new Vector3(this.modifier.mesh_offset);
                if (this.modifier.mesh_size != null)
                    this.modifier.mesh_size = new Vector3(this.modifier.mesh_size);
                if (this.modifier.child != null)
                    this.modifier.child = new ProjectComponent(this.modifier.child, this.project);
                this.material_content = comp.material_content;
                Log("position " + JSON.stringify(this.position), DebugLevel.VERBOSE);
                Log("rotation " + JSON.stringify(this.rotation), DebugLevel.VERBOSE);
                Log("size " + JSON.stringify(this.size), DebugLevel.VERBOSE);
                Log("bounds " + JSON.stringify(this.bounds), DebugLevel.VERBOSE);
                Log("Done constructing component " + comp.name, DebugLevel.VERBOSE);
            }
        }
        GetShapeFromData(shape, width, is_mirrored) {
            function GetSize(data) {
                const min = new Vector2(1, 1).mult(Number.MAX_VALUE);
                const max = new Vector2(1, 1).mult(Number.MIN_VALUE);
                for (let s of data) {
                    if (s.x < min.x)
                        min.x = s.x;
                    if (s.x > max.x)
                        max.x = s.x;
                    if (s.y < min.y)
                        min.y = s.y;
                    if (s.y > max.y)
                        max.y = s.y;
                }
                return max.sub(min);
            }
            function CenterShape(data) {
                const result = [];
                let sum = new Vector2();
                for (const s of data)
                    sum = sum.add(s);
                sum = sum.div(data.length);
                for (const s of data)
                    result.push(s.sub(sum));
                return result;
            }
            function MirrorShape(data) {
                for (let v of data)
                    v.x = -v.x;
                return data.reverse();
            }
            let result = [];
            if (shape.data == null) {
                for (let i = 0; i < shape.length; i++)
                    result[i] = new Vector2(shape[i]);
                result = CenterShape(result);
                if (is_mirrored)
                    result = MirrorShape(result);
                return result;
            }
            for (let i = 0; i < shape.data.length; i++)
                result[i] = new Vector2(shape.data[i]);
            result = CenterShape(result);
            if (is_mirrored)
                result = MirrorShape(result);
            if (shape.offset_left == 0 && shape.offset_right == 0)
                return result;
            const size = GetSize(result);
            const ol = shape.offset_left / 1000.0;
            const or = shape.offset_right / 1000.0;
            const dx = (width - size.x) / 2.0;
            if (ol != 0 && or != 0) {
                for (let i = 0; i < result.length; i++) {
                    const v = result[i];
                    if (v.x < ol - size.x / 2.0)
                        v.x -= dx;
                    if (v.x > -or + size.x / 2.0)
                        v.x += dx;
                    result[i] = v;
                }
            }
            else if (ol != 0) {
                for (let i = 0; i < result.length; i++) {
                    const v = result[i];
                    if (v.x < ol - size.x / 2.0)
                        v.x -= dx;
                    if (v.x > ol - size.x / 2.0)
                        v.x += dx;
                    result[i] = v;
                }
            }
            else if (or != 0) {
                for (let i = 0; i < result.length; i++) {
                    const v = result[i];
                    if (v.x < -or + size.x / 2.0)
                        v.x -= dx;
                    if (v.x > -or + size.x / 2.0)
                        v.x += dx;
                    result[i] = v;
                }
            }
            return result;
        }
        static GetShapeSize(data) {
            if (data == null)
                return new Vector2();
            const min = new Vector2(1, 1).mult(Number.MAX_VALUE);
            const max = new Vector2(1, 1).mult(Number.MIN_VALUE);
            for (let s of data) {
                if (s.x < min.x)
                    min.x = s.x;
                if (s.x > max.x)
                    max.x = s.x;
                if (s.y < min.y)
                    min.y = s.y;
                if (s.y > max.y)
                    max.y = s.y;
            }
            return max.sub(min);
        }
        GetShape() {
            return __awaiter(this, void 0, void 0, function* () {
                let fname = filesystem_1.Filesystem.GetRelativePath(this.modifier.shape);
                fname = fname.split('.').slice(0, -1).join('.') + ".s123drawing";
                if (this.project.guid)
                    fname = `s123://calculationResults/${this.project.guid}/${fname}`;
                logger_1.Logger.Log("Target shape file: " + fname, logger_1.Logger.DebugLevel.INFO);
                let shape = yield filesystem_1.Filesystem.GetFile(fname);
                if (shape != null) {
                    if (this.modifier.radius != null && this.modifier.radius != 0)
                        shape = this.GetShapeFromData(shape, this.modifier.width / 1000.0, this.modifier.is_mirrored);
                    else
                        shape = this.GetShapeFromData(shape, this.size.x / 1000.0, this.modifier.is_mirrored);
                }
                return shape;
            });
        }
        GetGeneratedMesh() {
            return __awaiter(this, void 0, void 0, function* () {
                logger_1.Logger.Log("Generating mesh for: " + this.name, logger_1.Logger.DebugLevel.INFO);
                const cut_angle1 = "cut_angle1" in this.modifier ? this.modifier.cut_angle1 : 0;
                const cut_angle2 = "cut_angle2" in this.modifier ? this.modifier.cut_angle2 : 0;
                const offset = this.modifier.mesh_offset != null ? this.modifier.mesh_offset : new Vector3();
                if (this.modifier.type == ProjectComponentModifierType.SHAPE) {
                    logger_1.Logger.Log("Original shape file: " + this.modifier.shape, logger_1.Logger.DebugLevel.INFO);
                    logger_1.Logger.Log("GetFileContents assigned: " + (filesystem_1.Filesystem.GetFileContents != null), logger_1.Logger.DebugLevel.INFO);
                    logger_1.Logger.Log("GetFileContentsAsync assigned: " + (filesystem_1.Filesystem.GetFileContentsAsync != null), logger_1.Logger.DebugLevel.INFO);
                }
                let mesh;
                if (this.modifier.type == ProjectComponentModifierType.SHAPE &&
                    this.modifier.shape != null && this.modifier.shape != "") {
                    const shape = yield this.GetShape();
                    if (shape == null)
                        return new PrimitiveMesh(this.size.div(1000.0), cut_angle1, cut_angle2, offset);
                    else {
                        if (this.modifier.radius != null && this.modifier.radius != 0) {
                            mesh = CurvedMesh.Construct(shape, this.modifier.radius / 1000.0, this.modifier.start_angle, this.modifier.end_angle, this.modifier.precision, true);
                        }
                        else
                            mesh = ShapedMesh.Construct(this.size.div(1000.0), shape, cut_angle1, cut_angle2, offset);
                    }
                }
                else {
                    mesh = new PrimitiveMesh(this.size.div(1000.0), cut_angle1, cut_angle2, offset);
                }
                if (this.bake != null && this.bake != "") {
                    let fname = "model.s123mdata";
                    if (this.project.guid)
                        fname = `s123://calculationResults/${this.project.guid}/${fname}`;
                    logger_1.Logger.Log("Applying additional model data from file: " + fname, logger_1.Logger.DebugLevel.INFO);
                    const data = yield filesystem_1.Filesystem.GetFile(fname);
                    if (data != null)
                        mesh.ApplyAdditionalData(data.meshes[this.guid]);
                }
                if (this.processings != null && this.processings.length > 0) {
                    const proc = this.processings.find(function (p) { return p.type == 2; });
                    const fitting = this.processings.find(function (p) { return p.type == 3; });
                    const unwrapper = new TriplanarUnwrapper(mesh, fitting, proc, this.position.div(1000.0), this.rotation, new Vector3(1, 1, 1));
                    mesh.uvs = unwrapper.Process();
                }
                return mesh;
            });
        }
        static GetMaterialData(material, project_guid, is_detailed, max_texture_size) {
            return __awaiter(this, void 0, void 0, function* () {
                logger_1.Logger.Log("Generating content for material: " + material, DebugLevel.VERBOSE);
                if (material == null || material == "")
                    return new material_1.Material();
                if (!material.startsWith("s123mat://") && !material.endsWith(".s123mat")) {
                    if (material == "0") //TODO
                        return new material_1.Material();
                    const diffuse = new material_1.Texture();
                    if (project_guid != null)
                        diffuse.fileName = `calculationResults/${project_guid}/${filesystem_1.Filesystem.GetRelativePath(material)}`;
                    else
                        diffuse.fileName = filesystem_1.Filesystem.GetRelativePath(material);
                    diffuse.realSize = new Vector2(1, 1);
                    diffuse.resolution = new Vector2(2048, 2048);
                    return { "diffuse": diffuse };
                }
                let material_content = null;
                if (material.startsWith("s123mat")) {
                    logger_1.Logger.Log("as system material", DebugLevel.VERBOSE);
                    material_content = (yield material_1.Material.GetByDirectLink(material)).ToContent(max_texture_size, is_detailed);
                }
                else {
                    logger_1.Logger.Log("as internal material", DebugLevel.VERBOSE);
                    let fname = filesystem_1.Filesystem.GetRelativePath(material);
                    if (project_guid != null && project_guid != "")
                        fname = `s123://calculationResults/${project_guid}/${fname}`;
                    const material_data = yield filesystem_1.Filesystem.GetFile(fname);
                    if (material_data != null)
                        material_content = material_1.Material.GetByData(material_data, project_guid).ToContent(max_texture_size, is_detailed);
                    else
                        logger_1.Logger.Log("Material data is empty", DebugLevel.VERBOSE);
                }
                logger_1.Logger.Log("Done content generation", DebugLevel.VERBOSE);
                return material_content;
            });
        }
        GetMaterialData(is_detailed, max_texture_size) {
            return __awaiter(this, void 0, void 0, function* () {
                const material_content = yield ProjectComponent.GetMaterialData(this.material, this.project.guid, is_detailed, max_texture_size);
                if (material_content != null && this.bake != null && this.bake != "") {
                    material_content.bake = `calculationResults/${this.project.guid}/${filesystem_1.Filesystem.GetRelativePath(this.bake)}&maxSize=${max_texture_size}`;
                    logger_1.Logger.Log("Applyed bake:" + material_content.bake, DebugLevel.VERBOSE);
                }
                return material_content;
            });
        }
        get bounds() {
            let position = this.position.div(1000.0);
            const rotation = this.rotation;
            if (this.modifier.type === 3 && this.modifier.apply_offset) {
                const s = this.size.div(1000.0);
                const n = new math_1.VMath.Vector3(this.modifier.mesh_size);
                const scale = s.div(n);
                const offset = math_1.VMath.Vector3.scale(this.modifier.mesh_offset, scale);
                position = position.add(rotation.rotate(offset));
            }
            const size = rotation.rotate(this.size).abs().div(1000.0);
            if (!position.isFinite() || !size.isFinite())
                return null;
            return math_1.VMath.Bounds.fromCenterAndSize(position, size);
        }
        UpdateFromFrontend(frontend_data, comp_inputs, initial_offset) {
            return __awaiter(this, void 0, void 0, function* () {
                if (frontend_data.build_order != null)
                    this.build_order = frontend_data.build_order;
                if (frontend_data.type != "calculation") {
                    logger_1.Logger.Log("Unsupported child type", DebugLevel.VERBOSE);
                    return;
                }
                logger_1.Logger.Log("Updating related project from frontend", logger_1.Logger.DebugLevel.INFO);
                const transform = frontend_data.transform;
                transform.position = new Vector3(transform.position);
                transform.rotation = new Quaternion(transform.rotation);
                const project_guid = frontend_data.file.replace("s123calc://", "");
                logger_1.Logger.Log("Waiting for info file for " + project_guid, logger_1.Logger.DebugLevel.INFO);
                const info = yield filesystem_1.Filesystem.GetFile(`s123://calculationResults/${project_guid}/info.json`);
                const project_fname = info.project;
                logger_1.Logger.Log("Waiting for project file", logger_1.Logger.DebugLevel.INFO);
                const { IIK } = yield Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./iik */ "./src/iik.ts")));
                const iik = new IIK.IIKCore();
                iik.project = yield filesystem_1.Filesystem.GetFile(`s123://calculationResults/${project_guid}/${project_fname}`);
                iik.has_frontend = false;
                if (comp_inputs != null)
                    iik.inputs = comp_inputs;
                iik.Prepare();
                yield iik.Calculate();
                const related_project = iik.project;
                this.position = transform.position.invertX().sub(related_project.GetOffset()).add(initial_offset).mult(1000.0);
                this.rotation = transform.rotation.toRH().mult(related_project.GetRotation().inversed());
                this.size = related_project.bounds.size.mult(1000.0);
                this.modifier =
                    {
                        "type": ProjectComponentModifierType.BUILTIN,
                        "related_project": frontend_data.file
                    };
            });
        }
        GetVertices() {
            const cut_angle1 = "cut_angle1" in this.modifier ? this.modifier.cut_angle1 : 0;
            const cut_angle2 = "cut_angle2" in this.modifier ? this.modifier.cut_angle2 : 0;
            let size = new Vector3(this.size);
            //if( this.modifier.type == ProjectComponentModifierType.LDSP )
            //{
            //    const size_offset = new Vector3();
            //    size_offset.x = ( this.modifier.edges[0].type == LDSPEdgeType.MM2 ? 2 : 0.03 ) +
            //                    ( this.modifier.edges[2].type == LDSPEdgeType.MM2 ? 2 : 0.03 );
            //    size_offset.y = ( this.modifier.edges[1].type == LDSPEdgeType.MM2 ? 2 : 0.03 ) +
            //                    ( this.modifier.edges[3].type == LDSPEdgeType.MM2 ? 2 : 0.03 );
            //    const position_offset = new Vector3();
            //    position_offset.x -= this.modifier.edges[0].type == LDSPEdgeType.MM2 ? 2 : 0.03;
            //    position_offset.x += this.modifier.edges[2].type == LDSPEdgeType.MM2 ? 2 : 0.03;
            //    position_offset.y -= this.modifier.edges[1].type == LDSPEdgeType.MM2 ? 2 : 0.03;
            //    position_offset.y += this.modifier.edges[3].type == LDSPEdgeType.MM2 ? 2 : 0.03;
            //    
            //    size = size.sub( size_offset );
            //    let result : Vector3[] = Project.GetRawVerticesFromSizeAndAngles( size, cut_angle1, cut_angle2 );
            //    for( let i = 0; i < result.length; i++ )
            //        result[i] = result[i].add( position_offset.div(2.0) );
            //    return result;
            //}
            if (size.x < 0.1)
                size.x = 1;
            if (size.y < 0.1)
                size.y = 1;
            if (size.z < 0.1)
                size.z = 1;
            return Project.GetRawVerticesFromSizeAndAngles(size, cut_angle1, cut_angle2);
        }
        GetFacePoints() {
            let pos = new math_1.VMath.Vector3(this.position);
            const rot = this.rotation;
            if (this.modifier.type === 3 && this.modifier.apply_offset) {
                const size = this.size.div(1000.0);
                const nsize = new math_1.VMath.Vector3(this.modifier.mesh_size);
                const scale = size.div(nsize);
                const offset = math_1.VMath.Vector3.scale(this.modifier.mesh_offset, scale).mult(1000.0);
                pos = pos.add(rot.rotate(offset));
            }
            let size = new Vector3(this.size);
            if (size.x < 0.1)
                size.x = 1;
            if (size.y < 0.1)
                size.y = 1;
            if (size.z < 0.1)
                size.z = 1;
            return Project.GetFacePointsFromRawVertices(pos, this.rotation, size, this.GetVertices());
        }
    }
    Project_1.ProjectComponent = ProjectComponent;
    class Project extends ConnectableProjectItem {
        constructor(...args) {
            super(null);
            this.components = [];
            this.connection_points = [];
            this.virtual_objects = [];
            if (args.length == 0) {
                this.offset = new Vector3();
                this.anchor_x = Anchor.CENTER;
                this.anchor_y = Anchor.MIN;
                this.anchor_z = Anchor.CENTER;
                this.normal = new Vector3();
                this.graph = new Graph();
            }
            else if (args.length == 1) {
                const project_data = args[0];
                this.UpdateFromData(project_data);
            }
            delete this.children;
            delete this.positioning_points;
        }
        get anchor() {
            return Project.GetAnchorVector(this);
        }
        set anchor(v) {
            this.anchor_x = Project.GetAnchor(v.x);
            this.anchor_y = Project.GetAnchor(v.y);
            this.anchor_z = Project.GetAnchor(v.z);
        }
        get bounds() {
            if (this.components.length === 0)
                return math_1.VMath.Bounds.default();
            const start_comp = this.components.find(function (c) {
                return c.is_active && c.bounds != null;
            });
            if (start_comp == null || start_comp.bounds == null)
                return math_1.VMath.Bounds.default();
            const result = start_comp.bounds;
            for (const comp of this.components) {
                if (!comp.is_active)
                    continue;
                const b = comp.bounds;
                if (b == null)
                    continue;
                result.encapsulate(b);
            }
            return result;
        }
        UpdateFromData(project_data) {
            Log("Project setup", DebugLevel.INFO);
            Log("Components count: " + project_data.components.length, DebugLevel.INFO);
            Log("Connection points count: " + project_data.connection_points.length, DebugLevel.INFO);
            if (project_data.virtual_objects !== undefined)
                Log("Virtual objects count: " + project_data.virtual_objects.length, DebugLevel.INFO);
            for (let key in project_data) {
                if (!project_data.hasOwnProperty(key))
                    continue;
                if (project_data[key] === Object(project_data[key]))
                    continue;
                this[key] = project_data[key];
            }
            Log("Processing components", DebugLevel.INFO);
            for (let comp of project_data.components)
                this.components.push(new ProjectComponent(comp, this));
            Log("Processing connection points", DebugLevel.INFO);
            for (let cp of project_data.connection_points)
                this.connection_points.push(new ConnectionPoint(cp, this));
            Log("Processing virtual objects", DebugLevel.INFO);
            for (let vo of project_data.virtual_objects)
                this.virtual_objects.push(new VirtualObject(vo));
            Log("Setting up additional variables", DebugLevel.INFO);
            Log("Setting offset", DebugLevel.INFO);
            this.offset = new math_1.VMath.Vector3(project_data.offset);
            Log("Setting normal", DebugLevel.INFO);
            this.normal = new math_1.VMath.Vector3(project_data.normal);
            this.graph = new Graph(project_data.graph);
            Log("Bounds: " + JSON.stringify(this.bounds), DebugLevel.VERBOSE);
            Log("Done project setup", DebugLevel.INFO);
        }
        UpdateFromFrontend(frontend_data, related_inputs, callback = null) {
            return __awaiter(this, void 0, void 0, function* () {
                logger_1.Logger.Log("Updating from frontend data", logger_1.Logger.DebugLevel.INFO);
                const container = frontend_data.product_container;
                let related_inputs_result = this.graph.related_inputs;
                if (related_inputs_result == null)
                    related_inputs_result = {};
                const initial_offset = this.GetOffset();
                logger_1.Logger.Log("Updating components", logger_1.Logger.DebugLevel.INFO);
                for (let child_name in container.children) {
                    let found = true;
                    let comp = this.components.find(function (c) { return c.name == child_name || c.guid == child_name; });
                    if (comp == null) {
                        comp = new ProjectComponent(this);
                        found = false;
                    }
                    const child = container.children[child_name];
                    let comp_inputs = null;
                    if (related_inputs != null && child_name in related_inputs)
                        comp_inputs = related_inputs[child_name];
                    logger_1.Logger.Log("Updating component: " + child_name, logger_1.Logger.DebugLevel.INFO);
                    yield comp.UpdateFromFrontend(child, comp_inputs, initial_offset);
                    if (!found) {
                        comp.name = child_name;
                        comp.is_active = true;
                        comp.path = "Р”РµС‚Р°Р»Рё";
                        this.components.push(comp);
                        logger_1.Logger.Log("Created new component for " + comp.name, logger_1.Logger.DebugLevel.INFO);
                    }
                    else {
                        logger_1.Logger.Log("Used existing component for " + comp.name, logger_1.Logger.DebugLevel.INFO);
                    }
                    if (related_inputs != null && child_name in related_inputs)
                        related_inputs_result[comp.guid] = related_inputs[child_name];
                }
                logger_1.Logger.Log("Updating virtual_objects", logger_1.Logger.DebugLevel.INFO);
                if (container.virtual_objects != null) {
                    for (let vo_name in container.virtual_objects) {
                        let found = true;
                        const vo_data = container.virtual_objects[vo_name];
                        let vo = this.virtual_objects.find(function (v) { return v.name == vo_name || v.guid == vo_data.guid; });
                        if (vo == null) {
                            vo = new VirtualObject();
                            found = false;
                        }
                        logger_1.Logger.Log("Updating " + vo_name, logger_1.Logger.DebugLevel.VERBOSE);
                        vo.UpdateFromFrontend(vo_data);
                        logger_1.Logger.Log("Done updating " + vo_name, logger_1.Logger.DebugLevel.VERBOSE);
                        if (!found) {
                            vo.name = vo_name;
                            vo.path = "РћР±СЉРµРєС‚С‹";
                            this.virtual_objects.push(vo);
                            logger_1.Logger.Log("Created new virtual object for " + vo_name, logger_1.Logger.DebugLevel.INFO);
                        }
                        else {
                            logger_1.Logger.Log("Used existing virtual object for " + vo_name, logger_1.Logger.DebugLevel.INFO);
                        }
                    }
                }
                if (this.virtual_objects.find(function (vo) {
                    return vo.modifier.type == VirtualObjectModifierType.CAMERA &&
                        (vo.modifier.camera_type == 1 || vo.modifier.camera_type == 2) &&
                        vo.modifier.order == 0;
                }) == null) {
                    logger_1.Logger.Log("Adding default spherical camera", logger_1.Logger.DebugLevel.INFO);
                    const cam = {};
                    cam.name = "РЎС„РµСЂРёС‡РµСЃРєР°СЏ РєР°РјРµСЂР°";
                    cam.path = "РћР±СЉРµРєС‚С‹";
                    cam.guid = utils_1.Utils.CreateUUID();
                    cam.position = new math_1.VMath.Vector3(0, this.bounds.size.y / 2.0, -2).mult(1000);
                    cam.modifier = {};
                    cam.modifier.type = VirtualObjectModifierType.CAMERA;
                    cam.modifier.camera_type = 1;
                    cam.modifier.rotation = new math_1.VMath.Quaternion();
                    cam.modifier.order = 0;
                    cam.modifier.is_pov = true;
                    cam.modifier.settings = {};
                    this.virtual_objects.push(cam);
                }
                this.graph.related_inputs = related_inputs_result;
                logger_1.Logger.Log("Callback is set: " + (callback != null), logger_1.Logger.DebugLevel.INFO);
                if (callback != null) {
                    callback(utils_1.Utils.safeStringify(this));
                }
            });
        }
        GetVertices() {
            return Project.GetRawVerticesFromSizeAndAngles(this.bounds.size, 0, 0);
        }
        GetFacePoints() {
            return Project.GetFacePointsFromRawVertices(this.bounds.center, new math_1.VMath.Quaternion(), this.bounds.size, this.GetVertices());
        }
        Assemble() {
            Log("Assembling project", DebugLevel.INFO);
            const processed = [];
            try {
                for (const cp of this.connection_points.reverse()) {
                    if (cp.p1 == null || cp.p2 == null)
                        continue;
                    const conn1 = cp.p1.connectable;
                    const conn2 = cp.p2.connectable;
                    cp.Connect(false);
                    if (conn1.children == undefined)
                        conn1.children = new Set();
                    conn1.children.add(conn2);
                    cp.Connect(true);
                    if (conn2.children == undefined)
                        conn2.children = new Set();
                    conn2.children.add(conn1);
                    processed.push(conn1);
                    processed.push(conn2);
                }
            }
            catch (e) {
                if (typeof e === "string") {
                    logger_1.Logger.Log(e.toUpperCase(), logger_1.Logger.DebugLevel.ERROR);
                }
                else if (e instanceof Error) {
                    logger_1.Logger.Log(e.message + "\n" + e.stack, logger_1.Logger.DebugLevel.ERROR);
                }
            }
            for (const conn of processed) {
                if (conn.children != undefined)
                    delete conn.children;
            }
            Log("Done assembling project", DebugLevel.INFO);
            this.ToCenter();
        }
        static GetRawVerticesFromSizeAndAngles(size, cut_angle1, cut_angle2) {
            const e = size.div(2.0);
            let angle_offset1 = 0;
            let angle_offset2 = 0;
            if (cut_angle1 !== 0)
                angle_offset1 = size.x * Math.tan(Math.PI - cut_angle1 / 180.0 * Math.PI);
            if (cut_angle2 !== 0)
                angle_offset2 = size.x * Math.tan(cut_angle2 / 180.0 * Math.PI);
            return [
                new math_1.VMath.Vector3(-e.x, -e.y, -e.z),
                new math_1.VMath.Vector3(-e.x, -e.y, e.z),
                new math_1.VMath.Vector3(e.x, -e.y + angle_offset1, e.z),
                new math_1.VMath.Vector3(e.x, -e.y + angle_offset1, -e.z),
                new math_1.VMath.Vector3(-e.x, e.y, -e.z),
                new math_1.VMath.Vector3(-e.x, e.y, e.z),
                new math_1.VMath.Vector3(e.x, e.y - angle_offset2, e.z),
                new math_1.VMath.Vector3(e.x, e.y - angle_offset2, -e.z)
            ];
        }
        static GetAnchor(a) {
            if (a === -0.5)
                return Anchor.MIN;
            return a === 0 ? Anchor.CENTER : Anchor.MAX;
        }
        static GetAnchorVector(point) {
            const get_float = function (a) {
                if (a === Anchor.MIN)
                    return -0.5;
                return a === Anchor.CENTER ? 0 : 0.5;
            };
            return new math_1.VMath.Vector3(get_float(point.anchor_x), get_float(point.anchor_y), get_float(point.anchor_z));
        }
        static GetFacePointsFromRawVertices(pos, rotation, size, raw_verts) {
            return [
                new FacePoint(raw_verts[0], pos, rotation),
                new FacePoint(raw_verts[1], pos, rotation),
                new FacePoint(raw_verts[2], pos, rotation),
                new FacePoint(raw_verts[3], pos, rotation),
                new FacePoint(raw_verts[4], pos, rotation),
                new FacePoint(raw_verts[5], pos, rotation),
                new FacePoint(raw_verts[6], pos, rotation),
                new FacePoint(raw_verts[7], pos, rotation),
                new FacePoint(raw_verts[4].add(raw_verts[0]).div(2.0), pos, rotation),
                new FacePoint(raw_verts[5].add(raw_verts[1]).div(2.0), pos, rotation),
                new FacePoint(raw_verts[6].add(raw_verts[2]).div(2.0), pos, rotation),
                new FacePoint(raw_verts[7].add(raw_verts[3]).div(2.0), pos, rotation),
                new FacePoint(raw_verts[1].add(raw_verts[0]).div(2.0), pos, rotation),
                new FacePoint(raw_verts[2].add(raw_verts[1]).div(2.0), pos, rotation),
                new FacePoint(raw_verts[3].add(raw_verts[2]).div(2.0), pos, rotation),
                new FacePoint(raw_verts[0].add(raw_verts[3]).div(2.0), pos, rotation),
                new FacePoint(raw_verts[5].add(raw_verts[4]).div(2.0), pos, rotation),
                new FacePoint(raw_verts[6].add(raw_verts[5]).div(2.0), pos, rotation),
                new FacePoint(raw_verts[7].add(raw_verts[6]).div(2.0), pos, rotation),
                new FacePoint(raw_verts[4].add(raw_verts[7]).div(2.0), pos, rotation),
                new FacePoint(raw_verts[4].add(raw_verts[1]).div(2.0), pos, rotation),
                new FacePoint(raw_verts[7].add(raw_verts[2]).div(2.0), pos, rotation),
                new FacePoint((raw_verts[2].add(raw_verts[6]).div(2.0)).add(raw_verts[1].add(raw_verts[5]).div(2.0)).div(2.0), pos, rotation),
                new FacePoint((raw_verts[0].add(raw_verts[4]).div(2.0)).add(raw_verts[3].add(raw_verts[7]).div(2.0)).div(2.0), pos, rotation),
                new FacePoint(raw_verts[2].add(raw_verts[0]).div(2.0), pos, rotation),
                new FacePoint(raw_verts[6].add(raw_verts[4]).div(2.0), pos, rotation),
                new FacePoint(new math_1.VMath.Vector3(0, 0, 0), pos, rotation)
            ];
        }
        GetOffset() {
            if (this.components.length === 0)
                return new math_1.VMath.Vector3();
            const points = this.GetFacePoints();
            const self = this;
            const target_point = points.find(function (p) {
                return p.anchor.equals(self.anchor);
            });
            if (target_point == null)
                return new math_1.VMath.Vector3();
            const ox = target_point.orths[0].mult(this.offset.x).div(1000.0);
            const oy = target_point.orths[1].mult(this.offset.y).div(1000.0);
            const oz = target_point.orths[2].mult(this.offset.z).div(1000.0);
            return target_point.position.add(ox).add(oy).add(oz);
        }
        GetRotation() {
            const identity_rotation = new Quaternion();
            if (this.components.length === 0)
                return identity_rotation;
            if (this.normal.isZero())
                return identity_rotation;
            const up = Vector3.cross(new Vector3(-1, 0, 0), this.normal);
            const additional_rotation = Quaternion.euler(90, 0, 0);
            return Quaternion.lookRotation(additional_rotation.rotate(this.normal), up);
        }
        GetPositioningPointByPath(path) {
            const item = this.GetProjectItemByPointPath(path);
            if (item == null)
                return undefined;
            return item.GetPointByPath(path);
        }
        GetProjectItemByPointPath(path) {
            for (const comp of this.components) {
                if (!path.includes(comp.path + "/" + comp.name + "/"))
                    continue;
                return comp;
            }
            for (const vo of this.virtual_objects) {
                if (!path.includes(vo.path + "/" + vo.name + "/"))
                    continue;
                return vo;
            }
            return undefined;
        }
        GetComponentByPath(path) {
            return this.components.find(function (c) {
                return c.path + "/" + c.name === path;
            });
        }
        ToCenter() {
            Log("Centering project", DebugLevel.INFO);
            Log("Project bounds: " + JSON.stringify(this.bounds), DebugLevel.VERBOSE);
            const center = this.bounds.center.mult(1000.0);
            for (const comp of this.components)
                comp.position = comp.position.sub(center);
            for (const vo of this.virtual_objects)
                vo.position = vo.position.sub(center);
            Log("Done centering project", DebugLevel.INFO);
        }
    }
    Project_1.Project = Project;
})(Project = exports.Project || (exports.Project = {}));


/***/ }),

/***/ "./src/utils.ts":
/*!**********************!*\
  !*** ./src/utils.ts ***!
  \**********************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.Utils = void 0;
var Utils;
(function (Utils) {
    Utils.CreateUUID = function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };
    Utils.GetHash = function (s) {
        let hash = 0, i, chr;
        if (s.length === 0)
            return hash;
        for (i = 0; i < s.length; i++) {
            chr = s.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0;
        }
        return hash;
    };
    Utils.safeStringify = function (obj, indent = 0) {
        let cache = [];
        const retVal = JSON.stringify(obj, function (key, value) {
            return typeof value === "object" && value !== null ? cache.includes(value) ? undefined : cache.push(value) && value : value;
        }, indent);
        cache = [];
        return retVal;
    };
})(Utils = exports.Utils || (exports.Utils = {}));


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/

Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.PerformanceCore = exports.Filesystem = exports.Utils = exports.Logger = exports.Generator = exports.IIK = exports.Project = exports.VMath = void 0;
const math_1 = __webpack_require__(/*! ./math */ "./src/math.ts");
Object.defineProperty(exports, "VMath", ({ enumerable: true, get: function () { return math_1.VMath; } }));
const project_1 = __webpack_require__(/*! ./project */ "./src/project.ts");
Object.defineProperty(exports, "Project", ({ enumerable: true, get: function () { return project_1.Project; } }));
const iik_1 = __webpack_require__(/*! ./iik */ "./src/iik.ts");
Object.defineProperty(exports, "IIK", ({ enumerable: true, get: function () { return iik_1.IIK; } }));
const generator_1 = __webpack_require__(/*! ./MeshUtils/generator */ "./src/MeshUtils/generator.ts");
Object.defineProperty(exports, "Generator", ({ enumerable: true, get: function () { return generator_1.Generator; } }));
const logger_1 = __webpack_require__(/*! ./logger */ "./src/logger.ts");
Object.defineProperty(exports, "Logger", ({ enumerable: true, get: function () { return logger_1.Logger; } }));
const utils_1 = __webpack_require__(/*! ./utils */ "./src/utils.ts");
Object.defineProperty(exports, "Utils", ({ enumerable: true, get: function () { return utils_1.Utils; } }));
const filesystem_1 = __webpack_require__(/*! ./filesystem */ "./src/filesystem.ts");
Object.defineProperty(exports, "Filesystem", ({ enumerable: true, get: function () { return filesystem_1.Filesystem; } }));
const performance_1 = __webpack_require__(/*! ./performance */ "./src/performance.ts");
Object.defineProperty(exports, "PerformanceCore", ({ enumerable: true, get: function () { return performance_1.PerformanceCore; } }));

})();

/******/ 	return __webpack_exports__;
/******/ })()
;
});
//# sourceMappingURL=s123_core.js.map