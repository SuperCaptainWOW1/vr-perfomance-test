import { S123 } from "./s123utils.js";

export class NetworkError extends Error {
  constructor(text, status) {
    super(text);
    this.status = status;
  }
}

/**
 * User type definition
 * @prop {string} pendingRequests
 * @prop {string} updateTokenPromise
 */
export default class Network {
  /**
   * Type
   * @typedef {Object} Options
   * @prop {Object} [options] Опции, которые будут переданы напрямую в window.fetch
   * @prop {Object} [params] Данные, которые нужно закодировать в параметрах запроса
   * @prop {String|Object} [body] Данные, которые нужно закодировать в теле запроса
   * @prop {String|Object} [data] Синоним для body
   * @prop {Boolean} [silent] Не выводить уведомления
   * @prop {('application/json'|'application/x-www-form-urlencoded')} [contentType] # Тип запроса, по умолчанию application/json
   * @prop {('json'|'text')} [responseType] Формат ответа, по умолчанию json
   * @prop {String} [base] Префикс запроса, по умолчанию /rest
   * @prop {function(any):any} [parser] Парсер результата запроса
   */

  /** @type {Promise<void>} */ refreshTokenPromise = null;
  /** @type {Array<String>} */ pendingRequests = null;
  /** @type {Function(string, string): any} */ #showNotification = null;

  /**
   * @param {Object} options
   * @param {Array} options.pendingRequests
   * @param {Function} options.showNotification
   */
  constructor(options = {}) {
    this.pendingRequests = options.pendingRequests ?? [];
    this.abortController = new AbortController();
    this.#showNotification =
      options.showNotification ?? ((error) => console.log(error));
  }

  /**
   *
   * @param {String} path
   * @param {Options} [options]
   */
  async post(path, options) {
    return this.#fetch("POST", path, options);
  }

  /**
   *
   * @param {String} path
   * @param {Options} [options]
   */
  async get(path, options) {
    return this.#fetch("GET", path, options);
  }

  /**
   *
   * @param {String} path
   * @param {Options} [options]
   */
  async delete(path, options) {
    return this.#fetch("DELETE", path, options);
  }

  /**
   *
   * @param {String} path
   * @param {Options} [options]
   */
  async put(path, options) {
    return this.#fetch("PUT", path, options);
  }

  async #fetch(method, path, options = {}) {
    console.log(path);

    options.body ??= options.data;
    options.responseType ??= "json";
    options.options ??= "options";
    options.silent ??= false;
    options.base ??= "/rest";

    stripUndefined(options.params);
    stripUndefined(options.body);

    const pathWithBase = options.base + path;
    const query = options.params ? S123.formatParams(options.params) : "";
    const fetchOptions = Object.assign({}, options.options);

    fetchOptions.method = method;

    if (typeof options.body == "object") {
      fetchOptions.headers ??= {};

      if (options.body instanceof FormData) {
        // options.contentType ??= "multipart/form-data";
        fetchOptions.body = options.body;
      } else {
        options.contentType ??= "application/json";

        if (options.contentType == "application/json") {
          fetchOptions.body = JSON.stringify(options.body);
        } else if (options.contentType == "application/x-www-form-urlencoded") {
          fetchOptions.body = new URLSearchParams(options.body);
        } else {
          throw new Error(
            `Unrecognized options.contentType during ${method} ${path}`
          );
        }

        fetchOptions.headers["Content-Type"] = options.contentType;
      }
    }

    if (this.abortController.signal.aborted) {
      this.#resume();
    }

    this.#emitStartRequest(path);
    let response;
    try {
      response = await fetch(
        S123.HOST + pathWithBase + query,
        Object.assign(S123.updateOptions(fetchOptions), {
          signal: this.abortController.signal,
        })
      );
    } catch (err) {
      !options.silent && this.#showNotification(err, "Ошибка сети");
      throw new NetworkError("Network error");
    }

    if (response.ok) {
      let result;

      try {
        result = await response[options.responseType]();
      } catch (error) {
        console.error(
          "Network#fetch: presumably options.responseType is wrong"
        );
        result = null;
      }

      if (typeof options.parser == "function") {
        const parseResult = options.parser(result);
        if (parseResult !== undefined) result = parseResult;
      }

      this.#emitEndRequest(path);
      return result;
    } else {
      this.#emitEndRequest(path);

      if (response.status == 401) {
        await this.refreshToken();
        return await this.#fetch(method, path, options);
      } else {
        const errorText = await response.text();
        !options.silent &&
          errorText &&
          this.#showNotification(errorText, "Ошибка дотнета");
        throw new NetworkError(errorText, response.status);
      }
    }
  }

  refreshToken() {
    if (!this.refreshTokenPromise) {
      this.refreshTokenPromise = (async () => {
        const token = S123.getToken();
        let authData = null;

        try {
          authData = await this.post("/connect/token", {
            base: "/identity",
            contentType: "application/x-www-form-urlencoded",
            body: {
              refresh_token: token.refresh_token,
              grant_type: "refresh_token",
              client_id: "web-business-app",
            },
          });
        } catch (error) {
          console.error(
            "Network#refreshToken: the server refuses to refresh a token"
          );
          console.error(error);
        }

        if (authData.refresh_token) {
          localStorage.setItem(
            S123.auth_key,
            JSON.stringify(
              Object.assign({}, token, {
                refresh_token: authData.refresh_token,
                access_token: authData.access_token,
              })
            )
          );
        } else {
          console.error(
            "Network#refreshToken: response from the server doesn't include refresh token:\n" +
              authData
          );
        }
      })().finally(() => {
        this.refreshTokenPromise = null;
      });
    }

    return this.refreshTokenPromise;
  }

  #emitStartRequest(path) {
    this.pendingRequests.push(path);
  }

  #emitEndRequest(path) {
    this.pendingRequests.splice(this.pendingRequests.indexOf(path), 1);
  }

  #resume() {
    console.log("Network#resume");
    this.abortController = new AbortController();
  }

  abort() {
    if (this.pendingRequests.length > 0) {
      console.log("Network#abort");
      this.abortController.abort();
    }
  }
}

export const network = new Network();

function stripUndefined(object) {
  if (typeof object == "object" && object !== null) {
    for (const key of Object.keys(object)) {
      if (object[key] === undefined) {
        delete object[key];
      }
    }
  }
}
