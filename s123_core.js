import runRemoteUMD from "./runRemoteUMD.js";

let origin = location.origin;

if (location.hostname == "localhost") {
  // if ((await fetch(`/common/s123_core.js`)).status != 200) {
  origin = "https://lab.system123.ru";
  // }
}

export default await runRemoteUMD(`core.js`);
