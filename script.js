import * as THREE from "three";
import { FBXLoader } from "three/examples/jsm/loaders/FBXLoader";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";
import { VRButton } from "three/examples/jsm/webxr/VRButton.js";
import { S123, S123Product } from "./s123utils.js";

// Настройки изменять на true/ false

const enableAntialiasing = true; // Включить/ выключить antialiasing
const enableResolutionDecrease = true; // Включить/ выключить уменьшение разрешение (текущее разрешение делится на resolutionDecreaseFactor)
const resolutionDecreaseFactor = 2; // Число на которое делится текущее разрешение
const enableTextures = false; // Включить/ выключить текстуры

let camera, renderer, scene, composer;
let controls;
S123(THREE, FBXLoader);

function start() {
  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    1000
  );

  renderer = new THREE.WebGLRenderer({ antialias: enableAntialiasing });
  renderer.xr.enabled = true;
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);

  renderer.setPixelRatio(
    enableResolutionDecrease
      ? window.devicePixelRatio / resolutionDecreaseFactor
      : window.devicePixelRatio
  );
  renderer.setSize(window.innerWidth, window.innerHeight);

  controls = new OrbitControls(camera, renderer.domElement);
  camera.position.set(0, 1, 3);
  controls.update();

  var vr_button = VRButton.createButton(renderer);
  document.body.appendChild(vr_button);

  renderer.setAnimationLoop(() => {
    animate();
  });

  var product = new S123Product(
    "b127bf20-db61-48c2-b1b5-24da4209f337",
    false,
    null,
    null,
    null,
    true,
    null,
    enableTextures
  );
  scene.add(product.container);
}

function animate() {
  S123.animate();

  controls.update();

  renderer.render(scene, camera);
}

start();
